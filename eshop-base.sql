-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: eshop
-- ------------------------------------------------------
-- Server version	5.7.18-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `addresses`
--

DROP TABLE IF EXISTS `addresses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `addresses` (
  `addressId` int(11) NOT NULL AUTO_INCREMENT,
  `apartments` int(11) DEFAULT NULL,
  `house` int(11) NOT NULL,
  `postCode` int(11) NOT NULL,
  `street` varchar(255) NOT NULL,
  `cityId` int(11) NOT NULL,
  PRIMARY KEY (`addressId`),
  KEY `FK_mw4y51ox4nvxj7uxcc2ifh0ra` (`cityId`),
  CONSTRAINT `FK_mw4y51ox4nvxj7uxcc2ifh0ra` FOREIGN KEY (`cityId`) REFERENCES `cities` (`cityId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `addresses`
--

LOCK TABLES `addresses` WRITE;
/*!40000 ALTER TABLE `addresses` DISABLE KEYS */;
INSERT INTO `addresses` VALUES (1,1,1,195270,'Nevskiy',1),(2,130,83,195271,'Kondratyevsky',2),(3,1,1,123321,'Tverskaya',3),(4,1,1,100922,'Tverskaya',3),(5,106,5,12932,'14th street',4);
/*!40000 ALTER TABLE `addresses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `categoryId` int(11) NOT NULL AUTO_INCREMENT,
  `categoryName` varchar(20) COLLATE latin1_general_ci NOT NULL,
  `filterFile` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`categoryId`),
  UNIQUE KEY `UK_eeyxi4vom9nrwcce0gddyo3rn` (`categoryName`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Phones','/WEB-INF/jsp/filters/filterPhones.jsp'),(2,'Laptops','/WEB-INF/jsp/filters/filterLaptops.jsp');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cities` (
  `cityId` int(11) NOT NULL AUTO_INCREMENT,
  `countryId` int(11) DEFAULT NULL,
  `cityName` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`cityId`),
  KEY `FK_8atj3hwbl1latdtjmeeyvqilu` (`countryId`),
  CONSTRAINT `FK_8atj3hwbl1latdtjmeeyvqilu` FOREIGN KEY (`countryId`) REFERENCES `countries` (`countryId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities`
--

LOCK TABLES `cities` WRITE;
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
INSERT INTO `cities` VALUES (1,1,'SPB'),(2,1,'Saint Petersburg '),(3,1,'Moscow'),(4,2,'Washington');
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `colors`
--

DROP TABLE IF EXISTS `colors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `colors` (
  `colorId` int(11) NOT NULL AUTO_INCREMENT,
  `colorName` varchar(20) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`colorId`),
  UNIQUE KEY `UK_5ur05bd8gp1mohicglit6t9f4` (`colorName`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `colors`
--

LOCK TABLES `colors` WRITE;
/*!40000 ALTER TABLE `colors` DISABLE KEYS */;
INSERT INTO `colors` VALUES (2,'black'),(3,'gold'),(4,'silver'),(1,'white');
/*!40000 ALTER TABLE `colors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `countryId` int(11) NOT NULL AUTO_INCREMENT,
  `countryName` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`countryId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,'Russia'),(2,'USA');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `deliverymethods`
--

DROP TABLE IF EXISTS `deliverymethods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `deliverymethods` (
  `deliveryMethodId` int(11) NOT NULL AUTO_INCREMENT,
  `deliveryMethodName` varchar(255) NOT NULL,
  `deliveryMethodPrice` double DEFAULT NULL,
  PRIMARY KEY (`deliveryMethodId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `deliverymethods`
--

LOCK TABLES `deliverymethods` WRITE;
/*!40000 ALTER TABLE `deliverymethods` DISABLE KEYS */;
INSERT INTO `deliverymethods` VALUES (1,'Post',15),(2,'Self delivery',0);
/*!40000 ALTER TABLE `deliverymethods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `laptops`
--

DROP TABLE IF EXISTS `laptops`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `laptops` (
  `innerId` int(11) NOT NULL AUTO_INCREMENT,
  `memory` int(11) NOT NULL,
  `screen` double NOT NULL,
  PRIMARY KEY (`innerId`),
  UNIQUE KEY `laptops_innerId_uindex` (`innerId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `laptops`
--

LOCK TABLES `laptops` WRITE;
/*!40000 ALTER TABLE `laptops` DISABLE KEYS */;
/*!40000 ALTER TABLE `laptops` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manufacturers`
--

DROP TABLE IF EXISTS `manufacturers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manufacturers` (
  `manufacturerId` int(11) NOT NULL AUTO_INCREMENT,
  `manufacturerName` varchar(20) COLLATE latin1_general_ci NOT NULL,
  PRIMARY KEY (`manufacturerId`),
  UNIQUE KEY `UK_755slq59x8ox3167ajtfly93x` (`manufacturerName`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manufacturers`
--

LOCK TABLES `manufacturers` WRITE;
/*!40000 ALTER TABLE `manufacturers` DISABLE KEYS */;
INSERT INTO `manufacturers` VALUES (1,'Apple'),(5,'Asus'),(13,'Dell'),(3,'HP'),(4,'Lenovo'),(2,'Meizu'),(7,'Sony'),(12,'Uncle Liao'),(6,'Xiaomi');
/*!40000 ALTER TABLE `manufacturers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `orderId` int(11) NOT NULL AUTO_INCREMENT,
  `orderDate` datetime DEFAULT NULL,
  `totalSum` double DEFAULT NULL,
  `addressId` int(11) NOT NULL,
  `deliveryMethodId` int(11) NOT NULL,
  `orderStatusId` int(11) DEFAULT NULL,
  `paymentMethodId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  PRIMARY KEY (`orderId`),
  KEY `FK_8gsnxevnneig3b1fa91mkj3ou` (`addressId`),
  KEY `FK_6fq9r3gk0610au6fl0bvgmik2` (`deliveryMethodId`),
  KEY `FK_6tq7rwgqh5ae0pcxxrtut2g63` (`orderStatusId`),
  KEY `FK_nm5xvd134dht61jc5v1p5ph01` (`paymentMethodId`),
  KEY `FK_2gcgbfud7n5ixb7qqb6nuaqxw` (`userId`),
  CONSTRAINT `FK_2gcgbfud7n5ixb7qqb6nuaqxw` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`),
  CONSTRAINT `FK_6fq9r3gk0610au6fl0bvgmik2` FOREIGN KEY (`deliveryMethodId`) REFERENCES `deliverymethods` (`deliveryMethodId`),
  CONSTRAINT `FK_6tq7rwgqh5ae0pcxxrtut2g63` FOREIGN KEY (`orderStatusId`) REFERENCES `orderstatuses` (`orderStatusId`),
  CONSTRAINT `FK_8gsnxevnneig3b1fa91mkj3ou` FOREIGN KEY (`addressId`) REFERENCES `addresses` (`addressId`),
  CONSTRAINT `FK_nm5xvd134dht61jc5v1p5ph01` FOREIGN KEY (`paymentMethodId`) REFERENCES `paymentmethods` (`paymentMethodId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` VALUES (1,'2017-08-06 18:17:20',6915,2,1,3,2,1),(2,'2017-08-06 18:18:44',6515,4,1,2,3,3),(3,'2017-08-06 21:07:38',6015,5,1,1,1,4),(4,'2017-08-07 14:19:28',515,2,1,3,2,1),(5,'2017-08-07 15:45:26',315,2,1,2,1,1),(6,'2017-08-07 16:05:44',4415,2,1,3,2,1);
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orderstatuses`
--

DROP TABLE IF EXISTS `orderstatuses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orderstatuses` (
  `orderStatusId` int(11) NOT NULL AUTO_INCREMENT,
  `orderStatusName` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`orderStatusId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orderstatuses`
--

LOCK TABLES `orderstatuses` WRITE;
/*!40000 ALTER TABLE `orderstatuses` DISABLE KEYS */;
INSERT INTO `orderstatuses` VALUES (1,'new'),(2,'paid'),(3,'sent');
/*!40000 ALTER TABLE `orderstatuses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `paymentmethods`
--

DROP TABLE IF EXISTS `paymentmethods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `paymentmethods` (
  `paymentMethodId` int(11) NOT NULL AUTO_INCREMENT,
  `paymentMethodName` varchar(255) NOT NULL,
  PRIMARY KEY (`paymentMethodId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `paymentmethods`
--

LOCK TABLES `paymentmethods` WRITE;
/*!40000 ALTER TABLE `paymentmethods` DISABLE KEYS */;
INSERT INTO `paymentmethods` VALUES (1,'Cash'),(2,'Card'),(3,'Paypal');
/*!40000 ALTER TABLE `paymentmethods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `phones`
--

DROP TABLE IF EXISTS `phones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `phones` (
  `innerId` int(11) NOT NULL AUTO_INCREMENT,
  `screen` double NOT NULL,
  `memory` int(11) DEFAULT NULL,
  PRIMARY KEY (`innerId`),
  UNIQUE KEY `phones_innerId_uindex` (`innerId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `phones`
--

LOCK TABLES `phones` WRITE;
/*!40000 ALTER TABLE `phones` DISABLE KEYS */;
INSERT INTO `phones` VALUES (1,4.5,2),(2,5.5,4);
/*!40000 ALTER TABLE `phones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photos`
--

DROP TABLE IF EXISTS `photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `photos` (
  `photoId` int(11) NOT NULL AUTO_INCREMENT,
  `productId` int(11) DEFAULT NULL,
  `photoURL` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`photoId`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photos`
--

LOCK TABLES `photos` WRITE;
/*!40000 ALTER TABLE `photos` DISABLE KEYS */;
INSERT INTO `photos` VALUES (1,1,'/static/photo/1/apple-iphone-5s-2.jpg'),(2,1,'/static/photo/1/apple-iphone-5s-1.jpg'),(3,1,'/static/photo/1/apple-iphone-5s-3.jpg'),(4,1,'/static/photo/1/apple-iphone-5s-ofic.jpg'),(27,5,'/static/photo/5/ivs5bv6nvse.png'),(28,5,'/static/photo/5/2b0aidnvfhb.jpg'),(35,5,'/static/photo/5/t8flsc9iesp.jpg'),(47,3,'/static/photo/3/cs1vltbf9aq.jpg'),(48,3,'/static/photo/3/pngv5m1pvlh.jpg'),(49,3,'/static/photo/3/o0fb6otd10t.jpg'),(50,6,'/static/photo/6/tu0f4r4347b.png'),(51,7,'/static/photo/7/finqf62i0jj.jpg'),(52,8,'/static/photo/8/m9u8q21njja.jpg'),(55,10,'/static/photo/10/arofb23i3pb.jpg');
/*!40000 ALTER TABLE `photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `productId` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) COLLATE latin1_general_ci NOT NULL,
  `price` double NOT NULL,
  `weight` double DEFAULT NULL,
  `categoryId` int(11) DEFAULT NULL,
  `colorId` int(11) DEFAULT NULL,
  `manufacturerId` int(11) DEFAULT NULL,
  `description` varchar(255) COLLATE latin1_general_ci DEFAULT NULL,
  `memory` int(11) DEFAULT NULL,
  `screen` double DEFAULT NULL,
  PRIMARY KEY (`productId`),
  UNIQUE KEY `UK_o61fmio5yukmmiqgnxf8pnavn` (`name`),
  KEY `FK_74yw2hp7rpgouxn9afpfhx35d` (`categoryId`),
  KEY `FK_o2435983krugiagk7hr9iqd7n` (`colorId`),
  KEY `FK_knndwxgcyoqiesyhv7mliabl1` (`manufacturerId`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (1,'iPhone 5s',300,200,1,1,1,'A wonderful gadget from 2013',16,4.5),(2,'iPhone 6',800,150,1,2,1,'Buy it even if you haven\'t repaid a mortgage for iPhone 5',64,5.2),(3,'iPhone 7',1200,120,1,1,1,'You can always use a mortgage to buy it!',128,7),(4,'M3 Note',250,200,1,3,2,'Cheap Chinese phone ',16,5.5),(5,'MacBook Air',1500,1500,2,1,1,'Excellent laptop for hipsters',512,13.6),(6,'iPhone 3',15,300,1,2,1,'Change your Nokia 3310 to this one!',1,4.2),(7,'IdeaPad 110-15ACL',300,2000,2,1,4,'Lenovo IdeaPad 110-15ACL, 80TJ00D7RK, 15.6\" (1366x768), 4GB, 500GB, AMD E1-7010, AMD Radeon R2, LAN, WiFi, Win10, black',4,15.6),(8,'X540LA',500,1500,2,2,5,' ASUS X540LA, 90NB0B02-M17600, 15.6\" (1920x1080), 8GB, 1000GB, Intel Core i5-5200U(2.2), Intel HD Graphics, DVDÂ±RW DL, LAN, WiFi, BT, FreeDOS, white',8,15.6),(10,'Dell laptop',800,1500,2,2,13,'excellent stuff',8,15.6);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `productsinorder`
--

DROP TABLE IF EXISTS `productsinorder`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `productsinorder` (
  `productInOrderId` int(11) NOT NULL AUTO_INCREMENT,
  `priceInOrder` double NOT NULL,
  `qtyInOrder` int(11) NOT NULL,
  `orderId` int(11) NOT NULL,
  `productId` int(11) NOT NULL,
  PRIMARY KEY (`productInOrderId`),
  KEY `FK_ewsvyvnqkp0a41s6upf5mctf3` (`orderId`),
  KEY `FK_gbqyhx9vkrfkquxl2qngspboq` (`productId`),
  CONSTRAINT `FK_gbqyhx9vkrfkquxl2qngspboq` FOREIGN KEY (`productId`) REFERENCES `products` (`productId`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `productsinorder`
--

LOCK TABLES `productsinorder` WRITE;
/*!40000 ALTER TABLE `productsinorder` DISABLE KEYS */;
INSERT INTO `productsinorder` VALUES (1,1500,1,1,5),(2,300,1,1,1),(3,1157.5,1,2,3),(4,1500,1,6,5),(5,300,1,6,1),(6,300,1,7,1),(7,250,5,8,4),(8,300,4,8,1),(9,800,3,1,2),(10,1500,2,1,5),(11,300,5,1,1),(12,250,8,2,4),(13,1500,3,2,5),(14,1500,4,3,5),(15,500,1,4,8),(16,300,1,5,7),(17,250,2,6,4),(18,300,1,6,7),(19,1200,3,6,3);
/*!40000 ALTER TABLE `productsinorder` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `roleId` int(11) NOT NULL AUTO_INCREMENT,
  `roleName` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`roleId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin'),(2,'operator'),(3,'user');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `birthdate` varchar(10) NOT NULL,
  `email` varchar(255) NOT NULL,
  `login` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `surname` varchar(255) NOT NULL,
  `roleId` int(11) NOT NULL,
  PRIMARY KEY (`userId`),
  UNIQUE KEY `UK_6dotkott2kjsp8vw4d0m25fb7` (`email`),
  UNIQUE KEY `UK_ow0gan20590jrb00upg3va2fn` (`login`),
  KEY `FK_rin8hpeky1j0qj4mwj1lcc6mb` (`roleId`),
  CONSTRAINT `FK_rin8hpeky1j0qj4mwj1lcc6mb` FOREIGN KEY (`roleId`) REFERENCES `roles` (`roleId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'29-10-1988','kr5031@gmail.com','admin','Konstantin','Pa$$w0rd','Reus22',1),(2,'01-01-1985','no@mail.com','user','Vasya','Pa$$w0rd','Userov',3),(3,'01-01-1812','vasya@mail.ru','vasya','Vasiliy','Pa$$w0rd','Lohankin',3),(4,'01-02-2003','ivan@mail.ru','ivan','Ivan','Pa$$w0rd','Ivan',3);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-08-08 19:04:17
