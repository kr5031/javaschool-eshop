package com.tsystems.javaschool.eshop.controller;

import com.tsystems.javaschool.eshop.model.Product;
import com.tsystems.javaschool.eshop.service.CartService;
import com.tsystems.javaschool.eshop.service.ProductService;
import com.tsystems.javaschool.eshop.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import static com.tsystems.javaschool.eshop.controller.ProductController.ALL_MANUFACTURERS_ID;

/**
 * Product cart controller
 */
@Controller
public class CartController {
    private static final Logger logger = Logger.getLogger(CartController.class.getName());
    @Autowired
    ProductController productController;
    @Autowired
    UserService userService;
    @Autowired
    private CartService cartService;
    @Autowired
    private ProductService productService;

    /**
     * Add specified productId's and qty's to a cart
     *
     * @param pathVariables productIds & qty passed via POST
     * @param model
     * @return cart view
     */
    @RequestMapping(value = "/cart/add", method = RequestMethod.POST)
    public String addToCart(@RequestParam MultiValueMap<String, String> pathVariables, Model model) {
        for (String key : pathVariables.keySet()) {
            if ("productId".equals(key)) {
                String productId = pathVariables.getFirst(key);
                Product toAdd = productService.getProductById(Integer.parseInt(productId));
                cartService.addProductToCart(toAdd, Integer.parseInt(pathVariables.getFirst("qty")));
                logger.info("UserID " + userService.getCurrentUserId() + " added productId "
                        + toAdd.getProductId() + " to cart");
            }
        }
        prepareModelForCart(model);
        return "cart";
    }

    /**
     * Set qty for products in a cart
     *
     * @param pathVariables HashMap<String, String> productId, qty
     * @param model
     * @return
     */
    @RequestMapping(value = "/cart/setQty", method = RequestMethod.POST)
    public String setProductQtyInCart(@RequestParam MultiValueMap<String, String> pathVariables, Model model) {
        for (String key : pathVariables.keySet()) {
            if ("productId".equals(key)) {
                int productId = Integer.parseInt(pathVariables.getFirst(key));
                int qty = Integer.parseInt(pathVariables.getFirst("qty"));
                cartService.setProductQtyInCart(productId, qty);
            }
        }
        prepareModelForCart(model);
        return "cartForm";
    }

    /**
     * Fill model for cart with products, product images and prices
     *
     * @param model
     */
    private void prepareModelForCart(Model model) {
        model.addAttribute("listImages", cartService.getPhotosForProductsInCart());
        model.addAttribute("productsInCart", cartService.getCartFromSession().getProductsInCart());
        model.addAttribute("pricesForProductsInCart", cartService.getPricesForProductsInCart());
        productController.fillModelWithStaticData(model, ALL_MANUFACTURERS_ID);
    }

    /**
     * @param model
     * @return cart view
     */
    @RequestMapping(value = "/cart")
    public String getCart(Model model) {
        prepareModelForCart(model);
        return "cart";
    }

    /**
     * Delete specified productId from the cart
     *
     * @param productId
     * @return cart view
     */
    @RequestMapping(value = "/cart/delete/{productId}")
    public String deleteProductFromCart(@PathVariable int productId) {
        Product product = productService.getProductById(productId);
        cartService.deleteProductFromCart(product);
        logger.info("UserID " + userService.getCurrentUserId() + " deleted productId "
                + productId + " from cart");
        return "redirect:/cart";
    }
}