package com.tsystems.javaschool.eshop.controller;

import com.tsystems.javaschool.eshop.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {
    @Autowired
    ProductService productService;

    /**
     * Fill navigation menu with categories
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/main")
    public String getIndex(Model model) {
        model.addAttribute("listCategories", productService.getAllCategories());
        return "main";
    }
}
