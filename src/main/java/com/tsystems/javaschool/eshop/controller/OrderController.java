package com.tsystems.javaschool.eshop.controller;

import com.tsystems.javaschool.eshop.exceptions.NotAuthorizedException;
import com.tsystems.javaschool.eshop.exceptions.WrongIdException;
import com.tsystems.javaschool.eshop.exceptions.WrongUserException;
import com.tsystems.javaschool.eshop.model.Order;
import com.tsystems.javaschool.eshop.model.User;
import com.tsystems.javaschool.eshop.service.CartService;
import com.tsystems.javaschool.eshop.service.OrderService;
import com.tsystems.javaschool.eshop.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.util.List;

import static com.tsystems.javaschool.eshop.controller.ProductController.ALL_MANUFACTURERS_ID;

/**
 * Controller for serving orders
 */
@Controller
public class OrderController {
    private static final Logger logger = Logger.getLogger(OrderController.class.getName());
    private static final String LOGIN = "login";
    private static final String LIST_ORDERS_ATTR = "listOrders";
    private static final String RESULT_ATTR = "result";

    @Autowired
    OrderService orderService;

    @Autowired
    HttpSession httpSession;

    @Autowired
    CartService cartService;

    @Autowired
    UserController userController;

    @Autowired
    UserService userService;

    @Autowired
    private ProductController productController;

    /**
     * @param model
     * @return my orders view
     */
    @RequestMapping(value = "/myOrders")
    public String getMyOrders(Model model) {
        User user = (User) httpSession.getAttribute("user");
        if (user == null) {
            productController.fillModelWithStaticData(model, ALL_MANUFACTURERS_ID);
            return LOGIN;
        } else {
            try {
                model.addAttribute(LIST_ORDERS_ATTR, orderService.getOrdersByUserId(user.getUserId()));
            } catch (WrongUserException e) {
                model.addAttribute(RESULT_ATTR, "Wrong User instance!");
            }
            productController.fillModelWithStaticData(model, ALL_MANUFACTURERS_ID);
            return "myOrders";
        }
    }

    /**
     * @param orderId
     * @param model
     * @return view for order with the specified ID
     */
    @RequestMapping(value = "/myOrders/{orderId}")
    public String getOrder(@PathVariable int orderId, Model model) {
        try {
            model.addAttribute("order", orderService.getOrderById((User) httpSession.getAttribute("user"), orderId));
            model.addAttribute("listProducts", orderService.getProductsInOrderByOrderId(orderId));
        } catch (WrongIdException e) {
            model.addAttribute("exception", "Wrong Order ID");
            logger.warn("Anonymous user failed to access order #" + orderId);
            return "errorPage";
        } catch (NotAuthorizedException e) {
            logger.warn("User " + userService.getCurrentUserId() + " failed to access order #" + orderId);
            model.addAttribute("exception", "You are not authorized to view this order!");
            return "errorPage";
        }
        productController.fillModelWithStaticData(model, ALL_MANUFACTURERS_ID);
        return "oneOrder";
    }

    /**
     * Get delivery method & payment method for an order
     *
     * @param pathVariables
     * @param model
     * @return
     */
    @RequestMapping(value = "/checkout", method = RequestMethod.POST)
    public String getCheckout(@RequestParam MultiValueMap<String, String> pathVariables, Model model) {
        prepareModelForCart(model);
        String deliveryMethodIdStr = pathVariables.getFirst("deliveryMethodId");
        String paymentMethodIdStr = pathVariables.getFirst("paymentMethodId");
        productController.fillModelWithStaticData(model, ALL_MANUFACTURERS_ID);
        if (deliveryMethodIdStr != null && !deliveryMethodIdStr.isEmpty() && paymentMethodIdStr != null && !paymentMethodIdStr.isEmpty()) {
            int deliveryMethodId = Integer.parseInt(deliveryMethodIdStr);
            int paymentMethodId = Integer.parseInt(paymentMethodIdStr);
            model.addAttribute("deliveryMethod", orderService.getDeliveryMethodById(deliveryMethodId));
            model.addAttribute("paymentMethod", orderService.getPaymentMethodById(paymentMethodId));
            return "checkout";
        } else {
            return "redirect:/selectDelivery";
        }
    }

    /**
     * Create new order
     *
     * @param pathVariables
     * @param model
     * @return
     */
    @RequestMapping(value = "/checkout/confirm", method = RequestMethod.POST)
    public String createOrder(@RequestParam MultiValueMap<String, String> pathVariables, Model model) {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>(pathVariables);
        productController.fillModelWithStaticData(model, ALL_MANUFACTURERS_ID);
        if (!userController.isLoggedIn()) {
            return LOGIN;
        }
        params.add("total", pathVariables.getFirst("total"));
        orderService.createOrder(params);
        return getMyOrders(model);
    }

    /**
     * @param model
     * @return view for choosing delivery & payment method
     */
    @RequestMapping(value = "/selectDelivery")
    public String getSelectDelivery(Model model) {
        productController.fillModelWithStaticData(model, ALL_MANUFACTURERS_ID);
        if (!userController.isLoggedIn()) {
            model.addAttribute("toRedirectAfter", "/cart");
            return LOGIN;
        }
        model.addAttribute("listDeliveryMethods", orderService.getAllDeliveryMethods());
        model.addAttribute("listPaymentMethods", orderService.getAllPaymentMethods());
        return "selectDelivery";
    }

    /**
     * @param model
     * @return admin view for all created orders
     */
    @RequestMapping(value = "/admin/orders")
    public String getAdminOrders(Model model) {
        productController.fillModelWithStaticData(model, ALL_MANUFACTURERS_ID);
        if (!userService.isAdmin()) {
            model.addAttribute(RESULT_ATTR, "Access denied");
            return LOGIN;
        }
        List<Order> orders = orderService.getAllOrders();
        model.addAttribute(LIST_ORDERS_ATTR, orders);
        model.addAttribute("listOrdersStatuses", orderService.getAllOrdersStatuses());
        return "adminOrders";
    }

    /**
     * Change orders' state via admin interface
     *
     * @param pathVariables
     * @param model
     * @return admin view for orders
     */
    @RequestMapping(value = "/admin/updateOrdersState", method = RequestMethod.POST)
    public String getAdminUpdateOrders(@RequestParam MultiValueMap<String, String> pathVariables, Model model) {
        productController.fillModelWithStaticData(model, ALL_MANUFACTURERS_ID);
        if (!userService.isAdmin()) {
            model.addAttribute("result", "Access denied");
            return LOGIN;
        }
        orderService.updateOrdersStatuses(pathVariables);
        model.addAttribute(LIST_ORDERS_ATTR, orderService.getAllOrders());
        model.addAttribute("listOrdersStatuses", orderService.getAllOrdersStatuses());
        return "adminOrders";
    }

    /**
     * Fills model for cart with products and images for them
     *
     * @param model
     */
    private void prepareModelForCart(Model model) {
        model.addAttribute("listImages", cartService.getPhotosForProductsInCart());
        model.addAttribute("productsInCart", cartService.getCartFromSession().getProductsInCart());
        model.addAttribute("pricesForProductsInCart", cartService.getPricesForProductsInCart());
        productController.fillModelWithStaticData(model, ALL_MANUFACTURERS_ID);
    }

}
