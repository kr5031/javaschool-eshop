package com.tsystems.javaschool.eshop.controller;

import com.tsystems.javaschool.eshop.service.PhotoService;
import com.tsystems.javaschool.eshop.service.ProductService;
import com.tsystems.javaschool.eshop.service.UserService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;
import java.io.IOException;

import static com.tsystems.javaschool.eshop.controller.ProductController.ALL_MANUFACTURERS_ID;

/**
 * Controller used for serving images
 */
@Controller
public class PhotoController {
    private static final Logger logger = Logger.getLogger(PhotoController.class.getName());
    private static final String RESULT_ATTR = "result";

    @Autowired
    PhotoService photoService;

    @Autowired
    UserService userService;

    @Autowired
    ProductService productService;

    @Autowired
    ServletContext servletContext;

    @Autowired
    ProductController productController;

    @Autowired
    HttpSession httpSession;

    /**
     * Request photo by photoId
     *
     * @param photoId
     * @return JPG image
     */
    @RequestMapping(value = "/getPhoto", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    @ResponseBody
    public byte[] getPhoto(@RequestParam("photoId") int photoId) {
        try {
            return photoService.getPhotoById(photoId);
        } catch (IOException e) {
            logger.warn("I/O exception during retrieving photoID=" + photoId);
            return new byte[]{};
        }
    }

    /**
     * Request one (first) photo for preview for the product
     *
     * @param productId
     * @return JPG image
     */
    @RequestMapping(value = "/getPreviewPhoto", method = RequestMethod.GET, produces = MediaType.IMAGE_JPEG_VALUE)
    @ResponseBody
    public byte[] getPreviewPhoto(@RequestParam("productId") int productId) {
        return photoService.getPreviewPhoto(productId);
    }

    /**
     * Upload images
     *
     * @param files     array of files (up to 3)
     * @param productId
     * @param model
     * @return admin view for fils upload
     */
    @RequestMapping(value = "/admin/uploadPhotos", method = RequestMethod.POST)
    public String uploadPhotos(@RequestParam("file") MultipartFile[] files, @RequestParam("productId") int productId,
                               Model model) {
        try {
            if (isAdmin()) {
                photoService.savePhotos(files, productId);
                model.addAttribute(RESULT_ATTR, "Upload successful");
                logger.info("Photos for productID " + productId + " were uploaded");
            }
        } catch (IOException e) {
            model.addAttribute(RESULT_ATTR, "Upload failed due to I/O error!");
            logger.warn("I/O exception during uploading photos by " + userService.getCurrentUserId());
        }
        productController.fillModelWithStaticData(model, ALL_MANUFACTURERS_ID);
        return "adminUploadPhotos";
    }

    /**
     * @param productId
     * @param model
     * @return upload view for product
     */
    @RequestMapping(value = "/admin/uploadPhotos", method = RequestMethod.GET)
    public String getUploadPhotos(@RequestParam("productId") Integer productId, Model model) {
        if (productId != null && isAdmin()) {
            model.addAttribute("listImages", photoService.getProductImagesURLs(productId));
            model.addAttribute("productId", productId);
        } else {
            model.addAttribute(RESULT_ATTR, "failed (productId is null or you're not admin");
        }
        productController.fillModelWithStaticData(model, ALL_MANUFACTURERS_ID);
        return "adminUploadPhotos";
    }

    @RequestMapping(value = "/admin/getUploadedPhotos", method = RequestMethod.POST)
    public String getUploadedPhotosForProductId(@RequestParam("productId") int productId, Model model) {
        model.addAttribute("listImages", photoService.getProductImagesURLs(productId));
        return "adminUploadedPhotos";
    }

    /**
     * @param model
     * @return upload form
     */
    @RequestMapping(value = "/admin/uploadForm")
    public String getUploadForm(Model model) {
        productController.fillModelWithStaticData(model, ALL_MANUFACTURERS_ID);
        model.addAttribute("listProducts", productService.getProductsForCategory(1));
        if (isAdmin()) {
            return "adminUploadForm";
        } else {
            return "login";
        }
    }

    /**
     * @return whether logged user is admin
     */
    private boolean isAdmin() {
        Boolean b = (Boolean) httpSession.getAttribute("isAdmin");
        return b != null && b;
    }
}
