package com.tsystems.javaschool.eshop.controller;

import com.tsystems.javaschool.eshop.exceptions.CantBeDeletedException;
import com.tsystems.javaschool.eshop.exceptions.MissingRequiredFieldsException;
import com.tsystems.javaschool.eshop.model.Product;
import com.tsystems.javaschool.eshop.service.PhotoService;
import com.tsystems.javaschool.eshop.service.ProductService;
import com.tsystems.javaschool.eshop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Controller for working with products
 */
@Controller
public class ProductController {
    private static final String CATEGORY_ID_ATTR = "categoryId";
    public static final String LIST_MANUFACTURERS_ATTR = "listManufacturers";
    private static final String LIST_PRODUCTS_ATTR = "listProducts";
    private static final String RESULT_ATTR = "result";
    private static final String ACCESS_DENIED_MSG = "Access denied";
    private static final String CANT_BE_DELETED_MSG = "Can't be deleted (has associated objects)";
    private static final String LOGIN_PAGE = "login";
    private static final String MANAGE_CATEGORIES_PAGE = "manageCategories";
    private static final String MANAGE_MANUFACTURERS_PAGE = "manageManufacturers";
    private static final String MANAGE_COLORS_PAGE = "manageColors";
    public static final int DEFAULT_CATEGORY_ID = 1;
    public static final int ALL_MANUFACTURERS_ID = 0;

    @Autowired
    PhotoService photoService;
    @Autowired
    private ProductService productService;
    @Autowired
    private UserService userService;

    /**
     * @param model
     * @return products view
     */
    @RequestMapping(value = "/product")
    public String getProducts(Model model) {
        fillModelWithStaticData(model, DEFAULT_CATEGORY_ID);
        return getProducts(getMultiValueMap(CATEGORY_ID_ATTR, Integer.toString(DEFAULT_CATEGORY_ID)), model);
    }

    /**
     * @param pathVariables
     * @param model
     * @return filtered view for products
     */
    @RequestMapping(value = "/product", method = RequestMethod.POST)
    public String getProducts(@RequestParam MultiValueMap<String, String> pathVariables, Model model) {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>(pathVariables);
        String s = params.getFirst(CATEGORY_ID_ATTR);
        int categoryId;
        if (s == null) {
            categoryId = 1;
            params.add(CATEGORY_ID_ATTR, "1");
        } else {
            categoryId = Integer.parseInt(s);
        }
        model.addAttribute("filterFile", productService.getFilterFileForCategory(categoryId));
        model.addAttribute(LIST_MANUFACTURERS_ATTR, productService.getAllManufacturersForCategory(categoryId));
        model.addAttribute(LIST_PRODUCTS_ATTR, productService.getFilteredProducts(params));
        model.addAttribute("lastCategoryIdFiltered", categoryId);
        fillModelWithStaticData(model, categoryId);
        return "product";
    }

    /**
     * Used to update product grid view with a filter applied
     *
     * @param pathVariables parameters for filtering
     * @param model
     * @return product view grid
     */
    @RequestMapping(value = "/productFiltered", method = RequestMethod.POST)
    public String getFilteredProducts(@RequestParam MultiValueMap<String, String> pathVariables, Model model) {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>(pathVariables);
        String s = params.getFirst(CATEGORY_ID_ATTR);
        int categoryId;
        if (s == null) {
            categoryId = 1;
            params.add(CATEGORY_ID_ATTR, "1");
        } else {
            categoryId = Integer.parseInt(s);
        }
        model.addAttribute(LIST_PRODUCTS_ATTR, productService.getFilteredProducts(params));
        model.addAttribute("lastCategoryIdFiltered", categoryId);
        fillModelWithStaticData(model, categoryId);
        return "productForm";
    }

    /**
     * @param categoryName
     * @param model
     * @return view for the category
     */
    @RequestMapping(value = "/category/{categoryName}")
    public String getCategory(@PathVariable String categoryName, Model model) {
        int categoryId = productService.getCategoryIdByName(categoryName);
        fillModelWithStaticData(model, categoryId);
        return getProducts(getMultiValueMap(CATEGORY_ID_ATTR, Integer.toString(categoryId)), model);
    }

    /**
     * @param id    product ID
     * @param model
     * @return view for the one product
     */
    @RequestMapping("/product/{id}")
    public String getProductById(@PathVariable int id, Model model) {
        fillModelWithStaticData(model, DEFAULT_CATEGORY_ID);
        Product product = productService.getProductById(id);
        model.addAttribute("product", product);
        model.addAttribute("listImages", photoService.getProductImagesURLs(id));
        return "oneProduct";
    }

    /**
     * @param model
     * @return admin view for adding products
     */
    @RequestMapping("/admin/addProducts")
    public String getAdminAddProducts(Model model) {
        fillModelWithStaticData(model, ALL_MANUFACTURERS_ID);
        if (!userService.isAdmin()) {
            model.addAttribute(RESULT_ATTR, ACCESS_DENIED_MSG);
            return LOGIN_PAGE;
        }
        return "adminAddProducts";
    }

    /**
     * Creates new products
     *
     * @param pathVariables
     * @param model
     * @return admin view for adding products with result
     */
    @RequestMapping(value = "/admin/addProducts", method = RequestMethod.POST)
    public String addProduct(@RequestParam MultiValueMap<String, String> pathVariables, Model model) {
        fillModelWithStaticData(model, ALL_MANUFACTURERS_ID);
        if (!userService.isAdmin()) {
            model.addAttribute(RESULT_ATTR, ACCESS_DENIED_MSG);
            return LOGIN_PAGE;
        }
        try {
            productService.createProduct(pathVariables);
            model.addAttribute(RESULT_ATTR, "Successful!");
            fillModelWithStaticData(model, ALL_MANUFACTURERS_ID);
        } catch (MissingRequiredFieldsException e) {
            fillModelWithStaticData(model, ALL_MANUFACTURERS_ID);
            model.addAttribute(RESULT_ATTR, "Missing required fields!");
        }
        return "adminAddProducts";
    }

    /**
     * @param model
     * @return admin view for managing categories
     */
    @RequestMapping(value = "/admin/manageCategories")
    public String manageCategories(Model model) {
        fillModelWithStaticData(model, ALL_MANUFACTURERS_ID);
        if (!userService.isAdmin()) {
            model.addAttribute(RESULT_ATTR, ACCESS_DENIED_MSG);
            return LOGIN_PAGE;
        }
        return MANAGE_CATEGORIES_PAGE;
    }

    /**
     * @param categoryName
     * @param model
     * @return admin view for adding category
     */
    @RequestMapping(value = "/admin/addCategory")
    public String addCategory(@RequestParam("categoryName") String categoryName, Model model) {
        if (!userService.isAdmin()) {
            model.addAttribute(RESULT_ATTR, ACCESS_DENIED_MSG);
            return LOGIN_PAGE;
        }
        productService.addCategory(categoryName);
        fillModelWithStaticData(model, ALL_MANUFACTURERS_ID);
        model.addAttribute(RESULT_ATTR, "category added");
        return MANAGE_CATEGORIES_PAGE;
    }

    /**
     * @param categoryId category ID to delete
     * @param model
     * @return admin view for managing categories
     */
    @RequestMapping(value = "/admin/deleteCategory")
    public String deleteCategory(@RequestParam(CATEGORY_ID_ATTR) int categoryId, Model model) {
        if (!userService.isAdmin()) {
            model.addAttribute(RESULT_ATTR, ACCESS_DENIED_MSG);
            return LOGIN_PAGE;
        }
        try {
            productService.deleteCategory(categoryId);
        } catch (CantBeDeletedException e) {
            fillModelWithStaticData(model, ALL_MANUFACTURERS_ID);
            model.addAttribute(RESULT_ATTR, CANT_BE_DELETED_MSG);
            return MANAGE_CATEGORIES_PAGE;
        }
        fillModelWithStaticData(model, ALL_MANUFACTURERS_ID);
        model.addAttribute(RESULT_ATTR, "category deleted");
        return MANAGE_CATEGORIES_PAGE;
    }

    /**
     * @param model
     * @return admin view for managing manufacturers
     */
    @RequestMapping(value = "/admin/manageManufacturers")
    public String manageManufacturers(Model model) {
        fillModelWithStaticData(model, ALL_MANUFACTURERS_ID);
        if (!userService.isAdmin()) {
            model.addAttribute(RESULT_ATTR, ACCESS_DENIED_MSG);
            return LOGIN_PAGE;
        }
        model.addAttribute(LIST_MANUFACTURERS_ATTR, productService.getAllManufacturers());
        return MANAGE_MANUFACTURERS_PAGE;
    }

    /**
     * @param manufacturerName to add
     * @param model
     * @return admin view for adding manufacturer
     */
    @RequestMapping(value = "/admin/addManufacturer")
    public String addManufacturer(@RequestParam("manufacturerName") String manufacturerName, Model model) {
        fillModelWithStaticData(model, ALL_MANUFACTURERS_ID);
        if (!userService.isAdmin()) {
            model.addAttribute(RESULT_ATTR, ACCESS_DENIED_MSG);
            return LOGIN_PAGE;
        }
        productService.addManufacturer(manufacturerName);
        model.addAttribute(RESULT_ATTR, "Manufacturer added");
        model.addAttribute(LIST_MANUFACTURERS_ATTR, productService.getAllManufacturers());
        return MANAGE_MANUFACTURERS_PAGE;
    }

    /**
     * @param manufacturerId to delete
     * @param model
     * @return admin view for adding manufacturer
     */
    @RequestMapping(value = "/admin/deleteManufacturer")
    public String deleteManufacturer(@RequestParam("manufacturerId") int manufacturerId, Model model) {
        fillModelWithStaticData(model, ALL_MANUFACTURERS_ID);
        if (!userService.isAdmin()) {
            model.addAttribute(RESULT_ATTR, ACCESS_DENIED_MSG);
            return LOGIN_PAGE;
        }
        try {
            productService.deleteManufacturer(manufacturerId);
        } catch (CantBeDeletedException e) {
            model.addAttribute(RESULT_ATTR, CANT_BE_DELETED_MSG);
            model.addAttribute(LIST_MANUFACTURERS_ATTR, productService.getAllManufacturers());
            return MANAGE_MANUFACTURERS_PAGE;
        }
        model.addAttribute(RESULT_ATTR, "Manufacturer deleted");
        model.addAttribute(LIST_MANUFACTURERS_ATTR, productService.getAllManufacturers());
        return MANAGE_MANUFACTURERS_PAGE;
    }

    /**
     * @param model
     * @return admin view for managing colors
     */
    @RequestMapping(value = "/admin/manageColors")
    public String manageColors(Model model) {
        fillModelWithStaticData(model, DEFAULT_CATEGORY_ID);
        if (!userService.isAdmin()) {
            model.addAttribute(RESULT_ATTR, ACCESS_DENIED_MSG);
            return LOGIN_PAGE;
        }
        return MANAGE_COLORS_PAGE;
    }

    /**
     * Add a new color
     *
     * @param colorName to add
     * @param model
     * @return admin view for managing colors with result
     */
    @RequestMapping(value = "/admin/addColor")
    public String addColor(@RequestParam("colorName") String colorName, Model model) {
        if (!userService.isAdmin()) {
            model.addAttribute(RESULT_ATTR, ACCESS_DENIED_MSG);
            fillModelWithStaticData(model, DEFAULT_CATEGORY_ID);
            return LOGIN_PAGE;
        }
        productService.addColor(colorName);
        fillModelWithStaticData(model, DEFAULT_CATEGORY_ID);
        model.addAttribute(RESULT_ATTR, "Color added");
        return MANAGE_COLORS_PAGE;
    }

    /**
     * Delete color by id
     *
     * @param colorId to delete
     * @param model
     * @return admin view for managing colors with result
     */
    @RequestMapping(value = "/admin/deleteColor")
    public String deleteColor(@RequestParam("colorId") int colorId, Model model) {
        if (!userService.isAdmin()) {
            fillModelWithStaticData(model, DEFAULT_CATEGORY_ID);
            model.addAttribute(RESULT_ATTR, ACCESS_DENIED_MSG);
            return LOGIN_PAGE;
        }
        try {
            productService.deleteColor(colorId);
        } catch (CantBeDeletedException e) {
            fillModelWithStaticData(model, DEFAULT_CATEGORY_ID);
            model.addAttribute(RESULT_ATTR, CANT_BE_DELETED_MSG);
            return MANAGE_COLORS_PAGE;
        }
        model.addAttribute(RESULT_ATTR, "Color deleted");
        fillModelWithStaticData(model, DEFAULT_CATEGORY_ID);
        return MANAGE_COLORS_PAGE;
    }

    /**
     * get list of products for the specified category Id
     *
     * @param categoryId
     * @param model
     * @return list of products for category
     */
    @RequestMapping(value = "/admin/getProductsForCategory", method = RequestMethod.POST)
    public String getProductsForCategory(@RequestParam(CATEGORY_ID_ATTR) int categoryId, Model model) {
        model.addAttribute(LIST_PRODUCTS_ATTR, productService.getProductsForCategory(categoryId));
        return "adminProductsForCategory";
    }

    /**
     * Create MultiValueMap with one K-V pair
     *
     * @param key
     * @param value
     * @return MultiValueMap with one pair of passed key-value
     */
    private MultiValueMap<String, String> getMultiValueMap(String key, String value) {
        MultiValueMap<String, String> mvm = new LinkedMultiValueMap<>();
        mvm.add(key, value);
        return mvm;
    }

    /**
     * Fill Spring model with exising product categories, colors and manufacturers
     *
     * @param model
     */
    public void fillModelWithStaticData(Model model, int categoryId) {
        model.addAttribute("listCategories", productService.getAllCategories());
        model.addAttribute("listColors", productService.getAllColors());
        if (categoryId == ALL_MANUFACTURERS_ID) {
            model.addAttribute(LIST_MANUFACTURERS_ATTR, productService.getAllManufacturers());
        } else {
            model.addAttribute(LIST_MANUFACTURERS_ATTR, productService.getAllManufacturersForCategory(categoryId));
        }
    }
}
