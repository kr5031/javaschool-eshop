package com.tsystems.javaschool.eshop.controller;

import com.tsystems.javaschool.eshop.model.dto.ProductDTO;
import com.tsystems.javaschool.eshop.model.dto.UserDTO;
import com.tsystems.javaschool.eshop.service.StatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

import static com.tsystems.javaschool.eshop.controller.ProductController.ALL_MANUFACTURERS_ID;

/**
 * Controller used to request statistic info by admin
 */
@Controller
public class StatsController {
    @Autowired
    StatsService statsService;

    @Autowired
    ProductController productController;

    /**
     * @param model
     * @return admin view with sales statistics
     */
    @RequestMapping(value = "/admin/sales")
    public String getAdminSales(Model model) {
        productController.fillModelWithStaticData(model, ALL_MANUFACTURERS_ID);

        List<ProductDTO> topProducts = statsService.getTopProducts(10);
        model.addAttribute("listTopProducts", topProducts);
        model.addAttribute("listNumberOfProductsSold", statsService.getNumberOfSalesForProductsDTO(topProducts));

        List<UserDTO> topUsers = statsService.getTopClients(10);
        model.addAttribute("listTopUsers", topUsers);
        model.addAttribute("listNumberOfOrders", statsService.getNumberOfOrdersForClients(topUsers));

        model.addAttribute("listOrdersLastWeek", statsService.getOrdersForLastWeek());
        model.addAttribute("listOrdersLastMonth", statsService.getOrdersForLastMonth());

        return "adminSales";
    }
}