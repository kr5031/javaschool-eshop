package com.tsystems.javaschool.eshop.controller;

import com.tsystems.javaschool.eshop.exceptions.MissingRequiredFieldsException;
import com.tsystems.javaschool.eshop.exceptions.PasswordsDontMatchException;
import com.tsystems.javaschool.eshop.exceptions.UserAlreadyExistsException;
import com.tsystems.javaschool.eshop.model.User;
import com.tsystems.javaschool.eshop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import java.text.ParseException;

import static com.tsystems.javaschool.eshop.controller.ProductController.ALL_MANUFACTURERS_ID;

/**
 * Controller for working with users
 */
@Controller
public class UserController {
    private static final int ADMIN_ROLE_ID = 1;
    private static final String PROFILE_PAGE = "profile";
    private static final String LOGIN_PAGE = "login";
    private static final String REGISTER_PAGE = "register";
    private static final String TO_REDIRECT_AFTER_ATTR = "toRedirectAfter";
    private static final String RESULT_ATTR = "result";
    @Autowired
    private UserService userService;
    @Autowired
    private HttpSession httpSession;
    @Autowired
    private ProductController productController;

    /**
     * @param model
     * @return personal profile page
     */
    @RequestMapping("/profile")
    public String getProfile(Model model) {
        User user = (User) httpSession.getAttribute("user");
        productController.fillModelWithStaticData(model, ALL_MANUFACTURERS_ID);
        if (user != null) {
            model.addAttribute("user", user);
            return PROFILE_PAGE;
        } else {
            return LOGIN_PAGE;
        }
    }

    /**
     * Login with the credentials passed
     *
     * @param pathVariables with login-password pair
     * @param model
     * @return personal profile pview
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @Transactional
    public String login(@RequestParam MultiValueMap<String, String> pathVariables, Model model) {
        productController.fillModelWithStaticData(model, ALL_MANUFACTURERS_ID);
        String login = pathVariables.getFirst(LOGIN_PAGE);
        String pwd = pathVariables.getFirst("password");
        String redirectTo = pathVariables.getFirst(TO_REDIRECT_AFTER_ATTR);
        if (redirectTo == null || redirectTo.isEmpty()) {
            redirectTo = "/";
        }
        User user = userService.getUserByLogin(login);
        if (user != null && pwd != null && pwd.equals(user.getPassword())) {
            httpSession.setAttribute("user", user);
            if (user.getUserRole().getRoleId() == ADMIN_ROLE_ID) {
                httpSession.setAttribute("isAdmin", true);
            }
            return "redirect:" + redirectTo;
        } else {
            model.addAttribute("wrongCredentials", true);
            model.addAttribute(TO_REDIRECT_AFTER_ATTR, redirectTo);
            return getProfile(model);
        }
    }

    /**
     * To logout
     *
     * @param model
     * @return main view
     */
    @RequestMapping(value = "/logout")
    public String logout(Model model) {
        httpSession.setAttribute("user", null);
        httpSession.setAttribute("isAdmin", null);
        productController.fillModelWithStaticData(model, ALL_MANUFACTURERS_ID);
        return "redirect:/";
    }

    /**
     * Update user's profile
     *
     * @param pathVariables
     * @param model
     * @return personal view
     * @throws ParseException wrong parameters passed
     */
    @RequestMapping(value = "/updateUser", method = RequestMethod.POST)
    public String updateUser(@RequestParam MultiValueMap<String, String> pathVariables, Model model) {
        productController.fillModelWithStaticData(model, ALL_MANUFACTURERS_ID);
        if (isLoggedIn()) {
            User user = (User) httpSession.getAttribute("user");
            try {
                userService.saveOrUpdateUser(pathVariables, user);
                httpSession.setAttribute("user", user);
                model.addAttribute("user", user);
                return PROFILE_PAGE;
            } catch (MissingRequiredFieldsException e) {
                model.addAttribute("missingRequiredFields", true);
            } catch (PasswordsDontMatchException e) {
                model.addAttribute("passwordsDontMatch", true);
            }
            model.addAttribute("user", user);
            return PROFILE_PAGE;
        } else {
            return LOGIN_PAGE;
        }
    }

    /**
     * To register
     *
     * @param model
     * @return register view
     */
    @RequestMapping(value = "/register")
    public String register(Model model) {
        productController.fillModelWithStaticData(model, ALL_MANUFACTURERS_ID);
        if (!isLoggedIn()) {
            model.addAttribute(TO_REDIRECT_AFTER_ATTR, "/cart");
            return REGISTER_PAGE;
        } else {
            return "redirect:/";
        }
    }

    /**
     * Pass parameters to register
     *
     * @param pathVariables
     * @param model
     * @return register view with result
     */
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String registerUser(@RequestParam MultiValueMap<String, String> pathVariables, Model model) {
        productController.fillModelWithStaticData(model, ALL_MANUFACTURERS_ID);
        if (isLoggedIn()) {
            model.addAttribute(RESULT_ATTR, "You are already logged in!");
            return REGISTER_PAGE;
        }
        User user = null;

        try {
            user = userService.createUser(pathVariables);
        } catch (PasswordsDontMatchException e) {
            model.addAttribute(RESULT_ATTR, "Passwords don't match!");
        } catch (MissingRequiredFieldsException e) {
            model.addAttribute(RESULT_ATTR, "Missing required fields!");
        } catch (UserAlreadyExistsException e) {
            model.addAttribute(RESULT_ATTR, "User already exists!");
        } finally {
            if (!model.containsAttribute(RESULT_ATTR)) {
                model.addAttribute(RESULT_ATTR, "Registration successful!");
            }
            httpSession.setAttribute("user", user);
            model.addAttribute("user", user);
        }
        return REGISTER_PAGE;
    }

    /**
     * To check whether the user is authenticated
     *
     * @return
     */
    public boolean isLoggedIn() {
        User user = (User) httpSession.getAttribute("user");
        return user != null;
    }
}
