package com.tsystems.javaschool.eshop.controller.rest;

import com.tsystems.javaschool.eshop.model.dto.ProductDTO;
import com.tsystems.javaschool.eshop.model.dto.UserDTO;
import com.tsystems.javaschool.eshop.service.JmsService;
import com.tsystems.javaschool.eshop.service.StatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Controller used for requesting stats over REST
 */
@RestController
public class StatsControllerRest {
    @Autowired
    StatsService statsService;

    @Autowired
    JmsService jmsService;

    /**
     * @param n number of top products
     * @return collection of DTO for products (preview photos injected within DTO)
     */
    @RequestMapping("/rest/prod/{n}")
    public List<ProductDTO> getTopNproducts(@PathVariable int n) {
        return statsService.getTopProducts(n);
    }

    /**
     * @param n number of top clients
     * @return collection of DTO for users
     */
    @RequestMapping("/rest/client/{n}")
    public List<UserDTO> getTopNclients(@PathVariable int n) {
        return statsService.getTopClients(n);
    }

    /**
     * imitates event of statistics update
     */
    @RequestMapping("/rest/update")
    public void update() {
        jmsService.orderCreated();
    }
}