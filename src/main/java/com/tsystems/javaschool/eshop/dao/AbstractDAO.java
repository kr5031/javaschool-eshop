package com.tsystems.javaschool.eshop.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Abstract class for storing SessionFactory
 */
public abstract class AbstractDAO {
    @Autowired
    private SessionFactory sessionFactory;

    /**
     * Get a Session from the SessionFactory
     *
     * @return Session
     */
    protected Session getSession() {
        return sessionFactory.getCurrentSession();
    }

    /**
     * Save an entity to the database
     *
     * @param entity to save
     */
    public void persist(Object entity) {
        getSession().persist(entity);
    }

    /**
     * Delete an entity from the database
     *
     * @param entity to delete
     */
    public void delete(Object entity) {
        getSession().delete(entity);
    }
}