package com.tsystems.javaschool.eshop.dao;

import com.tsystems.javaschool.eshop.model.Address;
import com.tsystems.javaschool.eshop.model.City;
import com.tsystems.javaschool.eshop.model.Country;
import com.tsystems.javaschool.eshop.model.DeliveryMethod;
import com.tsystems.javaschool.eshop.model.Order;
import com.tsystems.javaschool.eshop.model.OrderStatus;
import com.tsystems.javaschool.eshop.model.PaymentMethod;
import com.tsystems.javaschool.eshop.model.ProductInOrder;
import com.tsystems.javaschool.eshop.model.User;

import java.util.List;

public interface OrderDAO {
    /**
     * get Order by its' ID
     *
     * @param id order ID
     * @return Order object
     */
    Order getOrderById(int id);

    /**
     * Get all order of the user
     *
     * @param user
     * @return List of Orders for the User
     */
    List<Order> getOrdersOfUser(User user);

    /**
     * Get all order in the DB (for admin only)
     *
     * @return List of Orders
     */
    List<Order> getAllOrders();

    /**
     * Get statuses of all created Orders
     *
     * @return List of statuses for the Orders
     */
    List<OrderStatus> getAllOrdersStatuses();

    /**
     * Get order status by its' id
     *
     * @param statusId order status ID
     * @return OrderStatus object
     */
    OrderStatus getOrderStatusById(int statusId);

    /**
     * Save new or change existing order
     *
     * @param order changed
     */
    void saveOrUpdateOrder(Order order);

    /**
     * Save new or change existing address
     *
     * @param address changed
     */
    void saveOrUpdateAddress(Address address);

    /**
     * Save new or change existing Country
     *
     * @param country changed
     */
    void saveOrUpdateCountry(Country country);

    /**
     * Save new or change existing City
     *
     * @param city changed
     */
    void saveOrUpdateCity(City city);

    /**
     * Save new or change existing ProductInOrder
     *
     * @param productInOrder changed
     */
    void saveOrUpdateProductInOrder(ProductInOrder productInOrder);

    /**
     * Get all products in the particular order
     *
     * @param orderId
     * @return List of ProductInOrder
     */
    List<ProductInOrder> getProductsInOrderByOrderId(int orderId);

    /**
     * Get all existing delivery methods
     *
     * @return List of DeliveryMethod
     */
    List<DeliveryMethod> getAllDeliveryMethods();

    /**
     * Get all existing payment methods
     *
     * @return List of PaymentMethod
     */
    List<PaymentMethod> getAllPaymentMethods();

    /**
     * Get delivery method by its' ID
     *
     * @param deliveryMethodId
     * @return DeliveryMethod object
     */
    DeliveryMethod getDeliveryMethodById(int deliveryMethodId);

    /**
     * Get payment method by its' ID
     *
     * @param paymentMethodId
     * @return PaymentMethod object
     */
    PaymentMethod getPaymentMethodById(int paymentMethodId);

    /**
     * Get city by its' name and country
     *
     * @param cityName
     * @param countryName
     * @return City object
     */
    City getCityByName(String cityName, String countryName);

    /**
     * Get country by its' name
     *
     * @param countryName
     * @return Country object
     */
    Country getCountryByName(String countryName);

    /**
     * Get Country by its' name
     *
     * @param countryName
     * @return Country object
     */
    int getCountryIdByName(String countryName);

    /**
     * Get Address object
     *
     * @param city
     * @param postCode
     * @param street
     * @param house
     * @param apartments
     * @return Address
     */
    Address getAddress(City city, int postCode, String street, int house, int apartments);

    /**
     * Get OrderStatus by its' name
     *
     * @param orderStatusName
     * @return OrderStatus object
     */
    OrderStatus getOrderStatus(String orderStatusName);
}
