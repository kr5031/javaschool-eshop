package com.tsystems.javaschool.eshop.dao;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface PhotoDAO {
    /**
     * get photo by its' ID
     *
     * @param photoId
     * @return image as byte array
     */
    byte[] getPhotoById(int photoId);

    /**
     * get preview photo by productID
     *
     * @param productId
     * @return image as byte array
     */
    byte[] getPreviewPhoto(int productId);

    /**
     * Save uploaded photos
     *
     * @param files     array of image files
     * @param productId associated with the images
     * @throws IOException
     */
    void savePhotos(MultipartFile[] files, int productId) throws IOException;

    /**
     * Get all image IDs for a product
     *
     * @param productId
     * @return list of photoID associated with the product
     */
    List<Integer> getListPhotosByProductId(int productId);
}
