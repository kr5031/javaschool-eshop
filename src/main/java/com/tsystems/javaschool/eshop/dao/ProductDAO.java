package com.tsystems.javaschool.eshop.dao;

import com.tsystems.javaschool.eshop.exceptions.CantBeDeletedException;
import com.tsystems.javaschool.eshop.exceptions.MissingRequiredFieldsException;
import com.tsystems.javaschool.eshop.model.Category;
import com.tsystems.javaschool.eshop.model.Color;
import com.tsystems.javaschool.eshop.model.Manufacturer;
import com.tsystems.javaschool.eshop.model.Photo;
import com.tsystems.javaschool.eshop.model.Product;
import org.springframework.util.MultiValueMap;

import java.util.Collection;
import java.util.List;

public interface ProductDAO {
    void saveProduct(Product product);

    void addCategory(Category category);

    void deleteCategory(int categoryId) throws CantBeDeletedException;

    List<Product> getAllProducts();

    List<Product> getFilteredProducts(MultiValueMap<String, String> multiValueMap);

    Product getProductById(int productId);

    void updateProduct(Product product);

    List<Photo> getProductImages(int productId);

    Manufacturer getManufacturerById(int id);

    List<Manufacturer> getManufacturersListByIds(Collection<Integer> ids);

    Category getCategoryById(int categoryId);

    Color getColorById(int colorId);

    List<Manufacturer> getAllManufacturersForCategory(int categoryId);

    String getFilterFileForCategory(int categoryId);

    Double getPriceForProductId(int productId);

    List<Category> getAllCategories();

    List<Color> getAllColors();

    List<Manufacturer> getAllManufacturers();

    void createProduct(Product product) throws MissingRequiredFieldsException;

    int getCategoryIdByName(String categoryName);

    void addManufacturer(Manufacturer manufacturer);

    void deleteManufacturer(Manufacturer manufacturer) throws CantBeDeletedException;

    void addColor(Color color);

    void deleteColor(Color color) throws CantBeDeletedException;

    List<Product> getProductsForCategory(int categoryId);
}