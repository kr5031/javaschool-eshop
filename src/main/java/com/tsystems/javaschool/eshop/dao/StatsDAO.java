package com.tsystems.javaschool.eshop.dao;

import com.tsystems.javaschool.eshop.model.Order;
import com.tsystems.javaschool.eshop.model.Product;
import com.tsystems.javaschool.eshop.model.User;
import com.tsystems.javaschool.eshop.model.dto.UserDTO;

import java.util.List;

public interface StatsDAO {
    /**
     * get list of top clients by number of orders
     *
     * @param number of top records returned
     * @return list of users
     */
    List<User> getTopClients(int number);

    /**
     * Determine number of orders created by the user
     *
     * @param user
     * @return number of orders created by the user
     */
    long getNumberOfOrdersForClient(User user);

    /**
     * Determine number of orders created by the user by UserDTO
     *
     * @param user
     * @return number of orders created by the user by UserDTO
     */
    long getNumberOfOrdersForClient(UserDTO user);

    /**
     * Get top of products with maximum number of sales
     *
     * @param number number of top records returned
     * @return list of products with maximum number of sales
     */
    List<Object[]> getTopProducts(int number);

    /**
     * Get number of sales for the product
     *
     * @param product
     * @return number of sales for the product
     */
    long getNumberOfSalesForProduct(Product product);

    /**
     * Get all orders for last wekk
     *
     * @return orders for last wekk
     */
    List<Order> getOrdersForLastWeek();

    /**
     * Get all orders for last month
     *
     * @return all orders for last month
     */
    List<Order> getOrdersForLastMonth();
}
