package com.tsystems.javaschool.eshop.dao;

import com.tsystems.javaschool.eshop.model.Role;
import com.tsystems.javaschool.eshop.model.User;

public interface UserDAO {
    /**
     * Get user by the login
     *
     * @param login
     * @return user by the login
     */
    User getUserByLogin(String login);

    /**
     * Get user by the e-mail
     *
     * @param email
     * @return user by the e-mail
     */
    User getUserByEmail(String email);

    /**
     * Save the user (existing or new)
     *
     * @param user
     */
    void saveOrUpdateUser(User user);

    /**
     * Get user role object by its' ID
     *
     * @param id
     * @return role object
     */
    Role getRoleById(int id);

    /**
     * Get user by his ID
     *
     * @param userId
     * @return user by his ID
     */
    User getUserById(int userId);
}
