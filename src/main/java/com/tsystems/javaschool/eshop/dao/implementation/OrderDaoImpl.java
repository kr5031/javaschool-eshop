package com.tsystems.javaschool.eshop.dao.implementation;

import com.tsystems.javaschool.eshop.dao.AbstractDAO;
import com.tsystems.javaschool.eshop.dao.OrderDAO;
import com.tsystems.javaschool.eshop.model.Address;
import com.tsystems.javaschool.eshop.model.City;
import com.tsystems.javaschool.eshop.model.Country;
import com.tsystems.javaschool.eshop.model.DeliveryMethod;
import com.tsystems.javaschool.eshop.model.Order;
import com.tsystems.javaschool.eshop.model.OrderStatus;
import com.tsystems.javaschool.eshop.model.PaymentMethod;
import com.tsystems.javaschool.eshop.model.ProductInOrder;
import com.tsystems.javaschool.eshop.model.User;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository("orderDAO")
@Transactional
public class OrderDaoImpl extends AbstractDAO implements OrderDAO {
    private static final Logger logger = Logger.getLogger(OrderDaoImpl.class.getName());
    private static final String ORDER_ID_ATTR = "orderId";

    @Override
    public Order getOrderById(int id) {
        Criteria criteria = getSession().createCriteria(Order.class);
        criteria.add(Restrictions.eq(ORDER_ID_ATTR, id));
        return (Order) criteria.uniqueResult();
    }

    @Override
    public List<Order> getOrdersOfUser(User user) {
        Criteria criteria = getSession().createCriteria(Order.class).addOrder(org.hibernate.criterion.Order.desc(ORDER_ID_ATTR));
        criteria.add(Restrictions.eq("user", user));
        return (List<Order>) criteria.list();
    }

    @Override
    public void saveOrUpdateOrder(Order order) {
        getSession().saveOrUpdate(order);
        logger.info("order ID " + order.getOrderId() + " was saved");
    }

    @Override
    public void saveOrUpdateAddress(Address address) {
        getSession().saveOrUpdate(address);
        logger.info("Address ID " + address.getAddressId() + " was saved");
    }

    @Override
    public void saveOrUpdateCountry(Country country) {
        getSession().saveOrUpdate(country);
    }

    @Override
    public void saveOrUpdateCity(City city) {
        getSession().saveOrUpdate(city);
    }

    @Override
    public void saveOrUpdateProductInOrder(ProductInOrder productInOrder) {
        getSession().saveOrUpdate(productInOrder);
    }

    @Override
    public List<ProductInOrder> getProductsInOrderByOrderId(int orderId) {
        Criteria criteria = getSession().createCriteria(ProductInOrder.class);
        Order order = getOrderById(orderId);
        criteria.add(Restrictions.eq("order", order));
        return (List<ProductInOrder>) criteria.list();
    }

    @Override
    public List<DeliveryMethod> getAllDeliveryMethods() {
        Criteria criteria = getSession().createCriteria(DeliveryMethod.class);
        return (List<DeliveryMethod>) criteria.list();
    }

    @Override
    public DeliveryMethod getDeliveryMethodById(int deliveryMethodId) {
        Criteria criteria = getSession().createCriteria(DeliveryMethod.class);
        criteria.add(Restrictions.eq("deliveryMethodId", deliveryMethodId));
        return (DeliveryMethod) criteria.uniqueResult();
    }

    @Override
    public PaymentMethod getPaymentMethodById(int paymentMethodId) {
        Criteria criteria = getSession().createCriteria(PaymentMethod.class);
        criteria.add(Restrictions.eq("paymentMethodId", paymentMethodId));
        return (PaymentMethod) criteria.uniqueResult();
    }

    @Override
    public List<PaymentMethod> getAllPaymentMethods() {
        Criteria criteria = getSession().createCriteria(PaymentMethod.class);
        return (List<PaymentMethod>) criteria.list();
    }

    @Override
    public int getCountryIdByName(String countryName) {
        Criteria criteria = getSession().createCriteria(Country.class);
        criteria.add(Restrictions.eq("countryName", countryName));
        return ((Country) criteria.uniqueResult()).getCountryId();
    }

    @Override
    public City getCityByName(String cityName, String countryName) {
        Criteria criteria = getSession().createCriteria(City.class);
        criteria.add(Restrictions.eq("cityName", cityName));
        Country country = getCountryByName(countryName);
        criteria.add(Restrictions.eq("country", country));
        City city = (City) criteria.uniqueResult();
        if (city == null) {
            city = new City();
            city.setCountry(country);
            city.setCityName(cityName);
            saveOrUpdateCity(city);
        }
        return city;
    }

    @Override
    public Country getCountryByName(String countryName) {
        Criteria criteria = getSession().createCriteria(Country.class);
        criteria.add(Restrictions.eq("countryName", countryName));
        Country country = (Country) criteria.uniqueResult();
        if (country == null) {
            country = new Country();
            country.setCountryName(countryName);
            saveOrUpdateCountry(country);
        }
        return country;
    }

    @Override
    public Address getAddress(City city, int postCode, String street, int house, int apartments) {
        Criteria criteria = getSession().createCriteria(Address.class);
        criteria.add(Restrictions.eq("city", city));
        criteria.add(Restrictions.eq("postCode", postCode));
        criteria.add(Restrictions.eq("street", street));
        criteria.add(Restrictions.eq("house", house));
        criteria.add(Restrictions.eq("apartments", apartments));
        Address result = (Address) criteria.uniqueResult();
        if (result == null) {
            result = new Address();
            result.setCity(city);
            result.setPostCode(postCode);
            result.setStreet(street);
            result.setHouse(house);
            result.setApartments(apartments);
            saveOrUpdateAddress(result);
        }
        return result;
    }

    @Override
    public OrderStatus getOrderStatus(String orderStatusName) {
        Criteria criteria = getSession().createCriteria(OrderStatus.class);
        criteria.add(Restrictions.eq("orderStatusName", orderStatusName));
        return (OrderStatus) criteria.uniqueResult();
    }

    @Override
    public List<Order> getAllOrders() {
        Criteria criteria = getSession().createCriteria(Order.class).addOrder(org.hibernate.criterion.Order.desc(ORDER_ID_ATTR));
        return (List<Order>) criteria.list();
    }

    @Override
    public List<OrderStatus> getAllOrdersStatuses() {
        Criteria criteria = getSession().createCriteria(OrderStatus.class);
        return (List<OrderStatus>) criteria.list();
    }

    @Override
    public OrderStatus getOrderStatusById(int statusId) {
        Criteria criteria = getSession().createCriteria(OrderStatus.class);
        criteria.add(Restrictions.eq("orderStatusId", statusId));
        return (OrderStatus) criteria.uniqueResult();
    }
}
