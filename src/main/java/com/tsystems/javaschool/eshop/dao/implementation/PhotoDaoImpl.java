package com.tsystems.javaschool.eshop.dao.implementation;

import com.tsystems.javaschool.eshop.dao.AbstractDAO;
import com.tsystems.javaschool.eshop.dao.PhotoDAO;
import com.tsystems.javaschool.eshop.model.Photo;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Transactional
@Repository("photoDAO")
public class PhotoDaoImpl extends AbstractDAO implements PhotoDAO {
    private static final String PRODUCT_ID_ATTR = "productId";

    @Override
    public byte[] getPhotoById(int photoId) {
        Criteria criteria = getSession().createCriteria(Photo.class);
        criteria.add(Restrictions.eq("photoId", photoId));
        Photo photo = (Photo) criteria.uniqueResult();
        if (photo == null) {
            return getNoPhotoAvailable();
        }
        return photo.getImage();
    }

    private byte[] getNoPhotoAvailable() {
        Criteria criteria = getSession().createCriteria(Photo.class);
        criteria.add(Restrictions.eq(PRODUCT_ID_ATTR, 0));
        Photo photo = (Photo) criteria.uniqueResult();
        return photo == null? new byte[] {0} : photo.getImage();
    }

    @Override
    public byte[] getPreviewPhoto(int productId) {
        Criteria criteria = getSession().createCriteria(Photo.class);
        criteria.add(Restrictions.eq(PRODUCT_ID_ATTR, productId));
        List<Photo> photo = (List<Photo>) criteria.list();
        if (photo == null || photo.isEmpty()) {
            return getNoPhotoAvailable();
        } else {
            return photo.get(0).getImage();
        }
    }

    @Override
    public void savePhotos(MultipartFile[] multipartFiles, int productId) throws IOException {
        for (MultipartFile file : multipartFiles) {
            if (file == null || file.getBytes().length == 0) {
                continue;
            }
            byte[] bytes = file.getBytes();
            Photo p = new Photo();
            p.setProductId(productId);
            p.setImage(bytes);
            getSession().saveOrUpdate(p);
        }
    }

    @Override
    public List<Integer> getListPhotosByProductId(int productId) {
        Criteria criteria = getSession().createCriteria(Photo.class);
        criteria.add(Restrictions.eq(PRODUCT_ID_ATTR, productId));
        List<Photo> photos = (List<Photo>) criteria.list();
        List<Integer> result = new ArrayList<>(photos.size());
        for (Photo photo : photos) {
            result.add(photo.getPhotoId());
        }
        return result;
    }

}
