package com.tsystems.javaschool.eshop.dao.implementation;

import com.tsystems.javaschool.eshop.exceptions.CantBeDeletedException;
import com.tsystems.javaschool.eshop.exceptions.MissingRequiredFieldsException;
import com.tsystems.javaschool.eshop.dao.AbstractDAO;
import com.tsystems.javaschool.eshop.dao.ProductDAO;
import com.tsystems.javaschool.eshop.model.Category;
import com.tsystems.javaschool.eshop.model.Color;
import com.tsystems.javaschool.eshop.model.Manufacturer;
import com.tsystems.javaschool.eshop.model.Photo;
import com.tsystems.javaschool.eshop.model.Product;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@Repository("productDAO")
@Transactional
public class ProductDaoImpl extends AbstractDAO implements ProductDAO {
    private static final Logger logger = Logger.getLogger(ProductDaoImpl.class.getName());

    private static final String PRODUCT_ID_ATTR = "productId";
    private static final String CATEGORY_ID_ATTR = "categoryId";
    private static final String MANUFACTURER_ID_ATTR = "manufacturerId";
    private static final String CATEGORY_ATTR = "category";
    private static final String WAS_DELETED_MSG = " was deleted";
    private static final String WITH_ID_MSG = " with ID ";

    public void saveProduct(Product product) {
        persist(product);
    }

    public List<Product> getAllProducts() {
        Criteria criteria = getSession().createCriteria(Product.class);
        return (List<Product>) criteria.list();
    }

    public Product getProductById(int productId) {
        Criteria criteria = getSession().createCriteria(Product.class);
        criteria.add(Restrictions.eq(PRODUCT_ID_ATTR, productId));
        return (Product) criteria.uniqueResult();
    }

    public void updateProduct(Product product) {
        getSession().update(product);
    }

    @Override
    public List<Photo> getProductImages(int productId) {
        Criteria criteria = getSession().createCriteria(Photo.class);
        criteria.add(Restrictions.eq(PRODUCT_ID_ATTR, productId));
        return (List<Photo>) criteria.list();
    }

    @Override
    public List<Product> getFilteredProducts(MultiValueMap<String, String> multiValueMap) {
        if (multiValueMap == null || multiValueMap.size() == 0) {
            return getAllProducts();
        } else {
            Criteria criteria = getSession().createCriteria(Product.class);
            Conjunction conjunction = getConjunction(multiValueMap);
            criteria.add(conjunction);
            return (List<Product>) criteria.list();
        }
    }

    private Conjunction getConjunction(MultiValueMap<String, String> multiValueMap) {
        Conjunction conjunction = Restrictions.conjunction();
        for (Map.Entry<String, List<String>> entry : multiValueMap.entrySet()) {
            String key = entry.getKey();
            switch (key) {
                case CATEGORY_ID_ATTR:
                    addCategoryToConjunction(multiValueMap, conjunction, key);
                    break;

                case MANUFACTURER_ID_ATTR:
                    addManufacturerToConjunction(multiValueMap, conjunction, key);
                    break;

                case "memory":
                    addMemoryToConjunction(multiValueMap, conjunction, key);
                    break;

                case "screenFrom":
                    addScreenToConjunction(multiValueMap, conjunction, key);
                    break;

                case "priceFrom":
                    addPriceToConjunction(multiValueMap, conjunction, key);
                    break;

                default:
            }
        }
        return conjunction;
    }

    private void addPriceToConjunction(MultiValueMap<String, String> multiValueMap, Conjunction conjunction, String key) {
        String inputValue = multiValueMap.getFirst(key);
        double priceFrom;
        double priceTo;
        if (inputValue.isEmpty()) {
            priceFrom = 0;
        } else {
            priceFrom = Double.parseDouble(multiValueMap.getFirst(key));
        }
        if (multiValueMap.getFirst("priceTo").isEmpty()) {
            priceTo = 999999;
        } else {
            priceTo = Double.parseDouble(multiValueMap.getFirst("priceTo"));
        }
        conjunction.add(Restrictions.between("price", priceFrom, priceTo));
    }

    private void addScreenToConjunction(MultiValueMap<String, String> multiValueMap, Conjunction conjunction, String key) {
        String inputValue = multiValueMap.getFirst(key);
        double screenFrom;
        double screenTo;
        if (inputValue.isEmpty()) {
            screenFrom = 0;
        } else {
            screenFrom = Double.parseDouble(multiValueMap.getFirst(key));
        }
        if (multiValueMap.getFirst("screenTo").isEmpty()) {
            screenTo = 999999;
        } else {
            screenTo = Double.parseDouble(multiValueMap.getFirst("screenTo"));
        }
        conjunction.add(Restrictions.between("screen", screenFrom, screenTo));
    }

    private void addMemoryToConjunction(MultiValueMap<String, String> multiValueMap, Conjunction conjunction, String key) {
        List<Integer> valuesConverted = convertToIntList(multiValueMap.get(key));
        conjunction.add(Restrictions.in("memory", valuesConverted));
    }

    private void addManufacturerToConjunction(MultiValueMap<String, String> multiValueMap, Conjunction conjunction, String key) {
        List<Integer> valuesConverted = convertToIntList(multiValueMap.get(key));
        conjunction.add(Restrictions.in("manufacturer", getManufacturersListByIds(valuesConverted)));
    }

    private void addCategoryToConjunction(MultiValueMap<String, String> multiValueMap, Conjunction conjunction, String key) {
        conjunction.add(Restrictions.eq(CATEGORY_ATTR, getCategoryById(Integer.parseInt
                (multiValueMap.getFirst(key)))));
    }

    private List<Integer> convertToIntList(List<String> strings) {
        List<Integer> result = new ArrayList<>();
        for (String s : strings) {
            result.add(Integer.parseInt(s));
        }
        return result;
    }

    @Override
    public Manufacturer getManufacturerById(int id) {
        Criteria criteria = getSession().createCriteria(Manufacturer.class);
        criteria.add(Restrictions.eq(MANUFACTURER_ID_ATTR, id));
        return (Manufacturer) criteria.uniqueResult();
    }

    @Override
    public Category getCategoryById(int id) {
        Criteria criteria = getSession().createCriteria(Category.class);
        criteria.add(Restrictions.eq(CATEGORY_ID_ATTR, id));
        return (Category) criteria.uniqueResult();
    }

    @Override
    public List<Manufacturer> getManufacturersListByIds(Collection<Integer> ids) {
        Criteria criteria = getSession().createCriteria(Manufacturer.class);
        Integer[] id = ids.toArray(new Integer[ids.size()]);
        criteria.add(Restrictions.in(MANUFACTURER_ID_ATTR, id));
        return (List<Manufacturer>) criteria.list();
    }

    @Override
    public List<Manufacturer> getAllManufacturersForCategory(int categoryId) {
        Criteria criteriaProducts = getSession().createCriteria(Product.class);
        Criteria criteriaManufacturers = getSession().createCriteria(Manufacturer.class);
        Category category = getCategoryById(categoryId);
        criteriaProducts.add(Restrictions.eq(CATEGORY_ATTR, category));
        List<Product> productsInCategory = (List<Product>) criteriaProducts.list();
        if (productsInCategory.isEmpty()) {
            return new ArrayList<>();
        }
        Disjunction disjunction = Restrictions.disjunction();

        for (Product product : productsInCategory) {
            disjunction.add(Restrictions.eq(MANUFACTURER_ID_ATTR, product.getManufacturer().getManufacturerId()));
        }

        criteriaManufacturers.add(disjunction);

        return (List<Manufacturer>) criteriaManufacturers.list();
    }

    @Override
    public String getFilterFileForCategory(int categoryId) {
        Criteria criteria = getSession().createCriteria(Category.class);
        criteria.add(Restrictions.eq(CATEGORY_ID_ATTR, categoryId));
        return ((Category) criteria.uniqueResult()).getFilterFile();
    }

    @Override
    public Double getPriceForProductId(int productId) {
        Criteria criteria = getSession().createCriteria(Product.class);
        criteria.add(Restrictions.eq(PRODUCT_ID_ATTR, productId));
        return ((Product) criteria.uniqueResult()).getPrice();
    }

    @Override
    public List<Category> getAllCategories() {
        Criteria criteria = getSession().createCriteria(Category.class);
        return (List<Category>) criteria.list();
    }

    @Override
    public List<Color> getAllColors() {
        Criteria criteria = getSession().createCriteria(Color.class);
        return (List<Color>) criteria.list();
    }

    @Override
    public List<Manufacturer> getAllManufacturers() {
        Criteria criteria = getSession().createCriteria(Manufacturer.class);
        return (List<Manufacturer>) criteria.list();
    }

    @Override
    public Color getColorById(int colorId) {
        Criteria criteria = getSession().createCriteria(Color.class);
        criteria.add(Restrictions.eq("colorId", colorId));
        return (Color) criteria.uniqueResult();
    }

    @Override
    public void createProduct(Product product) throws MissingRequiredFieldsException {
        getSession().saveOrUpdate(product);
        logger.info("Product " + product.getProductId() + " " + product.getDescription() + " was created");

    }

    @Override
    public int getCategoryIdByName(String categoryName) {
        Criteria criteria = getSession().createCriteria(Category.class);
        criteria.add(Restrictions.eq("categoryName", categoryName));
        Category result = (Category) criteria.uniqueResult();
        return result.getCategoryId();
    }

    @Override
    public void addCategory(Category category) {
        getSession().saveOrUpdate(category);
    }

    @Override
    public void deleteCategory(int categoryId) throws CantBeDeletedException {
        Criteria criteria = getSession().createCriteria(Category.class);
        criteria.add(Restrictions.eq(CATEGORY_ID_ATTR, categoryId));
        Category toDelete = (Category) criteria.uniqueResult();

        Criteria criteriaProducts = getSession().createCriteria(Product.class);
        criteriaProducts.add(Restrictions.eq("category", toDelete));
        List<Product> associatedProducts = (List<Product>) criteriaProducts.list();
        if (!associatedProducts.isEmpty()) {
            throw new CantBeDeletedException();
        } else if (toDelete != null) {
            logger.info("Category " + toDelete.getCategoryName() + WITH_ID_MSG + toDelete.getCategoryId() + WAS_DELETED_MSG);
            getSession().delete(toDelete);
        }
    }

    @Override
    public void addManufacturer(Manufacturer manufacturer) {
        getSession().saveOrUpdate(manufacturer);
        logger.info("Manufacturer " + manufacturer.getManufacturerName() + " was created with ID " + manufacturer.getManufacturerId());
    }

    @Override
    public void addColor(Color color) {
        getSession().saveOrUpdate(color);
    }

    @Override
    public void deleteColor(Color color) throws CantBeDeletedException {
        Criteria criteria = getSession().createCriteria(Product.class);
        criteria.add(Restrictions.eq("color", color));
        List<Product> associatedProducts = (List<Product>) criteria.list();
        if (!associatedProducts.isEmpty()) {
            throw new CantBeDeletedException();
        } else if (color != null) {
            logger.info("Color " + color.getColorName() + WITH_ID_MSG + color.getColorId() + WAS_DELETED_MSG);
            getSession().delete(color);
        }
    }

    @Override
    public void deleteManufacturer(Manufacturer manufacturer) throws CantBeDeletedException {
        Criteria criteria = getSession().createCriteria(Product.class);
        criteria.add(Restrictions.eq("manufacturer", manufacturer));
        List<Product> associatedProducts = (List<Product>) criteria.list();
        if (!associatedProducts.isEmpty()) {
            throw new CantBeDeletedException();
        } else if (manufacturer != null) {
            logger.info("Manufacturer " + manufacturer.getManufacturerName() + WITH_ID_MSG + manufacturer.getManufacturerId() + WAS_DELETED_MSG);
            getSession().delete(manufacturer);
        }
    }

    @Override
    public List<Product> getProductsForCategory(int categoryId) {
        Criteria criteriaCategory = getSession().createCriteria(Category.class);
        criteriaCategory.add(Restrictions.eq(CATEGORY_ID_ATTR, categoryId));
        Category category = (Category) criteriaCategory.uniqueResult();

        Criteria criteriaProduct = getSession().createCriteria(Product.class);
        criteriaProduct.add(Restrictions.eq(CATEGORY_ATTR, category));
        return (List<Product>) criteriaProduct.list();
    }
}