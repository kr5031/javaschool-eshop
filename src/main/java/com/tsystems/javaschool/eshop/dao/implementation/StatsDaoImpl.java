package com.tsystems.javaschool.eshop.dao.implementation;

import com.tsystems.javaschool.eshop.dao.AbstractDAO;
import com.tsystems.javaschool.eshop.dao.ProductDAO;
import com.tsystems.javaschool.eshop.dao.StatsDAO;
import com.tsystems.javaschool.eshop.dao.UserDAO;
import com.tsystems.javaschool.eshop.model.Order;
import com.tsystems.javaschool.eshop.model.Product;
import com.tsystems.javaschool.eshop.model.ProductInOrder;
import com.tsystems.javaschool.eshop.model.User;
import com.tsystems.javaschool.eshop.model.dto.UserDTO;
import org.hibernate.Criteria;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Repository("statsDAO")
@Transactional
public class StatsDaoImpl extends AbstractDAO implements StatsDAO {
    private static final String PRODUCT_ATTR = "product";
    @Autowired
    ProductDAO productDAO;
    @Autowired
    UserDAO userDAO;

    @Override
    public List<User> getTopClients(int number) {
        Criteria criteria = getSession().createCriteria(Order.class);
        ProjectionList projectionList = Projections.projectionList();
        projectionList.add(Projections.groupProperty("user"));
        projectionList.add(Projections.rowCount(), "qty");
        criteria.setProjection(projectionList).addOrder(org.hibernate.criterion.Order.desc("qty"));
        criteria.setMaxResults(number);

        List<Object[]> rows = criteria.list();
        List<User> users = new ArrayList<>();
        for (int i = 0; i < rows.size(); i++) {
            Object u = rows.get(i)[0];
            users.add((User) u);
        }
        return users;
    }

    @Override
    public List<Object[]> getTopProducts(int number) {
        Criteria criteria = getSession().createCriteria(ProductInOrder.class);
        ProjectionList projectionList = Projections.projectionList();
        projectionList.add(Projections.groupProperty(PRODUCT_ATTR));
        projectionList.add(Projections.sum("qtyInOrder"), "qty");
        criteria.setProjection(projectionList).addOrder(org.hibernate.criterion.Order.desc("qty"));
        criteria.setMaxResults(number);
        return (List<Object[]>) criteria.list();
    }

    @Override
    public long getNumberOfSalesForProduct(Product product) {
        Criteria criteria = getSession().createCriteria(ProductInOrder.class);
        ProjectionList projectionList = Projections.projectionList();
        projectionList.add(Projections.groupProperty(PRODUCT_ATTR));
        projectionList.add(Projections.sum("qtyInOrder"));
        criteria.add(Restrictions.eq(PRODUCT_ATTR, product));
        criteria.setProjection(projectionList);
        return (long) ((Object[]) criteria.list().get(0))[1];
    }

    @Override
    public long getNumberOfOrdersForClient(User user) {
        Criteria criteria = getSession().createCriteria(Order.class);
        ProjectionList projectionList = Projections.projectionList();
        projectionList.add(Projections.groupProperty("user"));
        projectionList.add(Projections.rowCount());
        criteria.add(Restrictions.eq("user", user));
        criteria.setProjection(projectionList);
        return (long) ((Object[]) criteria.list().get(0))[1];
    }

    @Override
    public long getNumberOfOrdersForClient(UserDTO userDTO) {
        User u = userDAO.getUserByLogin(userDTO.getLogin());
        return getNumberOfOrdersForClient(u);
    }

    @Override
    public List<Order> getOrdersForLastWeek() {
        Criteria criteria = getSession().createCriteria(Order.class);
        Timestamp weekAgo = getTimestampAgo(-7);
        Timestamp today = new Timestamp(System.currentTimeMillis());
        criteria.add(Restrictions.between("orderDate", weekAgo, today));
        return (List<Order>) criteria.list();
    }

    @Override
    public List<Order> getOrdersForLastMonth() {
        Criteria criteria = getSession().createCriteria(Order.class);
        Timestamp monthAgo = getTimestampAgo(-30);
        Timestamp today = new Timestamp(System.currentTimeMillis());
        criteria.add(Restrictions.between("orderDate", monthAgo, today));
        return (List<Order>) criteria.list();
    }

    private Timestamp getTimestampAgo(int daysAgo) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DAY_OF_YEAR, daysAgo);
        long l = cal.getTimeInMillis();
        return new Timestamp(l);
    }
}
