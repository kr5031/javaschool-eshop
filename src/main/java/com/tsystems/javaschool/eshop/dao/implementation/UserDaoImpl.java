package com.tsystems.javaschool.eshop.dao.implementation;

import com.tsystems.javaschool.eshop.dao.AbstractDAO;
import com.tsystems.javaschool.eshop.dao.UserDAO;
import com.tsystems.javaschool.eshop.model.Role;
import com.tsystems.javaschool.eshop.model.User;
import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("userDAO")
@Transactional
public class UserDaoImpl extends AbstractDAO implements UserDAO {
    private static final Logger logger = Logger.getLogger(UserDaoImpl.class.getName());

    @Override
    public User getUserByLogin(String login) {
        Criteria criteria = getSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("login", login));
        return (User) criteria.uniqueResult();
    }

    @Override
    public void saveOrUpdateUser(User user) {
        getSession().saveOrUpdate(user);
        logger.info("User " + user.getUserId() + " was saved");
    }

    @Override
    public Role getRoleById(int id) {
        Criteria criteria = getSession().createCriteria(Role.class);
        criteria.add(Restrictions.eq("roleId", id));
        return (Role) criteria.uniqueResult();
    }

    @Override
    public User getUserByEmail(String email) {
        Criteria criteria = getSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("email", email));
        return (User) criteria.uniqueResult();
    }

    @Override
    public User getUserById(int userId) {
        Criteria criteria = getSession().createCriteria(User.class);
        criteria.add(Restrictions.eq("userId", userId));
        return (User) criteria.uniqueResult();
    }
}
