package com.tsystems.javaschool.eshop.exceptions;

/**
 * Thrown in case of trying to delete an object, which have objects depending on it
 * for ex., delete color or category, which have associated products
 */
public class CantBeDeletedException extends Exception {
}
