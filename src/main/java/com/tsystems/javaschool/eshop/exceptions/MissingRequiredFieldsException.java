package com.tsystems.javaschool.eshop.exceptions;

/**
 * exception thrown by the login/registration forms in case of wrong form
 */
public class MissingRequiredFieldsException extends Exception {
}
