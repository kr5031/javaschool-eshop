package com.tsystems.javaschool.eshop.model;


import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Address entity for order
 */
@Entity
@Table(name = "addresses")
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int addressId;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "cityId", nullable = false)
    private City city;

    @Column(name = "postCode", nullable = false)
    private int postCode;

    @Column(name = "street", nullable = false)
    private String street;

    @Column(name = "house", nullable = false)
    private int house;

    @Column(name = "apartments")
    private int apartments;

    public Address() {
        // awesome comment for SonarQube
    }

    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public int getPostCode() {
        return postCode;
    }

    public void setPostCode(int postCode) {
        this.postCode = postCode;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getHouse() {
        return house;
    }

    public void setHouse(int house) {
        this.house = house;
    }

    public int getApartments() {
        return apartments;
    }

    public void setApartments(int apartments) {
        this.apartments = apartments;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getPostCode()).append(", ")
                .append(getCity().getCountry().getCountryName()).append(", ")
                .append(getCity().getCityName()).append(", ")
                .append(getStreet()).append(" ")
                .append(getHouse()).append("-")
                .append(getApartments());
        return sb.toString();
    }
}
