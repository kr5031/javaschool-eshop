package com.tsystems.javaschool.eshop.model;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Cart entity
 */
public class Cart implements Serializable {
    private transient Map<Product, Integer> productsInCart;

    public Cart() {
        productsInCart = new HashMap<>();
    }

    /**
     * Add the product to cart
     *
     * @param product to add
     * @param qty     to add
     */
    public void addProductToCart(Product product, int qty) {
        if (productsInCart.containsKey(product)) {
            int newQty = productsInCart.get(product) + qty;
            productsInCart.put(product, newQty);
        } else {
            productsInCart.put(product, qty);
        }
    }

    public Map<Product, Integer> getProductsInCart() {
        return productsInCart;
    }

    public void setProductsInCart(Map<Product, Integer> productsInCart) {
        this.productsInCart = productsInCart;
    }

    /**
     * Set qty of product in the cart
     *
     * @param product to edit
     * @param qty     of product in the cart
     */
    public void setProductQtyInCart(Product product, int qty) {
        productsInCart.put(product, qty);
    }

    /**
     * Delete the product from the cart
     *
     * @param product to delete
     */
    public void deleteProductFromCart(Product product) {
        productsInCart.remove(product);
    }
}