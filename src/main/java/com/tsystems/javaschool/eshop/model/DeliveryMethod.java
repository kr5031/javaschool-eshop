package com.tsystems.javaschool.eshop.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Delivery method for an order entity
 */
@Entity
@Table(name = "deliveryMethods")
public class DeliveryMethod {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int deliveryMethodId;

    @Column(name = "deliveryMethodName", nullable = false)
    private String deliveryMethodName;

    @Column(name = "deliveryMethodPrice")
    private double deliveryMethodPrice;

    public DeliveryMethod() {
        //awesome comment for SonarQube
    }

    public int getDeliveryMethodId() {
        return deliveryMethodId;
    }

    public void setDeliveryMethodId(int deliveryMethodId) {
        this.deliveryMethodId = deliveryMethodId;
    }

    public String getDeliveryMethodName() {
        return deliveryMethodName;
    }

    public void setDeliveryMethodName(String deliveryMethodName) {
        this.deliveryMethodName = deliveryMethodName;
    }

    @Override
    public String toString() {
        return getDeliveryMethodName();
    }

    public double getDeliveryMethodPrice() {
        return deliveryMethodPrice;
    }

    public void setDeliveryMethodPrice(double deliveryMethodPrice) {
        this.deliveryMethodPrice = deliveryMethodPrice;
    }
}
