package com.tsystems.javaschool.eshop.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Photo entity
 */
@Entity
@Table(name = "photos")
public class Photo {
    @Column(name = "productId")
    int productId;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int photoId;
    @Column(name = "image")
    private byte[] image;

    public Photo() {
        //awesome comment for SonarQube
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public int getPhotoId() {
        return photoId;
    }

    public void setPhotoId(int photoId) {
        this.photoId = photoId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }
}