package com.tsystems.javaschool.eshop.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Product DTO for storing in order with fixed price
 */
@Entity
@Table(name = "productsInOrder")
public class ProductInOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int productInOrderId;

    @ManyToOne
    @JoinColumn(name = "orderId", nullable = false)
    private Order order;

    @ManyToOne
    @JoinColumn(name = "productId", nullable = false)
    private Product product;

    @Column(name = "qtyInOrder", nullable = false)
    private int qtyInOrder;

    @Column(name = "priceInOrder", nullable = false)
    private double priceInOrder;

    public ProductInOrder() {
        //awesome comment for SonarQube
    }

    public int getProductInOrderId() {
        return productInOrderId;
    }

    public void setProductInOrderId(int productInOrderId) {
        this.productInOrderId = productInOrderId;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public int getQtyInOrder() {
        return qtyInOrder;
    }

    public void setQtyInOrder(int qtyInOrder) {
        this.qtyInOrder = qtyInOrder;
    }

    public double getPriceInOrder() {
        return priceInOrder;
    }

    public void setPriceInOrder(double priceInOrder) {
        this.priceInOrder = priceInOrder;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ProductInOrder that = (ProductInOrder) o;

        return product.equals(that.product);
    }

    @Override
    public int hashCode() {
        return product.hashCode();
    }
}
