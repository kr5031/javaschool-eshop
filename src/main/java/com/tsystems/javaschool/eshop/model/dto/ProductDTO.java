package com.tsystems.javaschool.eshop.model.dto;

import com.tsystems.javaschool.eshop.model.Category;
import com.tsystems.javaschool.eshop.model.Color;
import com.tsystems.javaschool.eshop.model.Manufacturer;
import com.tsystems.javaschool.eshop.model.Product;

/**
 * DTO for Product with injected preview photo within
 */
public class ProductDTO {
    private Integer productId;
    private String name;
    private Double price;
    private Category category;
    private Manufacturer manufacturer;
    private Color color;
    private Double weight;
    private String description;
    private Double screen;
    private Integer memory;
    private byte[] image;

    public ProductDTO(Product product) {
        this.productId = product.getProductId();
        this.name = product.getName();
        this.price = product.getPrice();
        this.category = product.getCategory();
        this.manufacturer = product.getManufacturer();
        this.color = product.getColor();
        this.weight = product.getWeight();
        this.description = product.getDescription();
        this.screen = product.getScreen();
        this.memory = product.getMemory();
    }

    public ProductDTO() {
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Manufacturer getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(Manufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getScreen() {
        return screen;
    }

    public void setScreen(Double screen) {
        this.screen = screen;
    }

    public Integer getMemory() {
        return memory;
    }

    public void setMemory(Integer memory) {
        this.memory = memory;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
