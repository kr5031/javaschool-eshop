package com.tsystems.javaschool.eshop.model.dto;

import com.tsystems.javaschool.eshop.model.Role;
import com.tsystems.javaschool.eshop.model.User;

import java.time.LocalDate;

/**
 * DTO for exposing users via rest without password
 */
public class UserDTO {
    private int userId;
    private String login;
    private String name;
    private String surname;
    private LocalDate birthdate;
    private String email;
    private Role userRole;

    public UserDTO(User user) {
        setUserId(user.getUserId());
        setLogin(user.getLogin());
        setName(user.getName());
        setSurname(user.getSurname());
        setBirthdate(user.getBirthdate());
        setEmail(user.getEmail());
        setUserRole(user.getUserRole());
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public LocalDate getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(LocalDate birthdate) {
        this.birthdate = birthdate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Role getUserRole() {
        return userRole;
    }

    public void setUserRole(Role userRole) {
        this.userRole = userRole;
    }
}
