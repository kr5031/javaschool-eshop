package com.tsystems.javaschool.eshop.service;

import com.tsystems.javaschool.eshop.model.Cart;
import com.tsystems.javaschool.eshop.model.Product;

import java.util.Map;

public interface CartService {
    /**
     * Add the product to the cart
     *
     * @param product to add
     * @param qty     to add
     */
    void addProductToCart(Product product, int qty);

    /**
     * Delete the product from the cart
     *
     * @param product to delete
     */
    void deleteProductFromCart(Product product);

    /**
     * Get cart from HttpContext
     *
     * @return cart from HttpContext
     */
    Cart getCartFromSession();

    /**
     * Get Map<product,price> for products in cart
     *
     * @return Map<product,price> for products in cart
     */
    Map<Product, Double> getPricesForProductsInCart();

    /**
     * Get Map<product, photoURL> for the product
     *
     * @return Map<product, photoURL> for the product
     */
    Map<Product, String> getPhotosForProductsInCart();

    /**
     * Delete all from the cart
     */
    void clearCart();

    /**
     * Get products added to the cart
     *
     * @return products added to the cart
     */
    Map<Product, Integer> getProductsInCart();

    /**
     * Set qty of product in the cart
     *
     * @param productId
     * @param qty
     */
    void setProductQtyInCart(int productId, int qty);
}
