package com.tsystems.javaschool.eshop.service;

/**
 * Used for working with message topic
 */
public interface JmsService {
    /**
     * Notify the subscribers that stats was changed
     */
    void orderCreated();
}
