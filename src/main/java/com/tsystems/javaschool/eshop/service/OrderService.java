package com.tsystems.javaschool.eshop.service;

import com.tsystems.javaschool.eshop.exceptions.NotAuthorizedException;
import com.tsystems.javaschool.eshop.exceptions.WrongIdException;
import com.tsystems.javaschool.eshop.exceptions.WrongOrderException;
import com.tsystems.javaschool.eshop.exceptions.WrongUserException;
import com.tsystems.javaschool.eshop.model.DeliveryMethod;
import com.tsystems.javaschool.eshop.model.Order;
import com.tsystems.javaschool.eshop.model.OrderStatus;
import com.tsystems.javaschool.eshop.model.PaymentMethod;
import com.tsystems.javaschool.eshop.model.ProductInOrder;
import com.tsystems.javaschool.eshop.model.User;
import org.springframework.util.MultiValueMap;

import java.util.List;

public interface OrderService {
    /**
     * Get the order by its' id
     *
     * @param user requesting the order
     * @param id   order ID
     * @return order by its' id
     * @throws WrongIdException       wrong order ID
     * @throws NotAuthorizedException order doesn't belong to the user and user is not an admin
     */
    Order getOrderById(User user, int id) throws WrongIdException, NotAuthorizedException;

    /**
     * Get all orders of the user (by his ID)
     *
     * @param userId
     * @return all orders of the user
     * @throws WrongUserException wrong user ID
     */
    List<Order> getOrdersByUserId(int userId) throws WrongUserException;

    /**
     * Get all orders in the system (for admin only)
     *
     * @return all orders in the system (for admin only)
     */
    List<Order> getAllOrders();

    /**
     * Get statuses of all orders in the system (for admin only)
     *
     * @return statuses of all orders in the system (for admin only)
     */
    List<OrderStatus> getAllOrdersStatuses();

    /**
     * Save an order (existing or new)
     *
     * @param order to save
     * @throws WrongOrderException order is null
     */
    void saveOrUpdateOrder(Order order) throws WrongOrderException;

    /**
     * Get products DTO associated with an order
     *
     * @param orderId
     * @return list of products DTO associated with an order
     */
    List<ProductInOrder> getProductsInOrderByOrderId(int orderId);

    /**
     * Get all existing delivery methods
     *
     * @return all existing delivery methods
     */
    List<DeliveryMethod> getAllDeliveryMethods();

    /**
     * Get all existing payment methods
     *
     * @return all existing payment methods
     */
    List<PaymentMethod> getAllPaymentMethods();

    /**
     * Get delivery method by its ID
     *
     * @param deliveryMethodId
     * @return delivery method
     */
    DeliveryMethod getDeliveryMethodById(int deliveryMethodId);

    /**
     * Get payment method by its ID
     *
     * @param paymentMethodId
     * @return payment method by its ID
     */
    PaymentMethod getPaymentMethodById(int paymentMethodId);

    /**
     * Create an order base on passed form values
     *
     * @param pathVariables passed form values
     */
    void createOrder(MultiValueMap<String, String> pathVariables);

    /**
     * Save product dto
     *
     * @param productInOrder
     */
    void saveOrUpdateProductInOrder(ProductInOrder productInOrder);

    /**
     * Set order statuses based on passed form values
     *
     * @param pathVariables passed form values
     */
    void updateOrdersStatuses(MultiValueMap<String, String> pathVariables);
}
