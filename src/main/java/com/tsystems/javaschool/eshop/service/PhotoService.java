package com.tsystems.javaschool.eshop.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface PhotoService {
    /**
     * Get photo by its' id
     *
     * @param photoId
     * @return image as byte array
     * @throws IOException
     */
    byte[] getPhotoById(int photoId) throws IOException;

    /**
     * Get preview photo by product ID
     *
     * @param productId
     * @return image as byte array
     */
    byte[] getPreviewPhoto(int productId);

    /**
     * Save uploaded photos to the DB
     *
     * @param files     uploaded photos
     * @param productId associated with the photos
     * @throws IOException
     */
    void savePhotos(MultipartFile[] files, int productId) throws IOException;

    /**
     * Get all images for the product
     *
     * @param productId
     * @return list of images for the product
     */
    List<Integer> getListPhotosByProductId(int productId);

    /**
     * Get list of photo IDs associated with the product
     *
     * @param productId
     * @return list of photo IDs associated with the product
     */
    List<String> getProductImagesURLs(int productId);
}
