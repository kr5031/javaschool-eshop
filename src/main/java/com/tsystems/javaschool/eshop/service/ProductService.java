package com.tsystems.javaschool.eshop.service;

import com.tsystems.javaschool.eshop.exceptions.CantBeDeletedException;
import com.tsystems.javaschool.eshop.exceptions.MissingRequiredFieldsException;
import com.tsystems.javaschool.eshop.model.Category;
import com.tsystems.javaschool.eshop.model.Color;
import com.tsystems.javaschool.eshop.model.Manufacturer;
import com.tsystems.javaschool.eshop.model.Product;
import org.springframework.util.MultiValueMap;

import java.util.List;

public interface ProductService {
    /**
     * Create a new product based on passed form values (admin only)
     *
     * @param pathVariables passed form values
     * @throws MissingRequiredFieldsException form values are incorrect or missing
     */
    void createProduct(MultiValueMap<String, String> pathVariables) throws MissingRequiredFieldsException;

    /**
     * Save a new product (admin only)
     *
     * @param product new product
     */
    void saveProduct(Product product);

    /**
     * Add a new category (admin only)
     *
     * @param categoryName
     */
    void addCategory(String categoryName);

    /**
     * Delete a category by its' ID
     *
     * @param categoryId
     */
    void deleteCategory(int categoryId) throws CantBeDeletedException;

    /**
     * Get all existing products
     *
     * @return list of all existing products
     */
    List<Product> getAllProducts();

    /**
     * Get product by its' id
     *
     * @param productId
     * @return product by its' id
     */
    Product getProductById(int productId);

    /**
     * Get products based on filter parameters
     *
     * @param multiValueMap key-value pairs for filtering
     * @return products based on filter parameters
     */
    List<Product> getFilteredProducts(MultiValueMap<String, String> multiValueMap);

    /**
     * Get all products for the specified category Id
     *
     * @param categoryId
     * @return list of products
     */
    List<Product> getProductsForCategory(int categoryId);

    /**
     * Save changed existing product (admin only)
     *
     * @param product
     */
    void updateProduct(Product product);

    /**
     * Get all manufacturers for a category (used for filling filter with options)
     *
     * @param categoryId
     * @return all manufacturers for a category (used for filling filter with options)
     */
    List<Manufacturer> getAllManufacturersForCategory(int categoryId);

    /**
     * Get name of HTML file with filters for the category
     *
     * @param categoryId
     * @return name of HTML file with filters for the category
     */
    String getFilterFileForCategory(int categoryId);

    /**
     * Get price for the product
     *
     * @param productId
     * @return price for the product
     */
    Double getPriceForProductId(int productId);

    /**
     * Get all existing categories (for filling navigation bar menu)
     *
     * @return all existing categories
     */
    List<Category> getAllCategories();

    /**
     * Get all existing colors (for filling filter menu)
     *
     * @return all existing colors
     */
    List<Color> getAllColors();

    /**
     * Get all existing manufacturers (for filter menu)
     *
     * @return all existing manufacturers
     */
    List<Manufacturer> getAllManufacturers();

    /**
     * Find category ID by its' name
     *
     * @param categoryName
     * @return category ID
     */
    int getCategoryIdByName(String categoryName);

    /**
     * Add a new manufacturer (admin only)
     *
     * @param manufacturerName
     */
    void addManufacturer(String manufacturerName);

    /**
     * Delete manufacturer (admin only)
     *
     * @param manufacturerId
     */
    void deleteManufacturer(int manufacturerId) throws CantBeDeletedException;

    /**
     * Add a new color (admin only)
     *
     * @param colorName
     */
    void addColor(String colorName);

    /**
     * Delete a color (admin only)
     *
     * @param colorId
     */
    void deleteColor(int colorId) throws CantBeDeletedException;
}