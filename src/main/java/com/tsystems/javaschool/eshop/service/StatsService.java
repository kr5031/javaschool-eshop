package com.tsystems.javaschool.eshop.service;

import com.tsystems.javaschool.eshop.model.Order;
import com.tsystems.javaschool.eshop.model.Product;
import com.tsystems.javaschool.eshop.model.dto.ProductDTO;
import com.tsystems.javaschool.eshop.model.dto.UserDTO;

import java.util.List;

public interface StatsService {
    /**
     * Get top clients (by number of orders)
     *
     * @param number number of top clients
     * @return top N clients
     */
    List<UserDTO> getTopClients(int number);

    /**
     * Get number of orders for the client
     *
     * @param users
     * @return number of orders for the client
     */
    List<Long> getNumberOfOrdersForClients(List<UserDTO> users);

    /**
     * Get top products (by number of sold qt.)
     *
     * @param number number of top products
     * @return top N products
     */
    List<ProductDTO> getTopProducts(int number);

    /**
     * Get number of sales for the product
     *
     * @param products
     * @return number of sales for the product
     */
    List<Long> getNumberOfSalesForProducts(List<Product> products);

    /**
     * Get number of sales for the product (by DTO)
     *
     * @param products
     * @return number of sales for the product
     */
    List<Long> getNumberOfSalesForProductsDTO(List<ProductDTO> products);

    /**
     * Get number of orders for last week
     *
     * @return number of orders for last week
     */
    List<Order> getOrdersForLastWeek();

    /**
     * Get number of orders for last month
     *
     * @return number of orders for last month
     */
    List<Order> getOrdersForLastMonth();
}
