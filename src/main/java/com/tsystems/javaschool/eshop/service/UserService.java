package com.tsystems.javaschool.eshop.service;

import com.tsystems.javaschool.eshop.exceptions.MissingRequiredFieldsException;
import com.tsystems.javaschool.eshop.exceptions.PasswordsDontMatchException;
import com.tsystems.javaschool.eshop.exceptions.UserAlreadyExistsException;
import com.tsystems.javaschool.eshop.model.Role;
import com.tsystems.javaschool.eshop.model.User;
import org.springframework.util.MultiValueMap;

public interface UserService {
    /**
     * Get user by his login
     *
     * @param login
     * @return user by his login
     */
    User getUserByLogin(String login);

    /**
     * Save new parameters of user based on values passed via form
     *
     * @param multiValueMap values passed via form
     * @param user          to save
     * @throws MissingRequiredFieldsException form values are incorrect or missing
     * @throws PasswordsDontMatchException    passwords and its confirmation don't match
     */
    void saveOrUpdateUser(MultiValueMap<String, String> multiValueMap, User user) throws MissingRequiredFieldsException,
            PasswordsDontMatchException;

    /**
     * Create a new user
     *
     * @param multiValueMap user profile values passed via form
     * @return
     * @throws MissingRequiredFieldsException form values are incorrect or missing
     * @throws PasswordsDontMatchException    passwords and its confirmation don't match
     * @throws UserAlreadyExistsException
     */
    User createUser(MultiValueMap<String, String> multiValueMap) throws MissingRequiredFieldsException,
            PasswordsDontMatchException, UserAlreadyExistsException;

    /**
     * Get user role by its' id
     *
     * @param id
     * @return user role by its' id
     */
    Role getRoleById(int id);

    /**
     * Get user by his id
     *
     * @param userId
     * @return user by his id
     */
    User getUserById(int userId);

    /**
     * Get current logged in user
     *
     * @return current logged in user
     */
    User getCurrentUser();

    /**
     * True if current logged user has admin role
     *
     * @return true if current logged user has admin role
     */
    boolean isAdmin();

    /**
     * Get ID of current logged in user
     *
     * @return ID of current logged in user
     */
    int getCurrentUserId();
}
