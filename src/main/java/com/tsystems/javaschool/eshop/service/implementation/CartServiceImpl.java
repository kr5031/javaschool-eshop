package com.tsystems.javaschool.eshop.service.implementation;

import com.tsystems.javaschool.eshop.model.Cart;
import com.tsystems.javaschool.eshop.model.Product;
import com.tsystems.javaschool.eshop.service.CartService;
import com.tsystems.javaschool.eshop.service.PhotoService;
import com.tsystems.javaschool.eshop.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service("cartService")
public class CartServiceImpl implements CartService {
    private static final String NO_PHOTO_FILE = "/static/photo/no-image-available.jpg";
    @Autowired
    HttpSession httpSession;
    @Autowired
    ProductService productService;
    @Autowired
    PhotoService photoService;

    @Override
    public void addProductToCart(Product product, int qty) {
        Cart cart = getCartFromSession();
        cart.addProductToCart(product, qty);
        httpSession.setAttribute("cart", cart);
    }

    @Override
    public Map<Product, Integer> getProductsInCart() {
        Cart cart = getCartFromSession();
        return cart.getProductsInCart();
    }

    @Override
    public void deleteProductFromCart(Product product) {
        Cart cart = getCartFromSession();
        cart.deleteProductFromCart(product);
    }

    @Override
    public Cart getCartFromSession() {
        Cart cart = (Cart) httpSession.getAttribute("cart");
        if (cart == null) {
            cart = new Cart();
            httpSession.setAttribute("cart", cart);
        }
        return cart;
    }

    @Override
    public Map<Product, Double> getPricesForProductsInCart() {
        Map<Product, Double> result = new HashMap<>();
        Map<Product, Integer> productsInCart = getProductsInCart();
        for (Product product : productsInCart.keySet()) {
            result.put(product, product.getPrice());
        }
        return result;
    }

    @Override
    public Map<Product, String> getPhotosForProductsInCart() {
        Map<Product, String> result = new HashMap<>();
        Map<Product, Integer> productsInCart = getProductsInCart();
        for (Product product : productsInCart.keySet()) {
            List<String> photos = photoService.getProductImagesURLs(product.getProductId());
            if (photos != null && !photos.isEmpty()) {
                result.put(product, photos.get(0));
            } else {
                result.put(product, NO_PHOTO_FILE);
            }
        }
        return result;
    }

    @Override
    public void clearCart() {
        Cart cart = getCartFromSession();
        cart.getProductsInCart().clear();
    }

    @Override
    public void setProductQtyInCart(int productId, int qty) {
        Cart cart = getCartFromSession();
        Product product = productService.getProductById(productId);
        cart.setProductQtyInCart(product, qty);
    }

    /**
     * Used for testing to set mock objects
     *
     * @param httpSession mock object
     */
    public void setHttpSession(HttpSession httpSession) {
        this.httpSession = httpSession;
    }

    /**
     * Used for testing to set mock objects
     *
     * @param productService mock object
     */
    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    /**
     * Used for testing to set mock objects
     *
     * @param photoService mock object
     */
    public void setPhotoService(PhotoService photoService) {
        this.photoService = photoService;
    }
}
