package com.tsystems.javaschool.eshop.service.implementation;

import com.tsystems.javaschool.eshop.service.JmsService;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.jms.TopicConnection;
import javax.jms.TopicConnectionFactory;
import javax.jms.TopicPublisher;
import javax.jms.TopicSession;
import javax.naming.Context;
import javax.naming.InitialContext;
import java.util.Properties;

@Service("jmsService")
public class JmsServiceImpl implements JmsService {
    private static final Logger log = Logger.getLogger(JmsService.class.getName());

    private static final String JMS_DEFAULT_CONNECTION_FACTORY = System.getProperty("JMS_DEFAULT_CONNECTION_FACTORY",
            "jms/RemoteConnectionFactory");
    private static final String JMS_DEFAULT_DESTINATION = System.getProperty("JMS_DEFAULT_DESTINATION",
            "jms/topic/eShop");
    private static final String userName = System.getProperty("JMS_username");
    private static final String password = System.getProperty("JMS_password");
    private static final String JMS_INITIAL_CONTEXT_FACTORY = System.getProperty("JMS_INITIAL_CONTEXT_FACTORY",
            "org.jboss.naming.remote.client.InitialContextFactory");
    private static final String JMS_PROVIDER_URL = System.getProperty("JMS_PROVIDER_URL",
            "http-remoting://127.0.0.1:8080");

    private static void informListeners() {
        try {
            final Properties env = new Properties();
            env.put(Context.INITIAL_CONTEXT_FACTORY, JMS_INITIAL_CONTEXT_FACTORY);
            env.put(Context.PROVIDER_URL, System.getProperty(Context.PROVIDER_URL, JMS_PROVIDER_URL));
            env.put(Context.SECURITY_PRINCIPAL, userName);
            env.put(Context.SECURITY_CREDENTIALS, password);

            Context context = new InitialContext(env);

            // Perform the JNDI lookups
            String connectionFactoryString = System.getProperty("connection.factory", JMS_DEFAULT_CONNECTION_FACTORY);
            log.info("Attempting to acquire connection factory \"" + connectionFactoryString + "\"");
            TopicConnectionFactory connectionFactory = (TopicConnectionFactory) context.lookup(connectionFactoryString);
            log.info("Found connection factory \"" + connectionFactoryString + "\" in JNDI");

            String destinationString = System.getProperty("destination", JMS_DEFAULT_DESTINATION);
            log.info("Attempting to acquire destination \"" + destinationString + "\"");
            TopicConnection conn = connectionFactory.createTopicConnection(userName, password);
            log.info("Found destination \"" + destinationString + "\" in JNDI");
            Topic topic = (Topic) context.lookup(destinationString);
            TopicSession session = conn.createTopicSession(false, TopicSession.AUTO_ACKNOWLEDGE);
            conn.start();

            //send
            TopicPublisher send = session.createPublisher(topic);
            TextMessage tm = session.createTextMessage("stats updated, synchronize your data");
            send.publish(tm);
            log.info("Pushed \"" + tm.getText() + "\" to the topic " + JMS_DEFAULT_DESTINATION);
            send.close();

            //close
            session.close();
            context.close();
            conn.close();
        } catch (Exception e) {
            log.error(e.getMessage());
        }

    }

    public void orderCreated() {
        informListeners();
    }
}
