package com.tsystems.javaschool.eshop.service.implementation;

import com.tsystems.javaschool.eshop.exceptions.NotAuthorizedException;
import com.tsystems.javaschool.eshop.exceptions.WrongIdException;
import com.tsystems.javaschool.eshop.exceptions.WrongOrderException;
import com.tsystems.javaschool.eshop.exceptions.WrongUserException;
import com.tsystems.javaschool.eshop.dao.OrderDAO;
import com.tsystems.javaschool.eshop.model.Address;
import com.tsystems.javaschool.eshop.model.Cart;
import com.tsystems.javaschool.eshop.model.City;
import com.tsystems.javaschool.eshop.model.Country;
import com.tsystems.javaschool.eshop.model.DeliveryMethod;
import com.tsystems.javaschool.eshop.model.Order;
import com.tsystems.javaschool.eshop.model.OrderStatus;
import com.tsystems.javaschool.eshop.model.PaymentMethod;
import com.tsystems.javaschool.eshop.model.Product;
import com.tsystems.javaschool.eshop.model.ProductInOrder;
import com.tsystems.javaschool.eshop.model.User;
import com.tsystems.javaschool.eshop.service.CartService;
import com.tsystems.javaschool.eshop.service.JmsService;
import com.tsystems.javaschool.eshop.service.OrderService;
import com.tsystems.javaschool.eshop.service.ProductService;
import com.tsystems.javaschool.eshop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;

import java.sql.Timestamp;
import java.util.List;

@Service("orderService")
public class OrderServiceImpl implements OrderService {
    private static final int ADMIN_ID = 1;
    @Autowired
    OrderDAO orderDao;
    @Autowired
    UserService userService;
    @Autowired
    CartService cartService;
    @Autowired
    ProductService productService;
    @Autowired
    JmsService jmsService;

    @Override
    public Order getOrderById(User user, int id) throws WrongIdException, NotAuthorizedException {
        Order order = orderDao.getOrderById(id);
        if (order == null || user == null) {
            throw new WrongIdException();
        } else if (order.getUser().getUserId() != user.getUserId() && user.getUserRole().getRoleId() != ADMIN_ID) {
            throw new NotAuthorizedException();
        } else {
            return order;
        }
    }

    @Override
    public List<Order> getOrdersByUserId(int userId) throws WrongUserException {
        User user = userService.getUserById(userId);
        if (user == null) {
            throw new WrongUserException();
        } else {
            return orderDao.getOrdersOfUser(user);
        }
    }

    @Override
    public void saveOrUpdateOrder(Order order) throws WrongOrderException {
        if (order == null) {
            throw new WrongOrderException();
        } else {
            orderDao.saveOrUpdateOrder(order);
            jmsService.orderCreated();
        }
    }

    @Override
    public List<ProductInOrder> getProductsInOrderByOrderId(int orderId) {
        return orderDao.getProductsInOrderByOrderId(orderId);
    }

    @Override
    public List<DeliveryMethod> getAllDeliveryMethods() {
        return orderDao.getAllDeliveryMethods();
    }

    @Override
    public DeliveryMethod getDeliveryMethodById(int deliveryMethodId) {
        return orderDao.getDeliveryMethodById(deliveryMethodId);
    }

    @Override
    public void createOrder(MultiValueMap<String, String> pathVariables) {
        if (checkNewOrderFields(pathVariables)) {
            String countryStr = pathVariables.getFirst("country");
            Country country = orderDao.getCountryByName(countryStr);
            String cityName = pathVariables.getFirst("city");
            City city = orderDao.getCityByName(cityName, country.getCountryName());
            String postCodeStr = pathVariables.getFirst("postCode");
            int postCode = Integer.parseInt(postCodeStr);
            String street = pathVariables.getFirst("street");
            String houseStr = pathVariables.getFirst("house");
            int house = Integer.parseInt(houseStr);
            String apartmentsStr = pathVariables.getFirst("apartments");
            int apartments = Integer.parseInt(apartmentsStr);
            Address address = orderDao.getAddress(city, postCode, street, house, apartments);
            DeliveryMethod deliveryMethod = orderDao.getDeliveryMethodById(Integer.parseInt(pathVariables.getFirst("deliveryMethodId")));
            PaymentMethod paymentMethod = orderDao.getPaymentMethodById(Integer.parseInt(pathVariables.getFirst("paymentMethodId")));
            OrderStatus orderStatus = orderDao.getOrderStatus("new");
            double total = Double.parseDouble(pathVariables.getFirst("total"));

            Order order = new Order();

            order.setAddress(address);
            order.setDeliveryMethod(deliveryMethod);
            order.setPaymentMethod(paymentMethod);
            order.setOrderDate(new Timestamp(System.currentTimeMillis()));
            order.setOrderStatus(orderStatus);
            order.setTotalSum(total);
            order.setUser(userService.getCurrentUser());
            try {
                saveOrUpdateOrder(order);
                Cart cart = cartService.getCartFromSession();
                for (Product product : cart.getProductsInCart().keySet()) {
                    ProductInOrder productInOrder = new ProductInOrder();
                    productInOrder.setOrder(order);
                    productInOrder.setProduct(product);
                    productInOrder.setQtyInOrder(cart.getProductsInCart().get(product));
                    productInOrder.setPriceInOrder(productService.getPriceForProductId(product.getProductId()));
                    saveOrUpdateProductInOrder(productInOrder);
                }
                cartService.clearCart();
            } catch (WrongOrderException e) {
                // NOP
            }
        }
    }

    @Override
    public List<PaymentMethod> getAllPaymentMethods() {
        return orderDao.getAllPaymentMethods();
    }

    @Override
    public PaymentMethod getPaymentMethodById(int paymentMethodId) {
        return orderDao.getPaymentMethodById(paymentMethodId);
    }

    private boolean checkNewOrderFields(MultiValueMap<String, String> mvm) {
        return mvm.get("country") != null && mvm.get("city") != null && mvm.get("postCode") != null &&
                mvm.get("street") != null && mvm.get("house") != null && mvm.get("apartments") != null &&
                mvm.get("deliveryMethodId") != null && mvm.get("paymentMethodId") != null;
    }

    @Override
    public void saveOrUpdateProductInOrder(ProductInOrder productInOrder) {
        orderDao.saveOrUpdateProductInOrder(productInOrder);
    }

    @Override
    public List<Order> getAllOrders() {
        return orderDao.getAllOrders();
    }

    @Override
    public List<OrderStatus> getAllOrdersStatuses() {
        return orderDao.getAllOrdersStatuses();
    }

    @Override
    public void updateOrdersStatuses(MultiValueMap<String, String> pathVariables) {
        for (String orderId : pathVariables.keySet()) {
            Order order = orderDao.getOrderById(Integer.parseInt(orderId));
            OrderStatus newOrderStatus = orderDao.getOrderStatusById(Integer.parseInt(pathVariables.getFirst(orderId)));
            order.setOrderStatus(newOrderStatus);
            orderDao.saveOrUpdateOrder(order);
        }
    }

    public void setOrderDao(OrderDAO orderDao) {
        this.orderDao = orderDao;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public void setCartService(CartService cartService) {
        this.cartService = cartService;
    }

    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    public void setJmsService(JmsService jmsService) {
        this.jmsService = jmsService;
    }
}