package com.tsystems.javaschool.eshop.service.implementation;

import com.tsystems.javaschool.eshop.dao.PhotoDAO;
import com.tsystems.javaschool.eshop.service.PhotoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service("photoService")
public class PhotoServiceImpl implements PhotoService {
    public static final String GET_PHOTO_URL = "/getPhoto?photoId=";

    @Autowired
    PhotoDAO photoDAO;

    @Override
    public byte[] getPhotoById(int photoId) throws IOException {
        return photoDAO.getPhotoById(photoId);
    }

    @Override
    public byte[] getPreviewPhoto(int productId) {
        return photoDAO.getPreviewPhoto(productId);
    }

    @Override
    public List<Integer> getListPhotosByProductId(int productId) {
        return photoDAO.getListPhotosByProductId(productId);
    }

    @Override
    public void savePhotos(MultipartFile[] files, int productId) throws IOException {
        photoDAO.savePhotos(files, productId);
    }

    @Override
    public List<String> getProductImagesURLs(int productId) {
        List<Integer> photoIds = photoDAO.getListPhotosByProductId(productId);
        List<String> result = new ArrayList<>();
        for (Integer photoId : photoIds) {
            result.add(GET_PHOTO_URL + photoId);
        }
        return result;
    }

    public void setPhotoDAO(PhotoDAO photoDAO) {
        this.photoDAO = photoDAO;
    }
}
