package com.tsystems.javaschool.eshop.service.implementation;

import com.tsystems.javaschool.eshop.exceptions.CantBeDeletedException;
import com.tsystems.javaschool.eshop.exceptions.MissingRequiredFieldsException;
import com.tsystems.javaschool.eshop.dao.ProductDAO;
import com.tsystems.javaschool.eshop.model.Category;
import com.tsystems.javaschool.eshop.model.Color;
import com.tsystems.javaschool.eshop.model.Manufacturer;
import com.tsystems.javaschool.eshop.model.Product;
import com.tsystems.javaschool.eshop.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;

import java.util.List;

@Service("productService")
@Transactional
public class ProductServiceImpl implements ProductService {
    private static final String CATEGORY_ID_ATTR = "categoryId";
    private static final String COLOR_ID_ATTR = "colorId";
    private static final String MANUFACTURER_ID_ATTR = "manufacturerId";
    private static final String PRICE_ATTR = "price";
    private static final String DESCRIPTION_ATTR = "description";
    private static final String MEMORY_ATTR = "memory";
    private static final String WEIGHT_ATTR = "weight";
    private static final String SCREEN_ATTR = "screen";
    @Autowired
    private ProductDAO productDAO;

    public void saveProduct(Product product) {
        productDAO.saveProduct(product);
    }

    public List<Product> getAllProducts() {
        return productDAO.getAllProducts();
    }

    public Product getProductById(int productId) {
        return productDAO.getProductById(productId);
    }

    public void updateProduct(Product product) {
        productDAO.updateProduct(product);
    }

    @Override
    public List<Product> getFilteredProducts(MultiValueMap<String, String> multiValueMap) {
        return productDAO.getFilteredProducts(multiValueMap);
    }

    @Override
    public List<Manufacturer> getAllManufacturersForCategory(int categoryId) {
        return productDAO.getAllManufacturersForCategory(categoryId);
    }

    @Override
    public String getFilterFileForCategory(int categoryId) {
        return productDAO.getFilterFileForCategory(categoryId);
    }

    @Override
    public Double getPriceForProductId(int productId) {
        return productDAO.getPriceForProductId(productId);
    }

    @Override
    public List<Category> getAllCategories() {
        return productDAO.getAllCategories();
    }

    @Override
    public List<Color> getAllColors() {
        return productDAO.getAllColors();
    }

    @Override
    public List<Manufacturer> getAllManufacturers() {
        return productDAO.getAllManufacturers();
    }

    @Override
    public void createProduct(MultiValueMap<String, String> pathVariables) throws MissingRequiredFieldsException {
        checkProductParameters(pathVariables);
        Product product = new Product();
        product.setName(pathVariables.getFirst("name"));
        Category category = productDAO.getCategoryById(Integer.parseInt(pathVariables.getFirst(CATEGORY_ID_ATTR)));
        product.setCategory(category);
        product.setPrice(Double.parseDouble(pathVariables.getFirst(PRICE_ATTR)));
        Color color = productDAO.getColorById(Integer.parseInt(pathVariables.getFirst(COLOR_ID_ATTR)));
        product.setColor(color);
        Manufacturer manufacturer = productDAO.getManufacturerById(Integer.parseInt(pathVariables.getFirst(MANUFACTURER_ID_ATTR)));
        product.setManufacturer(manufacturer);
        product.setDescription(pathVariables.getFirst(DESCRIPTION_ATTR));
        product.setMemory(Integer.parseInt(pathVariables.getFirst(MEMORY_ATTR)));
        product.setWeight(Double.parseDouble(pathVariables.getFirst(WEIGHT_ATTR)));
        product.setScreen(Double.parseDouble(pathVariables.getFirst(SCREEN_ATTR)));

        productDAO.createProduct(product);
    }

    @Override
    public int getCategoryIdByName(String categoryName) {
        return productDAO.getCategoryIdByName(categoryName);
    }

    @Override
    public void addCategory(String categoryName) {
        Category category = new Category();
        category.setCategoryName(categoryName);
        productDAO.addCategory(category);
    }

    @Override
    public void deleteCategory(int categoryId) throws CantBeDeletedException {
        productDAO.deleteCategory(categoryId);
    }

    @Override
    public void addManufacturer(String manufacturerName) {
        Manufacturer manufacturer = new Manufacturer();
        manufacturer.setManufacturerName(manufacturerName);
        productDAO.addManufacturer(manufacturer);
    }

    @Override
    public void deleteManufacturer(int manufacturerId) throws CantBeDeletedException {
        Manufacturer manufacturer = productDAO.getManufacturerById(manufacturerId);
        productDAO.deleteManufacturer(manufacturer);
    }

    @Override
    public void addColor(String colorName) {
        Color color = new Color();
        color.setColorName(colorName);
        productDAO.addColor(color);
    }

    @Override
    public void deleteColor(int colorId) throws CantBeDeletedException {
        Color color = productDAO.getColorById(colorId);
        productDAO.deleteColor(color);
    }

    private void checkProductParameters(MultiValueMap<String, String> multiValueMap) throws MissingRequiredFieldsException {
        if (multiValueMap.getFirst("name") == null ||
                multiValueMap.getFirst(PRICE_ATTR) == null || multiValueMap.getFirst(CATEGORY_ID_ATTR) == null ||
                multiValueMap.getFirst(MANUFACTURER_ID_ATTR) == null || multiValueMap.getFirst(COLOR_ID_ATTR) == null ||
                multiValueMap.getFirst(WEIGHT_ATTR) == null || multiValueMap.getFirst(MEMORY_ATTR) == null ||
                multiValueMap.getFirst(SCREEN_ATTR) == null ||
                multiValueMap.getFirst(DESCRIPTION_ATTR) == null ||
                multiValueMap.getFirst(PRICE_ATTR).isEmpty() || multiValueMap.getFirst(CATEGORY_ID_ATTR).isEmpty() ||
                multiValueMap.getFirst(MANUFACTURER_ID_ATTR).isEmpty() || multiValueMap.getFirst(COLOR_ID_ATTR).isEmpty() ||
                multiValueMap.getFirst(WEIGHT_ATTR).isEmpty() || multiValueMap.getFirst(MEMORY_ATTR).isEmpty() ||
                multiValueMap.getFirst(SCREEN_ATTR).isEmpty() || multiValueMap.getFirst(DESCRIPTION_ATTR).isEmpty()) {
            throw new MissingRequiredFieldsException();
        }
    }

    public void setProductDAO(ProductDAO productDAO) {
        this.productDAO = productDAO;
    }

    @Override
    public List<Product> getProductsForCategory(int categoryId) {
        return productDAO.getProductsForCategory(categoryId);
    }
}