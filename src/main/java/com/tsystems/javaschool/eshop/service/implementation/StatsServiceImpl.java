package com.tsystems.javaschool.eshop.service.implementation;

import com.tsystems.javaschool.eshop.dao.StatsDAO;
import com.tsystems.javaschool.eshop.model.Order;
import com.tsystems.javaschool.eshop.model.Product;
import com.tsystems.javaschool.eshop.model.User;
import com.tsystems.javaschool.eshop.model.dto.ProductDTO;
import com.tsystems.javaschool.eshop.model.dto.UserDTO;
import com.tsystems.javaschool.eshop.service.PhotoService;
import com.tsystems.javaschool.eshop.service.ProductService;
import com.tsystems.javaschool.eshop.service.StatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service("statsService")
@Transactional
public class StatsServiceImpl implements StatsService {
    @Autowired
    StatsDAO statsDAO;

    @Autowired
    ProductService productService;

    @Autowired
    PhotoService photoService;

    @Override
    public List<UserDTO> getTopClients(int number) {
        List<User> topClients = statsDAO.getTopClients(number);
        List<UserDTO> toReturn = new ArrayList<>();
        for (User u : topClients) {
            toReturn.add(new UserDTO(u));
        }
        return toReturn;
    }

    @Override
    public List<Long> getNumberOfOrdersForClients(List<UserDTO> users) {
        List<Long> result = new ArrayList<>();
        for (UserDTO u : users) {
            result.add(statsDAO.getNumberOfOrdersForClient(u));
        }
        return result;
    }

    @Override
    public List<ProductDTO> getTopProducts(int number) {
        List<Object[]> rows = statsDAO.getTopProducts(number);
        List<ProductDTO> result = new ArrayList<>();
        for (Object[] row : rows) {
            Product p = (Product) row[0];
            ProductDTO dto = new ProductDTO(p);
            dto.setImage(photoService.getPreviewPhoto(dto.getProductId()));
            result.add(dto);
        }
        return result;
    }

    @Override
    public List<Long> getNumberOfSalesForProducts(List<Product> products) {
        List<Long> result = new ArrayList<>();
        for (Product p : products) {
            long qty = statsDAO.getNumberOfSalesForProduct(p);
            result.add(qty);
        }
        return result;
    }

    @Override
    public List<Long> getNumberOfSalesForProductsDTO(List<ProductDTO> products) {
        List<Product> l = new ArrayList<>();
        for (ProductDTO p : products) {
            Product product = productService.getProductById(p.getProductId());
            l.add(product);
        }
        return getNumberOfSalesForProducts(l);
    }

    @Override
    public List<Order> getOrdersForLastWeek() {
        return statsDAO.getOrdersForLastWeek();
    }

    @Override
    public List<Order> getOrdersForLastMonth() {
        return statsDAO.getOrdersForLastMonth();
    }

    public void setStatsDAO(StatsDAO statsDAO) {
        this.statsDAO = statsDAO;
    }

    public void setProductService(ProductService productService) {
        this.productService = productService;
    }

    public void setPhotoService(PhotoService photoService) {
        this.photoService = photoService;
    }
}
