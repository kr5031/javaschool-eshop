package com.tsystems.javaschool.eshop.service.implementation;

import com.tsystems.javaschool.eshop.exceptions.MissingRequiredFieldsException;
import com.tsystems.javaschool.eshop.exceptions.PasswordsDontMatchException;
import com.tsystems.javaschool.eshop.exceptions.UserAlreadyExistsException;
import com.tsystems.javaschool.eshop.dao.UserDAO;
import com.tsystems.javaschool.eshop.model.Role;
import com.tsystems.javaschool.eshop.model.User;
import com.tsystems.javaschool.eshop.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.MultiValueMap;

import javax.servlet.http.HttpSession;
import java.time.LocalDate;

@Service("userService")
public class UserServiceImpl implements UserService {
    private static final int CLIENT_ID = 3;

    private static final String SURNAME_ATTR = "surname";
    private static final String EMAIL_ATTR = "email";
    private static final String BIRTHDATE_ATTR = "birthdate";
    private static final String P_ATTR = "password";
    private static final String CONFIRM_P_ATTR = "confirmPassword";
    private static final String LOGIN_ATTR = "login";

    @Autowired
    UserDAO userDao;

    @Autowired
    HttpSession httpSession;

    @Override
    public boolean isAdmin() {
        Boolean isAdmin = (Boolean) httpSession.getAttribute("isAdmin");
        return isAdmin != null && isAdmin;
    }

    @Override
    public User getUserByLogin(String login) {
        if (login.isEmpty()) {
            return null;
        } else {
            return userDao.getUserByLogin(login);
        }
    }

    @Override
    @Transactional
    public void saveOrUpdateUser(MultiValueMap<String, String> multiValueMap, User user)
            throws MissingRequiredFieldsException, PasswordsDontMatchException {
        if (user != null) {
            checkUserParameters(multiValueMap);
            checkPassword(multiValueMap);
            user.setName(multiValueMap.getFirst("name"));
            user.setSurname(multiValueMap.getFirst(SURNAME_ATTR));
            user.setEmail(multiValueMap.getFirst(EMAIL_ATTR));
            user.setBirthdate(LocalDate.parse(multiValueMap.getFirst(BIRTHDATE_ATTR)));
            user.setPassword(multiValueMap.getFirst(P_ATTR));
            userDao.saveOrUpdateUser(user);
        } else {
            throw new MissingRequiredFieldsException();
        }
    }

    @Transactional
    @Override
    public User createUser(MultiValueMap<String, String> multiValueMap) throws MissingRequiredFieldsException,
            PasswordsDontMatchException, UserAlreadyExistsException {
        checkUserExists(multiValueMap);
        checkUserParameters(multiValueMap);
        checkUserParametersRegistration(multiValueMap);
        checkPassword(multiValueMap);
        User user = new User();
        fillUserFromParameters(multiValueMap, user);
        user.setLogin(multiValueMap.getFirst(LOGIN_ATTR));
        user.setUserRole(getRoleById(CLIENT_ID));
        userDao.saveOrUpdateUser(user);
        return userDao.getUserByLogin(multiValueMap.getFirst(LOGIN_ATTR));
    }

    @Override
    public Role getRoleById(int id) {
        return userDao.getRoleById(id);
    }

    private void checkUserParameters(MultiValueMap<String, String> multiValueMap) throws MissingRequiredFieldsException {
        if (multiValueMap.getFirst(EMAIL_ATTR) == null || multiValueMap.getFirst("name") == null ||
                multiValueMap.getFirst(SURNAME_ATTR) == null ||
                multiValueMap.getFirst(EMAIL_ATTR).isEmpty() || multiValueMap.getFirst("name").isEmpty()) {
            throw new MissingRequiredFieldsException();
        }
    }

    private void checkUserParametersRegistration(MultiValueMap<String, String> multiValueMap)
            throws MissingRequiredFieldsException {
        if (multiValueMap == null || multiValueMap.getFirst(LOGIN_ATTR) == null || multiValueMap.getFirst(LOGIN_ATTR).isEmpty()) {
            throw new MissingRequiredFieldsException();
        }
    }

    private void checkPassword(MultiValueMap<String, String> multiValueMap) throws PasswordsDontMatchException {
        if (multiValueMap == null ||
                multiValueMap.getFirst(P_ATTR) == null ||
                multiValueMap.getFirst(CONFIRM_P_ATTR) == null ||
                multiValueMap.getFirst(P_ATTR).isEmpty() ||
                multiValueMap.getFirst(CONFIRM_P_ATTR).isEmpty() ||
                !multiValueMap.getFirst(P_ATTR).equals(multiValueMap.getFirst(CONFIRM_P_ATTR))) {
            throw new PasswordsDontMatchException();
        }
    }

    private void fillUserFromParameters(MultiValueMap<String, String> multiValueMap, User user) {
        user.setPassword(multiValueMap.getFirst(P_ATTR));
        user.setEmail(multiValueMap.getFirst(EMAIL_ATTR));
        user.setSurname(multiValueMap.getFirst(SURNAME_ATTR));
        user.setName(multiValueMap.getFirst("name"));
        String birthDate = multiValueMap.getFirst(BIRTHDATE_ATTR);
        if (birthDate != null && !birthDate.isEmpty()) {
            user.setBirthdate(LocalDate.parse(birthDate));
        }
    }

    private void checkUserExists(MultiValueMap<String, String> multiValueMap) throws UserAlreadyExistsException {
        if (userDao.getUserByEmail(multiValueMap.getFirst(EMAIL_ATTR)) != null ||
                userDao.getUserByLogin(multiValueMap.getFirst(LOGIN_ATTR)) != null) {
            throw new UserAlreadyExistsException();
        }
    }

    @Override
    @Transactional
    public User getUserById(int userId) {
        return userDao.getUserById(userId);
    }

    @Override
    public User getCurrentUser() {
        return (User) httpSession.getAttribute("user");
    }

    @Override
    public int getCurrentUserId() {
        User user = getCurrentUser();
        return user == null ? -1 : user.getUserId();
    }

    public void setUserDao(UserDAO userDao) {
        this.userDao = userDao;
    }

    public void setHttpSession(HttpSession httpSession) {
        this.httpSession = httpSession;
    }
}
