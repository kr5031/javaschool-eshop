package com.tsystems.javaschool.eshop.service.rest.api;

import com.tsystems.javaschool.eshop.model.Order;
import com.tsystems.javaschool.eshop.model.Product;
import com.tsystems.javaschool.eshop.model.User;

import java.util.List;

/**
 * REST service providing statistics for external clients
 */
public interface StatsRestService {
    /**
     * Get top N products (by number of orders)
     *
     * @param n number of top products
     * @return top N products (by number of orders)
     */
    List<Product> getTopNproducts(int n);

    /**
     * Get top N clients (by number of orders)
     *
     * @param n number of top clients
     * @return top N clients (by number of orders)
     */
    List<User> getTopNclients(int n);

    /**
     * Get all orders for last week
     *
     * @return all orders for last week
     */
    List<Order> getOrdersForLastWeek();

    /**
     * Get all orders for last month
     *
     * @return all orders for last month
     */
    List<Order> getOrdersForLastMonth();
}
