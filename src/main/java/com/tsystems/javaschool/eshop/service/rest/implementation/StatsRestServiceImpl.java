package com.tsystems.javaschool.eshop.service.rest.implementation;

import com.tsystems.javaschool.eshop.model.Order;
import com.tsystems.javaschool.eshop.model.Product;
import com.tsystems.javaschool.eshop.model.User;
import com.tsystems.javaschool.eshop.service.rest.api.StatsRestService;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service("statsRestService")
public class StatsRestServiceImpl implements StatsRestService {

    @Override
    public List<Product> getTopNproducts(int n) {
        return Arrays.asList(new Product());
    }

    @Override
    public List<User> getTopNclients(int n) {
        return new ArrayList<>();
    }

    @Override
    public List<Order> getOrdersForLastWeek() {
        return new ArrayList<>();
    }

    @Override
    public List<Order> getOrdersForLastMonth() {
        return new ArrayList<>();
    }
}
