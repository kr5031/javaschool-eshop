<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page contentType="text/html; charset = UTF-8" %>
<!DOCTYPE html>

<html>
<head>
    <title>Manage products</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
    <script src="/static/bootstrap/js/tests/vendor/jquery.min.js"></script>
    <script src="/static/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<jsp:include page="header.jsp"/>

<div class="container">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3 form-box">
            <div class="form-top">
                <div class="form-top-left">
                    <h3>Add a product</h3>
                    <c:choose>
                        <c:when test="${empty result}">
                            <p>Please, fill the required fields:</p>
                        </c:when>
                        <c:otherwise>
                            <p>${result}</p>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
            <div class="form-bottom">
                <form action="/admin/addProducts" method="post">
                    <div class="form-group">
                        <label class="sr-only" for="name">Product</label>
                        <input type="text" name="name" placeholder="Product name..."
                               class="form-username form-control" id="name">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="price">Price</label>
                        <input type="text" name="price" placeholder="Price..."
                               class="form-username form-control" id="price">
                    </div>
                    <div class="form-group">
                        <select name="categoryId">
                            <c:forEach items="${listCategories}" var="category">
                                <option value="${category.categoryId}">
                                    [${category.categoryId}] ${category.categoryName}
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <select name="manufacturerId">
                            <c:forEach items="${listManufacturers}" var="manufacturer">
                                <option value="${manufacturer.manufacturerId}">
                                    [${manufacturer.manufacturerId}] ${manufacturer.manufacturerName}
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <select name="colorId">
                            <c:forEach items="${listColors}" var="color">
                                <option value="${color.colorId}">
                                    [${color.colorId}] ${color.colorName}
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="weight">Weight</label>
                        <input type="text" name="weight" placeholder="Weight..."
                               class="form-username form-control" id="weight">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="memory">Memory</label>
                        <input type="text" name="memory" placeholder="Memory..."
                               class="form-username form-control" id="memory">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="screen">Screen</label>
                        <input type="text" name="screen" placeholder="Screen..."
                               class="form-username form-control" id="screen">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="description">Description</label>
                        <input type="text" name="description" placeholder="Description..."
                               class="form-username form-control" id="description">
                    </div>
                    <button type="submit" class="btn">Add product</button>
                </form>
            </div>
        </div>
    </div>
</div>

</body>
</html>