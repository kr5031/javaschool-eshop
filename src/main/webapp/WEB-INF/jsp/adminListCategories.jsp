<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<c:forEach items="${listCategories}" var="category">
    <option value="${category.categoryId}">
        [${category.categoryId}] ${category.categoryName}
    </option>
</c:forEach>