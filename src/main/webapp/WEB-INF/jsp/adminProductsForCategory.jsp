<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<c:forEach items="${listProducts}" var="product" varStatus="i">
    <option value="${product.productId}">
        [${product.productId}] ${product.name}
    </option>
</c:forEach>