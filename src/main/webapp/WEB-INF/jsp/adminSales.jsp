<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<%@ page contentType="text/html; charset = UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>JS21 eShop</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
    <script src="/static/bootstrap/js/tests/vendor/jquery.min.js"></script>
    <script src="/static/bootstrap/js/bootstrap.min.js"></script>
    <style>
        /* Remove the navbar's default rounded borders and increase the bottom margin */
        .navbar {
            margin-bottom: 50px;
            border-radius: 0;
        }

        /* Remove the jumbotron's default bottom margin */
        .jumbotron {
            margin-bottom: 0;
        }

        /* Add a gray background color and some padding to the footer */
        footer {
            background-color: #f2f2f2;
            padding: 25px;
        }
    </style>
</head>
<body>

<jsp:include page="header.jsp"/>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-4">
            <h3>Top 10 products:</h3>
            <table class="table table-hover table-condensed table-striped">
                <thead>
                <tr>
                    <th>Product ID</th>
                    <th>Product Name</th>
                    <th>Number sold</th>
                </tr>
                </thead>
                <tbody>
                <c:if test="${!empty listTopProducts}">
                    <c:forEach items="${listTopProducts}" var="product" varStatus="i">
                        <tr>
                            <td>${product.productId}</td>
                            <td>${product.name}</td>
                            <td> ${listNumberOfProductsSold.get(i.count - 1)} </td>
                        </tr>
                    </c:forEach>
                </c:if>
                </tbody>
            </table>
        </div>
        <div class="col-sm-4">
            <h3>Top 10 Clients</h3>
            <table class="table table-hover table-condensed table-striped">
                <thead>
                <tr>
                    <th>Client ID</th>
                    <th>Surname</th>
                    <th>Name</th>
                    <th>Number of orders</th>
                </tr>

                </thead>
                <tbody>
                <c:if test="${!empty listTopUsers}">
                    <c:forEach items="${listTopUsers}" var="user" varStatus="i">
                        <tr>
                            <td>${user.userId}</td>
                            <td>${user.surname}</td>
                            <td>${user.name}</td>
                            <td>${listNumberOfOrders.get(i.count - 1)}</td>
                        </tr>
                    </c:forEach>
                </c:if>
                </tbody>
            </table>
        </div>
        <div class="col-sm-4">
            <h3>Orders amount for last week</h3>
            <table class="table table-hover table-condensed table-striped">
                <thead>
                <tr>
                    <th>Amount</th>
                    <th>Number of orders</th>
                </tr>
                </thead>
                <tbody>
                <c:if test="${!empty listOrdersLastWeek}">
                    <c:forEach items="${listOrdersLastWeek}" var="order" varStatus="i">
                        <c:set var="amount" value="${amount + order.totalSum}"/>
                        <c:set var="number" value="${number + 1}"/>
                    </c:forEach>
                </c:if>
                <tr>
                    <td>${amount}</td>
                    <td>${number}</td>
                </tr>
                </tbody>
            </table>
            <h3>Orders amount for last month</h3>
            <table class="table table-hover table-condensed table-striped">
                <thead>
                <tr>
                    <th>Amount</th>
                    <th>Number of orders</th>
                </tr>
                </thead>
                <tbody>
                <c:if test="${!empty listOrdersLastMonth}">
                    <c:forEach items="${listOrdersLastMonth}" var="monthOrder" varStatus="i">
                        <c:set var="monthAmount" value="${monthAmount + monthOrder.totalSum}"/>
                        <c:set var="monthNumber" value="${monthNumber + 1}"/>
                    </c:forEach>
                </c:if>
                <tr>
                    <td>${monthAmount}</td>
                    <td>${monthNumber}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

</body>
</html>