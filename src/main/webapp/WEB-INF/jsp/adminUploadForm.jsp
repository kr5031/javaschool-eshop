<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page contentType="text/html; charset = UTF-8" %>
<!DOCTYPE html>

<html>
<head>
    <title>Manage products</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
    <script src="/static/bootstrap/js/tests/vendor/jquery.min.js"></script>
    <script src="/static/bootstrap/js/bootstrap.min.js"></script>
    <script>
        function showProducts() {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    $("#productId").html(this.responseText);
                    showPhotos();
                }
            };
            xhttp.open("POST", "/admin/getProductsForCategory", true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            var categoryId = $('#categoryId').find(":selected").val();
            xhttp.send("categoryId=" + categoryId);
        }

        function showPhotos() {
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4 && this.status == 200) {
                    $("#uploadedPhotos").html(this.responseText);
                    $("#uploadProductId").val(productId);
                }
            };
            xhttp.open("POST", "/admin/getUploadedPhotos", true);
            xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            var productId = $('#productId').find(":selected").val();
            xhttp.send("productId=" + productId);
        }

        $(document).ready(function () {
            showProducts();
            showPhotos();
        });
    </script>
</head>
<body>

<jsp:include page="header.jsp"/>

<c:choose>
    <c:when test="${!empty result}">
        <p>${result}</p>
    </c:when>
</c:choose>
<div class="container-fluid">
    <div class="row">
        <div class="row">
            <div class="col-sm-offset-6 col-sm-6">
                <select name="categoryId" id="categoryId" onchange="showProducts()">
                    <jsp:include page="adminListCategories.jsp"/>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-offset-6 col-sm-6">
                <select name="productId" id="productId" onchange="showPhotos()">
                    <jsp:include page="adminProductsForCategory.jsp"/>
                </select>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="text-center">
                    <h3>Already uploaded for the product:</h3>
                </div>
            </div>
        </div>

        <div class="row" id="uploadedPhotos">
            <jsp:include page="adminUploadedPhotos.jsp"/>
        </div>

        <div class="row">
            <div class="col-sm-offset-6" id="uploadForm">
                <script>
                    function openUpload() {
                        window.open('/admin/uploadPhotos?productId=' + $('#productId').find(":selected").val(), '_blank');
                    }
                </script>
                <button onclick="openUpload()">
                Upload form
                </button>
            </div>
        </div>

    </div>
</div>

</body>
</html>