<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page contentType="text/html; charset = UTF-8" %>

<!DOCTYPE html>

<html>
<head>
    <title>Manage products</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
    <script src="/static/bootstrap/js/tests/vendor/jquery.min.js"></script>
    <script src="/static/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<jsp:include page="header.jsp"/>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-offset-6 col-sm-6">
            <c:choose>
                <c:when test="${!empty result}">
                    <p>${result}</p>
                </c:when>
            </c:choose>
            <form method="post" action="/admin/uploadPhotos" enctype="multipart/form-data" id="uploadForm"
                  name="uploadForm">
                Photo 1: <input type="file" name="file">
                Photo 2: <input type="file" name="file">
                Photo 3: <input type="file" name="file">
                <input type="hidden" name="productId" value="${productId}" id="uploadProductId">
                <br>
                <input type="submit" value="Upload">
            </form>
        </div>
    </div>
</div>
</body>
</html>