<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<c:forEach items="${listImages}" var="image" varStatus="i">
    <c:choose>
        <c:when test="${i.count eq 1}">
            <div class="col-sm-2 col-sm-offset-3"
                 style="display: flex; height: 320px; width: 254px; align-items: center; justify-content: center;">
                <img src="${image}" style="flex: none; max-width: 100%; max-height: 100%">
            </div>
        </c:when>
        <c:otherwise>
            <div class="col-sm-2"
                 style="display: flex; height: 320px; width: 254px; align-items: center; justify-content: center;">
                <img src="${image}" style="flex: none; max-width: 100%; max-height: 100%">
            </div>
        </c:otherwise>
    </c:choose>
</c:forEach>