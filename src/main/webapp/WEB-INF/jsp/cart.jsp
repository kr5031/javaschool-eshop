<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page contentType="text/html; charset = UTF-8" %>
<html lang="en">
<head>
    <title>Phones</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
    <script src="/static/bootstrap/js/tests/vendor/jquery.min.js"></script>
    <script src="/static/bootstrap/js/bootstrap.min.js"></script>

</head>
<body>

<jsp:include page="header.jsp"/>

<div class="container">
    <div class="row" id="cart">
        <jsp:include page="cartForm.jsp"/>
    </div>
</div>

</body>
</html>