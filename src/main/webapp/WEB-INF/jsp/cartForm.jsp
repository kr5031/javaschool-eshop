<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<div class="col-xs-8">
    <div class="panel panel-info">
        <div class="panel-heading">
            <div class="panel-title">
                <div class="row">
                    <div class="col-xs-6">
                        <h5><span class="glyphicon glyphicon-shopping-cart"></span> Shopping Cart</h5>
                    </div>
                    <div class="col-xs-6">
                        <a href="/">
                            <button type="button" class="btn btn-primary btn-sm btn-block">
                                <span class="glyphicon glyphicon-share-alt"></span> Continue shopping
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <c:set var="total" value="0"/>
            <c:forEach var="entry" items="${productsInCart}">
                <c:set var="amountForProduct" value="${pricesForProductsInCart.get(entry.key) * entry.value}"/>
                <c:set var="total" value="${total + amountForProduct}"/>
                <div class="row">
                    <div class="col-xs-2"><img class="img-responsive" src="${listImages.get(entry.key)}">
                    </div>
                    <div class="col-xs-4">
                        <h4 class="product-name"><strong>${entry.key.name}</strong></h4>
                        <h4>
                            <small>${entry.key.description}</small>
                        </h4>
                    </div>
                    <div class="col-xs-6">
                        <div class="col-xs-6 text-right">
                            <h6><strong>${pricesForProductsInCart.get(entry.key)} <span
                                    class="text-muted"> x </span></strong>${entry.value} = ${amountForProduct}
                            </h6>
                        </div>
                        <div class="col-xs-4">
                            <input type="text" id="qty" class="form-control input-sm" value="${entry.value}" onchange="updateCart()">
                            <script>
                                function updateCart() {
                                    var xhttp = new XMLHttpRequest();
                                    xhttp.onreadystatechange = function () {
                                        if(this.readyState == 4 && this.status == 200) {
                                            $("#cart").html(this.responseText);
                                        }
                                    };
                                    xhttp.open("POST", "/cart/setQty", true);
                                    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                                    var qty = $("#qty").val();
                                    xhttp.send("productId=${entry.key.productId}&qty=" + qty);
                                }
                            </script>
                        </div>
                        <div class="col-xs-2">
                            <form method="post" action="/cart/delete/${entry.key.productId}">
                                <button type="submit" class="btn btn-link btn-xs">
                                    <span class="glyphicon glyphicon-trash"> </span>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
                <hr>
            </c:forEach>
        </div>
        <hr>
        <div class="row">
            <div class="text-center">
                <div class="col-xs-12">
                    <h6 class="text-left">Delivery cost: ${deliveryMethod.deliveryMethodPrice}</h6>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="text-center">
                <div class="col-xs-9">
                    <h6 class="text-right">Added items?</h6>
                </div>
                <div class="col-xs-3">
                    <button type="button" class="btn btn-default btn-sm btn-block">
                        Update cart
                    </button>
                </div>
            </div>
        </div>

        <div class="panel-footer">
            <div class="row text-center">
                <div class="col-xs-9">
                    <h4 class="text-right">Total <strong>${total}</strong></h4>
                </div>
                <div class="col-xs-3">
                    <a href="/selectDelivery" style="text-decoration: none">
                        <button type="button" class="btn btn-success btn-block">
                            Checkout
                        </button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>