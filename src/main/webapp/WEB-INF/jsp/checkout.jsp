<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:include page="includes.jsp"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Phones</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
    <script src="/static/bootstrap/js/tests/vendor/jquery.min.js"></script>
    <script src="/static/bootstrap/js/bootstrap.min.js"></script>

</head>
<body>

<jsp:include page="header.jsp"/>

<div class='container'>
    <div class='row' style='padding-top:25px; padding-bottom:25px;'>
        <div class='col-md-12'>
            <div id='mainContentWrapper'>
                <div class="col-md-8 col-md-offset-2">
                    <h2 style="text-align: center;">
                        Review Your Order & Complete Checkout
                    </h2>

                    <div class="shopping_cart">
                        <div class="panel-group" id="accordion">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">Review
                                            Your Order</a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <div class="items">
                                            <c:set var="total" value="${deliveryMethod.deliveryMethodPrice}"/>
                                            <c:forEach items="${productsInCart}" var="entry" varStatus="i">
                                                <c:set var="amountForProduct"
                                                       value="${pricesForProductsInCart.get(entry.key) * entry.value}"/>
                                                <c:set var="total" value="${total + amountForProduct}"/>
                                                <div class="col-md-9">
                                                    <table class="table table-striped">
                                                        <tr>
                                                            <td colspan="2">
                                                                <b>${entry.key.name}</b></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <ul>
                                                                    <li>${entry.key.manufacturer}</li>
                                                                    <li>${entry.key.name}</li>
                                                                    <li>${entry.key.color}</li>
                                                                </ul>
                                                            </td>
                                                            <td>
                                                                <b>${entry.key.price} USD</b>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <c:if test="${i.count == 1}">
                                                    <div class="col-md-3">
                                                        <div style="text-align: center;">
                                                            <h3>Delivery:</h3>
                                                            <h3><span>${deliveryMethod.deliveryMethodName}</span>
                                                            </h3>
                                                            <h3><span
                                                                    style="color:green;">${deliveryMethod.deliveryMethodPrice} USD</span>
                                                            </h3>
                                                            <br>
                                                            <h3>Order Total</h3>
                                                            <h3><span style="color:green;">${total} USD</span></h3>
                                                        </div>
                                                    </div>
                                                </c:if>
                                            </c:forEach>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <div style="text-align: center; width:100%;"><a style="width:100%;"
                                                                                    data-toggle="collapse"
                                                                                    data-parent="#accordion"
                                                                                    href="#collapseTwo"
                                                                                    class=" btn btn-success"
                                                                                    onclick="$(this).fadeOut(); $('#payInfo').fadeIn();">Continue
                                        to Billing Information</a></div>
                                </h4>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">Contact
                                        and Billing Information</a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <b>Help us keep your account safe and secure, please verify your billing
                                        information.</b>
                                    <br/><br/>
                                    <form method="post" action="/checkout/confirm">
                                        <table class="table table-striped" style="font-weight: bold;">
                                            <tr>
                                                <td style="width: 175px;">
                                                    <label for="country">Country:</label></td>
                                                <td>
                                                    <input class="form-control" id="country"
                                                           name="country" required="required" type="text"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 175px;">
                                                    <label for="city">City:</label></td>
                                                <td>
                                                    <input class="form-control" id="city"
                                                           name="city" required="required" type="text"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 175px;">
                                                    <label for="postCode">Post code:</label></td>
                                                <td>
                                                    <input class="form-control" id="postCode"
                                                           name="postCode" type="text"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 175px;">
                                                    <label for="street">Street:</label></td>
                                                <td>
                                                    <input class="form-control" id="street" name="street"
                                                           required="required" type="text"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 175px;">
                                                    <label for="house">House:</label></td>
                                                <td>
                                                    <input class="form-control" id="house" name="house"
                                                           required="required" type="text"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 175px;">
                                                    <label for="apartments">Apartments:</label></td>
                                                <td>
                                                    <input class="form-control" id="apartments" name="apartments"
                                                           type="text"/>
                                                </td>
                                            </tr>
                                        </table>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <div style="text-align: center;">
                                                        <button type="submit" class="btn-info"
                                                                style="text-align: center; width: 100%; height: 35px">Create
                                                            order
                                                        </button>
                                                    </div>
                                                </h4>
                                            </div>
                                        </div>
                                        <input type="hidden" name="deliveryMethodId" value="${deliveryMethod.deliveryMethodId}">
                                        <input type="hidden" name="total" value="${total}">
                                        <input type="hidden" name="paymentMethodId" value="${paymentMethod.paymentMethodId}">
                                    </form>
                                </div>
                            </div>
                        </div>

                        <c:if test="${paymentMethod.paymentMethodId == 2}">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <div style="text-align: center;"><a data-toggle="collapse"
                                                                        data-parent="#accordion"
                                                                        href="#collapseThree"
                                                                        class=" btn   btn-success" id="payInfo"
                                                                        style="width:100%;display: none;" onclick="$(this).fadeOut();
                   document.getElementById('collapseThree').scrollIntoView()">Enter Payment Information </a>
                                    </div>
                                </h4>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                        <b>Payment Information</b>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <span class='payment-errors'></span>
                                    <fieldset>
                                        <legend>What method would you like to pay with today?</legend>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="card-holder-name">Name on
                                                Card</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" stripe-data="name"
                                                       id="name-on-card" placeholder="Card Holder's Name">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-3 control-label" for="card-number">Card
                                                Number</label>
                                            <div class="col-sm-9">
                                                <input type="text" class="form-control" stripe-data="number"
                                                       id="card-number" placeholder="Debit/Credit Card Number">
                                                <br/>
                                                <div><img class="pull-right"
                                                          src="https://s3.amazonaws.com/hiresnetwork/imgs/cc.png"
                                                          style="max-width: 250px; padding-bottom: 20px;">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label" for="expiry-month">Expiration
                                                    Date</label>
                                                <div class="col-sm-9">
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <select class="form-control col-sm-2"
                                                                    data-stripe="exp-month" id="card-exp-month"
                                                                    style="margin-left:5px;">
                                                                <option>Month</option>
                                                                <option value="01">Jan (01)</option>
                                                                <option value="02">Feb (02)</option>
                                                                <option value="03">Mar (03)</option>
                                                                <option value="04">Apr (04)</option>
                                                                <option value="05">May (05)</option>
                                                                <option value="06">June (06)</option>
                                                                <option value="07">July (07)</option>
                                                                <option value="08">Aug (08)</option>
                                                                <option value="09">Sep (09)</option>
                                                                <option value="10">Oct (10)</option>
                                                                <option value="11">Nov (11)</option>
                                                                <option value="12">Dec (12)</option>
                                                            </select>
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <select class="form-control" data-stripe="exp-year"
                                                                    id="card-exp-year">
                                                                <option value="2016">2016</option>
                                                                <option value="2017">2017</option>
                                                                <option value="2018">2018</option>
                                                                <option value="2019">2019</option>
                                                                <option value="2020">2020</option>
                                                                <option value="2021">2021</option>
                                                                <option value="2022">2022</option>
                                                                <option value="2023">2023</option>
                                                                <option value="2024">2024</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label" for="cvv">Card CVC</label>
                                                <div class="col-sm-3">
                                                    <input type="text" class="form-control" stripe-data="cvc"
                                                           id="card-cvc" placeholder="Security Code">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-offset-3 col-sm-9">
                                                </div>
                                            </div>
                                    </fieldset>
                                    <button class="btn btn-success btn-lg" style="width:100%;">Pay
                                        Now
                                    </button>
                                    <br/>
                                    <div style="text-align: left;"><br/>
                                        By submiting this order you are agreeing to our <a href="/legal/billing/">universal
                                            billing agreement</a>, and <a href="/legal/terms/">terms of service</a>.
                                        If you have any questions about our products or services please contact us
                                        before placing this order.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </c:if>
                </div>
                </form>
            </div>
        </div>
    </div>