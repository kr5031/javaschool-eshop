<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page contentType="text/html; charset = UTF-8" %>

<p class="bg-info text-center">Manufacturers</p>
<form action="/product" method="post" id="filterForm">
    <c:if test="${!empty listManufacturers}">
        <c:forEach items="${listManufacturers}" var="manufacturer">
            <div style="display: inline-flex">
                <label class="checkbox-inline">
                    <input type="checkbox" name="manufacturerId"
                           value="${manufacturer.manufacturerId}"
                           onclick="updateProducts()">${manufacturer.manufacturerName}
                </label>
            </div>
        </c:forEach>
    </c:if>
    <br>
    <br>
    <p class="bg-info text-center">Price</p>
    <div class="input-group">
        <input type="text" class="form-control input-sm" placeholder="from" name="priceFrom"
               onchange="updateProducts()"/>
        <span class="input-group-btn" style="width:0px;"></span>
        <input type="text" class="form-control input-sm" placeholder="to" name="priceTo" onchange="updateProducts()"/>
    </div>
    <br>
    <br>
    <c:if test="${!empty filterFile}">
        <jsp:include page="${filterFile}"/>
    </c:if>
    <br><br>
    <c:choose>
        <c:when test="${!empty listProducts}">
            <input type="hidden" name="categoryId" value="${listProducts[0].category.categoryId}">
        </c:when>
        <c:otherwise>
            <input type="hidden" name="categoryId" value="${lastCategoryIdFiltered}">
        </c:otherwise>
    </c:choose>

    <div align="center"><input class="btn btn-info" value="Filter" id="filter" onclick="updateProducts()"></div>
</form>