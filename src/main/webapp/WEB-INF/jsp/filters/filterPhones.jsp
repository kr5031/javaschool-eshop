<p class="bg-info text-center">Memory</p>
<div style="display: inline-flex">
    <label class="checkbox-inline"><input type="checkbox" name="memory"
                                          value="16" onclick="updateProducts()">16</label>
</div>
<div style="display: inline-flex">
    <label class="checkbox-inline"><input type="checkbox" name="memory"
                                          value="32" onclick="updateProducts()">32</label>
</div>
<div style="display: inline-flex">
    <label class="checkbox-inline"><input type="checkbox" name="memory"
                                          value="64" onclick="updateProducts()">64</label>
</div>
<div style="display: inline-flex">
    <label class="checkbox-inline"><input type="checkbox" name="memory"
                                          value="128" onclick="updateProducts()">128</label>
</div>
<br>
<br>
<p class="bg-info text-center">Screen</p>
<div class="input-group">
    <input type="text" class="form-control input-sm" placeholder="from" name="screenFrom" onchange="updateProducts()"/>
    <span class="input-group-btn" style="width:0px;"></span>
    <input type="text" class="form-control input-sm" placeholder="to" name="screenTo" onchange="updateProducts()"/>
</div>