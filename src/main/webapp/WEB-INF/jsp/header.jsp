<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page contentType = "text/html; charset = UTF-8" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>JS21 eShop</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
        /* Remove the navbar's default rounded borders and increase the bottom margin */
        .navbar {
            margin-bottom: 50px;
            border-radius: 0;
        }

        /* Remove the jumbotron's default bottom margin */
        .jumbotron {
            margin-bottom: 0;
        }

        /* Add a gray background color and some padding to the footer */
        footer {
            background-color: #f2f2f2;
            padding: 25px;
        }
    </style>
</head>
<body>

<div class="jumbotron">
    <div class="container text-center">
        <img src="/static/simpson.jpg" width="360px" height="202px" align="left">
        <h1>JS21 eShop</h1>
        <p>Shut up and take my money!</p>
    </div>
</div>

<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>

        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
                <li class="active"><a href="/index.jsp">Home</a></li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Products<span
                            class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <c:forEach items="${listCategories}" var="category">
                        <li><a href="/category/${category.categoryName}">${category.categoryName}</a></li>
                        </c:forEach>
                    </ul>
                </li>
                <li><a href="/myOrders">My Orders</a></li>
                <c:if test="${isAdmin}">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Admin panel<span
                                class="caret"></span></a>
                        <ul class="dropdown-menu">
                            <li><a href="/admin/orders">Manage orders</a></li>
                            <li><a href="/admin/sales">Sales stats</a></li>
                            <li><a href="/admin/addProducts">Add products</a></li>
                            <li><a href="/admin/uploadForm">Upload photos</a></li>
                            <li><a href="/admin/manageCategories">Manage categories</a></li>
                            <li><a href="/admin/manageManufacturers">Manage manufacturers</a></li>
                            <li><a href="/admin/manageColors">Manage colors</a></li>
                        </ul>
                    </li>
                </c:if>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/profile">
                    <c:choose>
                        <c:when test="${!empty user}">
                            <span class="glyphicon glyphicon-user"></span>
                            <c:out value=" ${user.name}"/>
                        </c:when>
                        <c:otherwise>
                            <span class="glyphicon glyphicon-log-in"></span>
                            <c:out value=" Guest"/>
                        </c:otherwise>
                    </c:choose>
                </a></li>
                <li><a href="/cart"><span class="glyphicon glyphicon-shopping-cart"></span> Cart</a></li>
                <c:choose>
                    <c:when test="${!empty user}">
                        <li><a href="/logout">
                            <span class="glyphicon glyphicon-log-out"></span>
                            Logout
                        </a></li>
                    </c:when>
                </c:choose>
            </ul>
        </div>
    </div>
</nav>