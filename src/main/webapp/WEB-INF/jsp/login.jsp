<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
    <script src="/static/bootstrap/js/tests/vendor/jquery.min.js"></script>
    <script src="/static/bootstrap/js/bootstrap.min.js"></script>
    <!-- CSS -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="/static/login/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/static/login/assets/css/form-elements.css">
    <link rel="stylesheet" href="/static/login/assets/css/style.css">

    <!-- Favicon and touch icons -->
    <link rel="shortcut icon" href="/static/login/assets/ico/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144"
          href="/static/login/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114"
          href="/static/login/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72"
          href="/static/login/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="/static/login/assets/ico/apple-touch-icon-57-precomposed.png">
</head>
<body>

<jsp:include page="header.jsp"/>

<div class="container">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3 form-box">
            <div class="form-top">
                <div class="form-top-left">
                    <h3>Welcome to eShop</h3>
                    <c:if test="${!empty result}">
                        <p>${result}</p>
                    </c:if>
                    <c:choose>
                        <c:when test="${!empty wrongCredentials}">
                            <p>Wrong credentials!</p>
                        </c:when>
                        <c:otherwise>
                            <p>Enter your username and password to log on:</p>
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="form-top-right">
                    <i class="fa fa-key"></i>
                </div>
            </div>
            <div class="form-bottom">
                <form role="form" action="/login" method="post" class="login-form">
                    <div class="form-group">
                        <label class="sr-only" for="form-username">Username</label>
                        <input type="text" name="login" placeholder="Username..."
                               class="form-username form-control" id="form-username">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="form-password">Password</label>
                        <input type="password" name="password" placeholder="Password..."
                               class="form-password form-control" id="form-password">
                    </div>
                    <button type="submit" class="btn">Login</button>
                    <c:if test="${!empty toRedirectAfter}">
                        <input type="hidden" name="toRedirectAfter" value="${toRedirectAfter}">
                    </c:if>
                </form>
                <br>
                <form role="form" action="/register" class="login-form">
                    <c:if test="${!empty toRedirectAfter}">
                        <input type="hidden" name="toRedirectAfter" value="${toRedirectAfter}">
                    </c:if>
                    <button type="submit" class="btn btn-primary" style="background:royalblue">Register</button>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>