<jsp:include page="includes.jsp"/>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>JS21 eShop</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="http://cdn.bootcss.com/animate.css/3.5.1/animate.min.css">
    <jsp:include page="bootstrap.jsp"/>
</head>
<body>

<jsp:include page="header.jsp"/>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-12">
            <div id="first-slider">
                <div id="carousel-example-generic" class="carousel slide carousel-fade">
                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <!-- Item 1 -->
                        <div class="item active slide1">
                            <div class="row">
                                <br>
                                <div class="container">
                                    <div class="col-md-3 text-right">
                                        <img style="max-width: 200px;" data-animation="animated zoomInLeft"
                                             src="http://s20.postimg.org/pfmmo6qj1/window_domain.png">
                                    </div>
                                    <div class="col-md-9 text-left">
                                        <h3 data-animation="animated bounceInDown">Welcome to JS21 eShop</h3>
                                        <h4 data-animation="animated bounceInUp">We're going to kill one puppy every
                                            hour until
                                            you buy anything!</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Wrapper for slides-->
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>