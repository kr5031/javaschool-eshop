<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page contentType="text/html; charset = UTF-8" %>
<!DOCTYPE html>

<html>
<head>
    <title>Manage categories</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
    <script src="/static/bootstrap/js/tests/vendor/jquery.min.js"></script>
    <script src="/static/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<jsp:include page="header.jsp"/>

<div class="container">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3 form-box">
            <div class="form-top">
                <div class="form-top-left">
                    <h3>Manage categories</h3>
                    <c:choose>
                        <c:when test="${empty result}">
                            <p>Please, fill the required fields:</p>
                        </c:when>
                        <c:otherwise>
                            <p>${result}</p>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
            <div class="form-bottom">
                <h3>Existing categories:</h3><br>
                <table>
                    <tr>
                        <td>ID</td>
                        <td>Name</td>
                    </tr>
                    <c:forEach items="${listCategories}" var="category">
                        <tr>
                            <td>${category.categoryId}</td>
                            <td>${category.categoryName}</td>
                        </tr>
                    </c:forEach>
                </table>
                <form action="/admin/addCategory" method="post">
                    <div class="form-group">
                        <label class="sr-only" for="categoryName">Add Category name</label>
                        <input type="text" name="categoryName" placeholder="Category name..."
                               class="form-username form-control" id="categoryName">
                    </div>
                    <button type="submit" class="btn">Add category</button>
                    <br><br>
                </form>

                <form action="/admin/deleteCategory" method="post">
                    <div class="form-group">
                        <select name="categoryId">
                            <c:forEach items="${listCategories}" var="category">
                                <option value="${category.categoryId}">
                                    [${category.categoryId}] ${category.categoryName}
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                    <button type="submit" class="btn">Delete category</button>
                </form>

            </div>
        </div>
    </div>
</div>

</body>
</html>