<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page contentType="text/html; charset = UTF-8" %>
<!DOCTYPE html>

<html>
<head>
    <title>Manage colors</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
    <script src="/static/bootstrap/js/tests/vendor/jquery.min.js"></script>
    <script src="/static/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<jsp:include page="header.jsp"/>

<div class="container">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3 form-box">
            <div class="form-top">
                <div class="form-top-left">
                    <h3>Add a product</h3>
                    <c:choose>
                        <c:when test="${empty result}">
                            <p>Please, fill the required fields:</p>
                        </c:when>
                        <c:otherwise>
                            <p>${result}</p>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
            <div class="form-bottom">
                <h3>Existing colors:</h3><br>
                <table class="table-bordered">
                    <tr>
                        <td width="50%">ID</td>
                        <td width="100%">Name</td>
                    </tr>
                    <c:forEach items="${listColors}" var="color">
                        <tr>
                            <td width="50%">${color.colorId}</td>
                            <td width="100%">${color.colorName}</td>
                        </tr>
                    </c:forEach>
                </table>
                <br><br>
                <form action="/admin/addColor" method="post">
                    <div class="form-group">
                        <label class="sr-only" for="colorName">Add Color name</label>
                        <input type="text" name="colorName" placeholder="Color name..."
                               class="form-username form-control" id="colorName">
                    </div>
                    <button type="submit" class="btn">Add color</button>
                    <br><br>
                </form>

                <form action="/admin/deleteColor" method="post">
                    <div class="form-group">
                        <select name="colorId">
                            <c:forEach items="${listColors}" var="color">
                                <option value="${color.colorId}">
                                    [${color.colorId}] ${color.colorName}
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                    <button type="submit" class="btn">Delete color</button>
                </form>
            </div>
        </div>
    </div>
</div>

</body>
</html>