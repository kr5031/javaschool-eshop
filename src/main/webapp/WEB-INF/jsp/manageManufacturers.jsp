<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page contentType="text/html; charset = UTF-8" %>
<!DOCTYPE html>

<html>
<head>
    <title>Manage manufacturers</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
    <script src="/static/bootstrap/js/tests/vendor/jquery.min.js"></script>
    <script src="/static/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>

<jsp:include page="header.jsp"/>

<div class="container">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3 form-box">
            <div class="form-top">
                <div class="form-top-left">
                    <h3>Add a product</h3>
                    <c:choose>
                        <c:when test="${empty result}">
                            <p>Please, fill the required fields:</p>
                        </c:when>
                        <c:otherwise>
                            <p>${result}</p>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
            <div class="form-bottom">
                <h3>Existing manufacturers:</h3><br>
                <table class="table-bordered">
                    <tr>
                        <td width="50%">ID</td>
                        <td width="50%">Name</td>
                    </tr>
                    <c:forEach items="${listManufacturers}" var="manufacturer">
                        <tr>
                            <td>${manufacturer.manufacturerId}</td>
                            <td>${manufacturer.manufacturerName}</td>
                        </tr>
                    </c:forEach>
                </table>
                <br><br>
                <form action="/admin/addManufacturer" method="post">
                    <div class="form-group">
                        <label class="sr-only" for="manufacturerName">Add Manufacturer name</label>
                        <input type="text" name="manufacturerName" placeholder="Manufacturer name..."
                               class="form-username form-control" id="manufacturerName">
                    </div>
                    <button type="submit" class="btn">Add manufacturer</button>
                    <br><br>
                </form>

                <form action="/admin/deleteManufacturer" method="post">
                    <div class="form-group">
                        <select name="manufacturerId">
                            <c:forEach items="${listManufacturers}" var="manufacturer">
                                <option value="${manufacturer.manufacturerId}">
                                    [${manufacturer.manufacturerId}] ${manufacturer.manufacturerName}
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                    <button type="submit" class="btn">Delete manufacturer</button>
                </form>

            </div>
        </div>
    </div>
</div>

</body>
</html>