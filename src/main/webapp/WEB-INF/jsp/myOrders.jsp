<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<%@ page contentType="text/html; charset = UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>JS21 eShop</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
    <script src="/static/bootstrap/js/tests/vendor/jquery.min.js"></script>
    <script src="/static/bootstrap/js/bootstrap.min.js"></script>
    <style>
        /* Remove the navbar's default rounded borders and increase the bottom margin */
        .navbar {
            margin-bottom: 50px;
            border-radius: 0;
        }

        /* Remove the jumbotron's default bottom margin */
        .jumbotron {
            margin-bottom: 0;
        }

        /* Add a gray background color and some padding to the footer */
        footer {
            background-color: #f2f2f2;
            padding: 25px;
        }
    </style>
</head>
<body>

<jsp:include page="header.jsp"/>

<div class="container-fluid">
    <div class="row">

        <div class="col-sm-10">
            <table class="table table-hover table-condensed table-striped">
                <thead>
                <tr>
                    <th>OrderID</th>
                    <th>Date</th>
                    <th>Name</th>
                    <th>Surname</th>
                    <th>Address</th>
                    <th>Payment Method</th>
                    <th>Delivery Method</th>
                    <th>Total Sum</th>
                    <th>Order status</th>
                </tr>
                </thead>

                <tbody>
                <c:if test="${!empty listOrders}">
                    <c:forEach items="${listOrders}" var="order">
                        <a href="/myOrders/${order.orderId}">
                            <tr style="cursor: hand"
                                onclick="window.location.href='/myOrders/${order.orderId}'; return false">
                                <td>${order.orderId}</td>
                                <td>${order.orderDate}</td>
                                <td>${order.user.name}</td>
                                <td>${order.user.surname}</td>
                                <td>${order.address}</td>
                                <td>${order.paymentMethod}</td>
                                <td>${order.deliveryMethod}</td>
                                <td>${order.totalSum}</td>
                                <td>${order.orderStatus.orderStatusName}</td>
                            </tr>
                        </a>
                    </c:forEach>
                </c:if>
                </tbody>
            </table>
        </div>
    </div>
</div>

</body>
</html>