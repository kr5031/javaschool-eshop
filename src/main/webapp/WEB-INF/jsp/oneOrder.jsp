<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<%@ page contentType="text/html; charset = UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>JS21 eShop</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
    <script src="/static/bootstrap/js/tests/vendor/jquery.min.js"></script>
    <script src="/static/bootstrap/js/bootstrap.min.js"></script>

</head>
<body>

<jsp:include page="header.jsp"/>

<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="invoice-title">
                <h2>Order #${order.orderId}</h2>
            </div>
            <hr>
            <div class="row">
                <div class="col-xs-6">
                    <address>
                        <strong>Billed To:</strong><br>
                        ${order.user.name} ${order.user.surname}<br>
                        ${order.address.street} ${order.address.house}<br>
                        Apt. ${order.address.apartments}<br>
                        ${order.address.city.cityName}, ${order.address.city.country.countryName} ${order.address.postCode}
                    </address>
                </div>
                <div class="col-xs-6 text-right">
                    <address>
                        <strong>Shipped To:</strong><br>
                        ${order.user.name} ${order.user.surname}<br>
                        ${order.address.street} ${order.address.house}<br>
                        Apt. ${order.address.apartments}<br>
                        ${order.address.city.cityName}, ${order.address.city.country.countryName} ${order.address.postCode}
                    </address>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-6">
                    <address>
                        <strong>Payment Method:</strong><br>
                        ${order.paymentMethod}<br>
                        ${order.user.email}
                    </address>
                </div>
                <div class="col-xs-6 text-right">
                    <address>
                        <strong>Order Date:</strong><br>
                        ${order.orderDate}<br><br>
                    </address>
                </div>
                <div class="col-xs-6">
                    <address>
                        <strong>Order status:</strong><br>
                        ${order.orderStatus.orderStatusName}<br>
                    </address>
                </div>
                <div class="col-xs-6 text-right">
                    <address>
                        <strong>Delivery method:</strong><br>
                        ${order.deliveryMethod.deliveryMethodName}<br><br>
                    </address>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><strong>Order summary</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-condensed">
                            <thead>
                            <tr>
                                <td><strong>Num.</strong></td>
                                <td><strong>Item</strong></td>
                                <td class="text-center"><strong>Price</strong></td>
                                <td class="text-center"><strong>Quantity</strong></td>
                                <td class="text-right"><strong>Total, USD</strong></td>
                            </tr>
                            </thead>
                            <tbody>
                            <c:set var="subtotal" scope="page" value="0"/>
                            <c:forEach items="${listProducts}" var="productInOrder" varStatus="i">
                                <tr>
                                    <td>${i.count}</td>
                                    <td>${productInOrder.product.name}</td>
                                    <td class="text-center">${productInOrder.priceInOrder}</td>
                                    <td class="text-center">${productInOrder.qtyInOrder}</td>
                                    <td class="text-right">${productInOrder.priceInOrder * productInOrder.qtyInOrder}</td>
                                    <c:set var="subtotal" value="${subtotal + productInOrder.priceInOrder * productInOrder.qtyInOrder}"/>
                                </tr>
                            </c:forEach>
                            <tr>
                                <td class="thick-line"></td>
                                <td class="thick-line"></td>
                                <td class="thick-line"></td>
                                <td class="thick-line text-center"><strong>Subtotal</strong></td>
                                <td class="thick-line text-right">${subtotal}</td>
                            </tr>
                            <tr>
                                <td class="no-line"></td>
                                <td class="no-line"></td>
                                <td class="no-line"></td>
                                <td class="no-line text-center"><strong>Shipping</strong></td>
                                <td class="no-line text-right">${order.deliveryMethod.deliveryMethodPrice}</td>
                            </tr>
                            <tr>
                                <td class="no-line"></td>
                                <td class="no-line"></td>
                                <td class="no-line"></td>
                                <td class="no-line text-center"><strong>Total</strong></td>
                                <td class="no-line text-right">${order.totalSum}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>