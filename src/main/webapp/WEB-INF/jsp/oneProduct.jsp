<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>JS21 eShop</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
    <script src="/static/bootstrap/js/tests/vendor/jquery.min.js"></script>
    <script src="/static/bootstrap/js/bootstrap.min.js"></script>

</head>
<body>

<jsp:include page="header.jsp"/>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-2">
            <c:choose>
                <c:when test="${!empty listImages}">
                    <c:forEach items="${listImages}" var="image">
                        <div class="thumbnail" style="align-content: center">
                            <img src="${image}">
                        </div>
                    </c:forEach>
                </c:when>
                <c:otherwise>
                    <div class="thumbnail" style="align-content: center">
                        <img src="/static/photo/no-image-available.jpg">
                    </div>
                </c:otherwise>
            </c:choose>
        </div>
        <div class="col-sm-6 col-sm-offset-1">
            <h2>
                <p class="text-center">${product.manufacturer} ${product.name}<br>
                    <small>${product.description}</small>
                </p>
            </h2>

            <table class="table table-hover table-condensed">
                <tr>
                    <td>Memory</td>
                    <td>${product.memory} Gb</td>
                </tr>
                <tr>
                    <td>Color</td>
                    <td>${product.color}</td>
                </tr>
                <tr>
                    <td>Weight</td>
                    <td>${product.weight} g</td>
                </tr>
                <tr>
                    <td>
                        <p class="text-primary"><b>Price</b></p>
                    </td>
                    <td>
                        <p class="text-primary"><b>${product.price} USD</b></p>
                    </td>
                </tr>
            </table>
            <form action="/cart/add" method="post">
                <div class="col-sm-3">
                    <div class="input-group">
                        <span class="input-group-addon" style="height: 46px; width: 20px">Quantity</span>
                        <input id="qty" type="text" class="form-control" name="qty" style="height: 46px" value="1">
                        <input type="hidden" name="productId" value="${product.productId}">
                    </div>
                </div>
                <div class="col-sm-4">
                    <span class="glyphicon glyphicon-shopping-cart"></span>&nbsp;&nbsp;<input type="submit"
                                                                                  class="btn btn-info btn-lg"
                                                                                  value="Add to cart">
                </div>
            </form>

        </div>
    </div>

</body>
</html>