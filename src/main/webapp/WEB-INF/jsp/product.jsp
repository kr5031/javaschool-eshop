<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Products</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/static/products.css">
    <script src="/static/bootstrap/js/tests/vendor/jquery.min.js"></script>
    <script src="/static/bootstrap/js/bootstrap.min.js"></script>
    <script>
        function updateProducts() {
            $.post('/productFiltered', $('#filterForm').serialize(), function (data) {
                $("#filteredProducts").html(data);
            });
        }
    </script>
</head>
<body>

<jsp:include page="header.jsp"/>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-2">
            <jsp:include page="/WEB-INF/jsp/filters/filterDefault.jsp"/>
        </div>
        <div class="col-sm-10" id="filteredProducts">
            <jsp:include page="/WEB-INF/jsp/productForm.jsp"/>
        </div>
    </div>
</div>

</body>
</html>
