<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<div class="container-fluid">
    <c:if test="${!empty listProducts}">
        <c:forEach items="${listProducts}" var="product" varStatus="i">
            <c:if test="${i.count mod 5 eq 1}">
                <div class="items row">
            </c:if>
            <div class="col-sm-2"
                 style="border-style:solid; border-width: 3px; border-color: #eee; height: 280px; margin-right: 2px;"
                 onclick="window.open('/product/${product.productId}', '_blank')">
                <div data-price="${product.price}" class="row item" style="margin: auto">
                    <div class="col-sm-12">
                        <img class="img-item"
                             src="/getPreviewPhoto?productId=${product.productId}"
                             alt="${product.description}" class="img-item"/>
                    </div>
                </div>
                <div class="row info" style="height: 100px;">
                    <div class="col-sm-12">
                        <h3>${product.manufacturer.manufacturerName}&nbsp;${product.name}</h3>
                        <p class="description">${product.description}</p>
                        <h5>$${product.price}</h5>
                    </div>
                </div>
            </div>
            <c:if test="${i.count mod 5 eq 0}">
                </div>
            </c:if>
        </c:forEach>
    </c:if>
</div>
