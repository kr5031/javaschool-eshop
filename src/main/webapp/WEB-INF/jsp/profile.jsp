<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<%@ page contentType="text/html; charset = UTF-8" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>JS21 eShop</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
    <script src="/static/bootstrap/js/tests/vendor/jquery.min.js"></script>
    <script src="/static/bootstrap/js/bootstrap.min.js"></script>

    <style>
        /* Remove the navbar's default rounded borders and increase the bottom margin */
        .navbar {
            margin-bottom: 50px;
            border-radius: 0;
        }

        /* Remove the jumbotron's default bottom margin */
        .jumbotron {
            margin-bottom: 0;
        }

        /* Add a gray background color and some padding to the footer */
        footer {
            background-color: #f2f2f2;
            padding: 25px;
        }
    </style>
</head>
<body>

<jsp:include page="header.jsp"/>

<div class="container">
    <div class="row">
        <div class="col-sm-6">
            <form action="/updateUser" method="post">
                <table class="table table-hover">
                    <tbody>

                    <tr>
                        <td>
                            Login
                        </td>
                        <td>
                            <input class="form-control" id="disabledInput" type="text" value="${user.login}" disabled>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td>Name</td>
                        <td><input id="name" type="text" class="form-control" name="name" value="${user.name}"></td>
                    </tr>
                    <tr>
                        <td>Surname</td>
                        <td><input id="surname" type="text" class="form-control" name="surname"
                                   value="${user.surname}"></td>
                    </tr>
                    <tr>
                        <td>E-mail</td>
                        <td><input id="email" type="text" class="form-control" name="email" value="${user.email}">
                        </td>
                    </tr>
                    <tr>
                        <td>Password</td>
                        <td><input id="password" type="password" class="form-control" name="password"
                                   value="${user.password}"></td>
                    </tr>
                    <tr>
                        <td>Confirm password</td>
                        <td><input id="confirmPassword" type="password" class="form-control" name="confirmPassword"
                                   value=""></td>
                    </tr>
                    </tbody>
                </table>
                <c:choose>
                    <c:when test="${missingRequiredFields}">
                        <c:out value="Missing required fields"/>
                    </c:when>
                </c:choose>
                <c:choose>
                    <c:when test="${passwordsDontMatch}">
                        <c:out value="Passwords don't match"/>
                    </c:when>
                </c:choose>
                <input type="submit" class="btn btn-info" value="Update">
            </form>
        </div>
    </div>
</div>

</body>
</html>