<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Laptops</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
    <script src="/static/bootstrap/js/tests/vendor/jquery.min.js"></script>
    <script src="/static/bootstrap/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:400,100,300,500">
    <link rel="stylesheet" href="/static/login/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/static/login/assets/css/form-elements.css">
    <link rel="stylesheet" href="/static/login/assets/css/style.css">

    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

    <link rel="shortcut icon" href="/static/login/assets/ico/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144"
          href="/static/login/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114"
          href="/static/login/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72"
          href="/static/login/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="/static/login/assets/ico/apple-touch-icon-57-precomposed.png">

</head>
<body>
<jsp:include page="header.jsp"/>
<c:choose>
    <c:when test="${missingRequiredFields}">
        <h2>Missing required fields</h2>
    </c:when>
    <c:when test="${passwordsDontMatch}">
        <h2>Passwords don't match</h2>
    </c:when>
</c:choose>

<div class="container">
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3 form-box">
            <div class="form-top">
                <div class="form-top-left">
                    <h3>Register form</h3>
                    <c:choose>
                        <c:when test="${empty result}">
                            <p>Please, fill the required fields:</p>
                        </c:when>
                        <c:otherwise>
                            <p>${result}</p>
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="form-top-right">
                    <i class="fa fa-key"></i>
                </div>
            </div>
            <div class="form-bottom">
                <form role="form" action="/register" method="post" class="login-form">
                    <div class="form-group">
                        <label class="sr-only" for="login">Login</label>
                        <input type="text" name="login" placeholder="Login..."
                               class="form-username form-control" id="login">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="name">Name</label>
                        <input type="text" name="name" placeholder="Name..."
                               class="form-username form-control" id="name">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="surname">Surname</label>
                        <input type="text" name="surname" placeholder="Surname..."
                               class="form-username form-control" id="surname">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="email">E-mail</label>
                        <input type="text" name="email" placeholder="E-mail..."
                               class="form-username form-control" id="email">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="form-password">Password</label>
                        <input type="password" name="password" placeholder="Password..."
                               class="form-password form-control" id="form-password">
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="form-password">Password</label>
                        <input type="password" name="confirmPassword" placeholder="Confirm password..."
                               class="form-password form-control" id="confirmPassword">
                    </div>
                    <button type="submit" class="btn">Register</button>
                </form>
            </div>
        </div>
    </div>
</div>

</body>
</html>