<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page contentType="text/html; charset = UTF-8" %>
<%@ page session="false" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Delivery</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/static/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/static/login/assets/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/static/bootstrap/css/deliveryMethod.css" type="text/css">
</head>
<body>

<jsp:include page="header.jsp"/>

<div class="container">
    <div class="row">
        <!-- RADIO BUTTONS BLOCK -->
        <div class="col-xs-12 col-sm-6 col-md-5">
            <h3 class="text-center">Select delivery method</h3>
            <div class="frb-group">
                <form action="/checkout" method="post">
                    <c:forEach items="${listDeliveryMethods}" var="deliveryMethod" varStatus="i">
                        <div class="frb frb-success">
                            <input type="radio" id="radio-button-${i.count}" name="deliveryMethodId"
                                   value="${deliveryMethod.deliveryMethodId}">
                            <label for="radio-button-${i.count}">
                                <span class="frb-title">${deliveryMethod.deliveryMethodName}</span>
                                <span class="frb-description">${deliveryMethod.deliveryMethodPrice} USD</span>
                            </label>
                        </div>
                    </c:forEach>
                    <br><br>
                    <c:forEach items="${listPaymentMethods}" var="paymentMethod" varStatus="i">
                        <div class="frb frb-success">
                            <input type="radio" id="paymentId-${i.count}" name="paymentMethodId"
                                   value="${paymentMethod.paymentMethodId}">
                            <label for="paymentId-${i.count}">
                                <span class="frb-title">${paymentMethod.paymentMethodName}</span>
                            </label>
                        </div>
                    </c:forEach>
                    <br><br>
                    <button type="submit" class="btn btn-primary" style="background:royalblue">Proceed to checkout
                    </button>
                </form>
            </div>
        </div>
    </div>
</body>
</html>