package com.tsystems.javaschool.eshop.model;

import org.junit.Test;

import static org.junit.Assert.*;

public class AddressTest {
    @Test
    public void getAddressId() throws Exception {
        Address address = getAddress();
        assertEquals(777, address.getAddressId());
    }

    @Test
    public void setAddressId() throws Exception {
        Address address = getAddress();
        address.setAddressId(0);
        assertEquals(0, address.getAddressId());
    }

    @Test
    public void getCity() throws Exception {
        Address address = getAddress();

        Country country = new Country();
        country.setCountryName("USA");
        country.setCountryId(1);
        City city = new City();
        city.setCityName("Washingthon");
        city.setCountry(country);

        assertEquals(city, address.getCity());
    }

    @Test
    public void setCity() throws Exception {
        Address address = getAddress();

        Country country = new Country();
        country.setCountryName("USA");
        country.setCountryId(1);
        City city = new City();
        city.setCityName("Miami");

        address.setCity(city);

        assertEquals(city, address.getCity());
    }

    @Test
    public void getPostCode() throws Exception {
        Address address = getAddress();
        assertEquals(111111, address.getPostCode());
    }

    @Test
    public void setPostCode() throws Exception {
        Address address = getAddress();
        address.setPostCode(222222);
        assertEquals(222222, address.getPostCode());
    }

    @Test
    public void getStreet() throws Exception {
        Address address = getAddress();
        assertEquals("5 line", address.getStreet());
    }

    @Test
    public void setStreet() throws Exception {
        Address address = getAddress();
        address.setStreet("6 line");
        assertEquals("6 line", address.getStreet());
    }

    @Test
    public void getHouse() throws Exception {
        Address address = getAddress();
        assertEquals(1, address.getHouse());
    }

    @Test
    public void setHouse() throws Exception {
        Address address = getAddress();
        address.setHouse(777);
        assertEquals(777, address.getHouse());
    }

    @Test
    public void getApartments() throws Exception {
        Address address = getAddress();
        assertEquals(2, address.getApartments());
    }

    @Test
    public void setApartments() throws Exception {
        Address address = getAddress();
        address.setApartments(3);
        assertEquals(3, address.getApartments());
    }

    private Address getAddress() {
        Address address = new Address();

        Country country = new Country();
        country.setCountryName("USA");
        country.setCountryId(1);
        City city = new City();
        city.setCityName("Washingthon");
        city.setCountry(country);

        address.setCity(city);
        address.setPostCode(111111);
        address.setStreet("5 line");
        address.setHouse(1);
        address.setApartments(2);
        address.setAddressId(777);

        return address;
    }

}