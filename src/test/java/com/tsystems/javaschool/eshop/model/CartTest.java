package com.tsystems.javaschool.eshop.model;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

public class CartTest {

    @Test
    public void addProductToCart() throws Exception {
        Cart cart = getCart();
        Product product = new Product();
        product.setProductId(888);
        cart.addProductToCart(product, 1);
        assertTrue(cart.getProductsInCart().containsKey(product));
    }

    @Test
    public void getProductsInCart() throws Exception {
        Cart cart = getCart();

        Product product = new Product();
        product.setProductId(777);
        Map<Product, Integer> expected = new HashMap<>();
        expected.put(product, 1);

        assertEquals(expected, cart.getProductsInCart());
    }

    @Test
    public void setProductsInCart() throws Exception {
        Cart cart = getCart();

        Product product = new Product();
        product.setProductId(888);
        Map<Product, Integer> expected = new HashMap<>();
        expected.put(product, 2);

        cart.setProductsInCart(expected);
        assertEquals(expected, cart.getProductsInCart());
    }

    @Test
    public void setProductQtyInCart() throws Exception {
        Cart cart = getCart();
        Product product = new Product();
        product.setProductId(777);

        cart.setProductQtyInCart(product, 100);
        assertEquals(100, (int) cart.getProductsInCart().get(product));
    }

    @Test
    public void deleteProductFromCart() throws Exception {
        Cart cart = getCart();
        Product product = new Product();
        product.setProductId(777);

        cart.deleteProductFromCart(product);
        assertTrue(cart.getProductsInCart().isEmpty());
    }

    private Cart getCart() {
        Cart cart = new Cart();
        Product product = new Product();
        product.setProductId(777);
        cart.addProductToCart(product, 1);
        return cart;
    }

}