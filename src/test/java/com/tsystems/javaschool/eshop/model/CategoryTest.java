package com.tsystems.javaschool.eshop.model;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class CategoryTest {
    private Category category;

    @Before
    public void setUp() throws Exception {
        category = new Category();
        category.setCategoryName("testName");
        category.setCategoryId(777);
        category.setFilterFile("test.jsp");
    }

    @Test
    public void getCategoryId() throws Exception {
        assertEquals(777, category.getCategoryId());
    }

    @Test
    public void setCategoryId() throws Exception {
        category.setCategoryId(888);
        assertEquals(888, category.getCategoryId());
    }

    @Test
    public void getCategoryName() throws Exception {
        assertEquals("testName", category.getCategoryName());
    }

    @Test
    public void setCategoryName() throws Exception {
        category.setCategoryName("newName");
        assertEquals("newName", category.getCategoryName());
    }

    @Test
    public void getFilterFile() throws Exception {
        assertEquals("test.jsp", category.getFilterFile());
    }

    @Test
    public void setFilterFile() throws Exception {
        category.setFilterFile("newfile.jsp");
        assertEquals("newfile.jsp", category.getFilterFile());
    }

}