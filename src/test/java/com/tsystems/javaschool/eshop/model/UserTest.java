package com.tsystems.javaschool.eshop.model;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;

import static org.junit.Assert.assertEquals;

public class UserTest {
    private User user;

    @Before
    public void setUp() {
        user = new User();
        user.setUserId(1);
        user.setBirthdate(LocalDate.of(2001, 1, 1));
        user.setEmail("user@mail.com");
        user.setLogin("user");
        user.setName("Vasya");
        user.setSurname("Pupkin");
        user.setPassword("Pa$$w0rd");
    }

    @After
    public void tearDown() {
        user = null;
    }

    @Test
    public void getUserId() {
        assertEquals(1, user.getUserId());
    }

    @Test
    public void setUserId() {
        user.setUserId(777);
        assertEquals(777, user.getUserId());
    }

    @Test
    public void getLogin() {
        assertEquals("user", user.getLogin());
    }

    @Test
    public void setLogin() {
        user.setLogin("777");
        assertEquals("777", user.getLogin());
    }

    @Test
    public void getPassword() {
        assertEquals("Pa$$w0rd", user.getPassword());
    }

    @Test
    public void setPassword() {
        user.setPassword("777");
        assertEquals("777", user.getPassword());
    }

    @Test
    public void getName() {
        assertEquals("Vasya", user.getName());
    }

    @Test
    public void setName() {
        user.setName("777");
        assertEquals("777", user.getName());
    }

    @Test
    public void getSurname() {
        assertEquals("Pupkin", user.getSurname());
    }

    @Test
    public void setSurname() {
        user.setSurname("777");
        assertEquals("777", user.getSurname());
    }

    @Test
    public void getBirthdate() {
        assertEquals(LocalDate.of(2001, 1, 1), user.getBirthdate());
    }

    @Test
    public void setBirthdate() {
        user.setBirthdate(LocalDate.of(2002, 2, 2));
        assertEquals(LocalDate.of(2002, 2, 2), user.getBirthdate());
    }

    @Test
    public void getEmail() {
        assertEquals("user@mail.com", user.getEmail());
    }

    @Test
    public void setEmail() {
        user.setEmail("no@mail.com");
        assertEquals("no@mail.com", user.getEmail());
    }
}