package com.tsystems.javaschool.eshop.service.implementation;

import com.tsystems.javaschool.eshop.model.Cart;
import com.tsystems.javaschool.eshop.model.Product;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import javax.servlet.http.HttpSession;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CartServiceImplTest {
    private CartServiceImpl cartService;
    private Product pSpy;
    private static final int PRODUCT_ID_IN_CART = 777;
    private static final double PRODUCT_PRICE_IN_CART = 888.99;

    @Spy
    private Cart cart;

    @Mock
    private HttpSession httpSession;

    @Mock
    private ProductServiceImpl productService;

    @Mock
    private PhotoServiceImpl photoService;

    @Before
    public void setUp() throws Exception {
        Product p = new Product();
        p.setProductId(PRODUCT_ID_IN_CART);
        p.setPrice(PRODUCT_PRICE_IN_CART);
        pSpy = spy(p);
        cart.addProductToCart(pSpy, 1);
        cartService = new CartServiceImpl();
        when(httpSession.getAttribute("cart")).thenReturn(cart);
        when(productService.getProductById(PRODUCT_ID_IN_CART)).thenReturn(pSpy);
        cartService.setHttpSession(httpSession);
        cartService.setPhotoService(photoService);
        cartService.setProductService(productService);
    }

    @Test
    public void addProductToCart() throws Exception {
        Product p = new Product();
        p.setProductId(111);
        cartService.addProductToCart(p, 1);
        verify(cart).addProductToCart(p, 1);
        verify(httpSession).setAttribute("cart", cart);
    }

    @Test
    public void deleteProductFromCart() throws Exception {
        Product p = new Product();
        p.setProductId(111);
        cartService.deleteProductFromCart(p);
        verify(cart).deleteProductFromCart(p);
    }

    @Test
    public void getCartFromSession() throws Exception {
        cartService.getCartFromSession();
        verify(httpSession).getAttribute("cart");
    }

    @Test
    public void getProductsFromCart() throws Exception {
        cartService.getProductsInCart();
        verify(httpSession).getAttribute("cart");
        verify(cart).getProductsInCart();
    }

    @Test
    public void getPricesForProductsInCart() throws Exception {
        cartService.getPricesForProductsInCart();
        verify(httpSession).getAttribute("cart");
        verify(pSpy).getPrice();
    }

    @Test
    public void getPhotosForProductsInCart() throws Exception {
        cartService.getPhotosForProductsInCart();
        verify(cart).getProductsInCart();
        verify(photoService).getProductImagesURLs(PRODUCT_ID_IN_CART);
    }

    @Test
    public void clearCart() throws Exception {
        cartService.clearCart();
        verify(httpSession).getAttribute("cart");
        verify(cart).getProductsInCart();
        assertTrue(cart.getProductsInCart().isEmpty());
    }

    @Test
    public void setProductQtyInCart() throws Exception {
        cartService.setProductQtyInCart(PRODUCT_ID_IN_CART, 10);
        verify(cart).setProductQtyInCart(pSpy, 10);
    }
}