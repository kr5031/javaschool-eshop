package com.tsystems.javaschool.eshop.service.implementation;

import com.tsystems.javaschool.eshop.dao.implementation.OrderDaoImpl;
import com.tsystems.javaschool.eshop.model.Cart;
import com.tsystems.javaschool.eshop.model.City;
import com.tsystems.javaschool.eshop.model.Country;
import com.tsystems.javaschool.eshop.model.Order;
import com.tsystems.javaschool.eshop.model.OrderStatus;
import com.tsystems.javaschool.eshop.model.ProductInOrder;
import com.tsystems.javaschool.eshop.model.Role;
import com.tsystems.javaschool.eshop.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OrderServiceImplTest {
    private static final int USER_ID = 777;
    private OrderServiceImpl orderService;
    private User user;
    private Order order;
    private OrderStatus os;

    @Mock
    OrderDaoImpl orderDAO;

    @Mock
    UserServiceImpl userService;

    @Mock
    CartServiceImpl cartService;

    @Mock
    ProductServiceImpl productService;

    @Mock
    JmsServiceImpl jmsService;

    @Before
    public void setUp() throws Exception {
        orderService = new OrderServiceImpl();
        orderService.setProductService(productService);
        orderService.setUserService(userService);
        orderService.setCartService(cartService);
        orderService.setOrderDao(orderDAO);
        orderService.setJmsService(jmsService);

        order = new Order();
        user = new User();
        user.setUserId(USER_ID);
        user.setUserRole(new Role());
        order.setUser(user);
        Country country = new Country();
        country.setCountryName("USA");
        City city = new City();
        city.setCityName("NY");
        city.setCountry(country);
        os = new OrderStatus();
        os.setOrderStatusName("new");
        os.setOrderStatusId(777);

        when(orderDAO.getOrderById(anyInt())).thenReturn(order);
        when(userService.getUserById(USER_ID)).thenReturn(user);
        when(orderDAO.getOrdersOfUser(user)).thenReturn(Arrays.asList(order));
        when(orderDAO.getCityByName(anyString(), anyString())).thenReturn(city);
        when(orderDAO.getCountryByName(anyString())).thenReturn(country);
        when(cartService.getCartFromSession()).thenReturn(new Cart());
        when(orderDAO.getOrderStatusById(anyInt())).thenReturn(os);
    }

    @Test
    public void getOrderById() throws Exception {
        orderService.getOrderById(user, 1);
        verify(orderDAO).getOrderById(1);
    }

    @Test
    public void getOrdersByUserId() throws Exception {
        List<Order> actual = orderService.getOrdersByUserId(USER_ID);
        verify(orderDAO).getOrdersOfUser(user);
        List<Order> expected = Arrays.asList(order);
        assertEquals(actual, expected);
    }

    @Test
    public void saveOrUpdateOrder() throws Exception {
        orderService.saveOrUpdateOrder(order);
        verify(orderDAO).saveOrUpdateOrder(order);
        verify(jmsService).orderCreated();
    }

    @Test
    public void getProductsInOrderByOrderId() throws Exception {
        orderService.getProductsInOrderByOrderId(1);
        verify(orderDAO).getProductsInOrderByOrderId(1);
    }

    @Test
    public void getAllDeliveryMethods() throws Exception {
        orderService.getAllDeliveryMethods();
        verify(orderDAO).getAllDeliveryMethods();
    }

    @Test
    public void getDeliveryMethodById() throws Exception {
        orderService.getDeliveryMethodById(1);
        verify(orderDAO).getDeliveryMethodById(1);
    }

    @Test
    public void createOrder() throws Exception {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("country", "USA");
        params.add("city", "NY");
        params.add("postCode", "123456");
        params.add("street", "5 line");
        params.add("house", "6");
        params.add("apartments", "111");
        params.add("deliveryMethodId", "3");
        params.add("paymentMethodId", "4");
        params.add("total", "555.55");

        orderService.createOrder(params);

        verify(userService).getCurrentUser();
        verify(cartService).getCartFromSession();
        verify(cartService).clearCart();
    }

    @Test
    public void getAllPaymentMethods() throws Exception {
        orderService.getAllPaymentMethods();
        verify(orderDAO).getAllPaymentMethods();
    }

    @Test
    public void getPaymentMethodById() throws Exception {
        orderService.getPaymentMethodById(1);
        verify(orderDAO).getPaymentMethodById(1);
    }

    @Test
    public void saveOrUpdateProductInOrder() throws Exception {
        ProductInOrder p = new ProductInOrder();
        orderService.saveOrUpdateProductInOrder(p);
        verify(orderDAO).saveOrUpdateProductInOrder(p);
    }

    @Test
    public void getAllOrders() throws Exception {
        orderService.getAllOrders();
        verify(orderDAO).getAllOrders();
    }

    @Test
    public void getAllOrdersStatuses() throws Exception {
        orderService.getAllOrdersStatuses();
        verify(orderDAO).getAllOrdersStatuses();
    }

    @Test
    public void updateOrdersStatuses() throws Exception {
        MultiValueMap<String, String> mvm = new LinkedMultiValueMap<>();
        mvm.add(Integer.toString(order.getOrderId()), "777");
        orderService.updateOrdersStatuses(mvm);
        verify(orderDAO).saveOrUpdateOrder(order);
        assertEquals(order.getOrderStatus().getOrderStatusId(), 777);
    }

}