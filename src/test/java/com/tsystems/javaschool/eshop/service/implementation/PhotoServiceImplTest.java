package com.tsystems.javaschool.eshop.service.implementation;

import com.tsystems.javaschool.eshop.dao.PhotoDAO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.multipart.MultipartFile;

import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class PhotoServiceImplTest {
    private PhotoServiceImpl photoService;

    @Mock
    PhotoDAO photoDAO;

    @Before
    public void setUp() throws Exception {
        photoService = new PhotoServiceImpl();
        photoService.setPhotoDAO(photoDAO);
    }

    @Test
    public void getPhotoById() throws Exception {
        photoService.getPhotoById(1);
        verify(photoDAO).getPhotoById(1);
    }

    @Test
    public void getPreviewPhoto() throws Exception {
        photoService.getPreviewPhoto(1);
        verify(photoDAO).getPreviewPhoto(1);
    }

    @Test
    public void getListPhotosByProductId() throws Exception {
        photoService.getListPhotosByProductId(1);
        verify(photoDAO).getListPhotosByProductId(1);
    }

    @Test
    public void savePhotos() throws Exception {
        MultipartFile[] m = new MultipartFile[] {};
        photoService.savePhotos(m, 1);
        verify(photoDAO).savePhotos(m, 1);
    }

    @Test
    public void getProductImagesURLs() throws Exception {
        photoService.getProductImagesURLs(1);
        verify(photoDAO).getListPhotosByProductId(1);
    }

}