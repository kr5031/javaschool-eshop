package com.tsystems.javaschool.eshop.service.implementation;

import com.tsystems.javaschool.eshop.dao.ProductDAO;
import com.tsystems.javaschool.eshop.model.Category;
import com.tsystems.javaschool.eshop.model.Color;
import com.tsystems.javaschool.eshop.model.Manufacturer;
import com.tsystems.javaschool.eshop.model.Product;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ProductServiceImplTest {
    @Mock
    ProductDAO productDAO;

    private ProductServiceImpl productService;
    private Product product;

    @Before
    public void setUp() throws Exception {
        productService = new ProductServiceImpl();
        productService.setProductDAO(productDAO);

        product = new Product();
        product.setProductId(1);
    }

    @Test
    public void saveProduct() throws Exception {
        productService.saveProduct(product);
        verify(productDAO).saveProduct(product);
    }

    @Test
    public void getAllProducts() throws Exception {
        productService.getAllProducts();
        verify(productDAO).getAllProducts();
    }

    @Test
    public void getProductById() throws Exception {
        productService.getProductById(1);
        verify(productDAO).getProductById(1);
    }

    @Test
    public void updateProduct() throws Exception {
        productService.updateProduct(product);
        verify(productDAO).updateProduct(product);
    }

    @Test
    public void getFilteredProducts() throws Exception {
        MultiValueMap<String, String> mvm = new LinkedMultiValueMap<>();
        productService.getFilteredProducts(mvm);
        verify(productDAO).getFilteredProducts(mvm);
    }

    @Test
    public void getAllManufacturersForCategory() throws Exception {
        productService.getAllManufacturersForCategory(1);
        verify(productDAO).getAllManufacturersForCategory(1);
    }

    @Test
    public void getFilterFileForCategory() throws Exception {
        productService.getFilterFileForCategory(1);
        verify(productDAO).getFilterFileForCategory(1);
    }

    @Test
    public void getPriceForProductId() throws Exception {
        productService.getPriceForProductId(1);
        verify(productDAO).getPriceForProductId(1);
    }

    @Test
    public void getAllCategories() throws Exception {
        productService.getAllCategories();
        verify(productDAO).getAllCategories();
    }

    @Test
    public void getAllColors() throws Exception {
        productService.getAllColors();
        verify(productDAO).getAllColors();
    }

    @Test
    public void getAllManufacturers() throws Exception {
        productService.getAllManufacturers();
        verify(productDAO).getAllManufacturers();
    }

    @Test
    public void createProduct() throws Exception {
        MultiValueMap<String, String> mvm = new LinkedMultiValueMap<>();
        mvm.add("name", "someName");
        mvm.add("categoryId", "1");
        mvm.add("price", "111.22");
        mvm.add("colorId", "3");
        mvm.add("manufacturerId", "5");
        mvm.add("description", "some description");
        mvm.add("memory", "8");
        mvm.add("weight", "5.5");
        mvm.add("screen", "5.5");
        productService.createProduct(mvm);
        verify(productDAO).createProduct(any(Product.class));
    }

    @Test
    public void getCategoryIdByName() throws Exception {
        productService.getCategoryIdByName("name");
        verify(productDAO).getCategoryIdByName("name");
    }

    @Test
    public void addCategory() throws Exception {
        productService.addCategory("name");
        verify(productDAO).addCategory(any(Category.class));
    }

    @Test
    public void deleteCategory() throws Exception {
        productService.deleteCategory(1);
        verify(productDAO).deleteCategory(1);
    }

    @Test
    public void addManufacturer() throws Exception {
        productService.addManufacturer("name");
        verify(productDAO).addManufacturer(any(Manufacturer.class));
    }

    @Test
    public void deleteManufacturer() throws Exception {
        productService.deleteManufacturer(1);
        verify(productDAO).deleteManufacturer(any(Manufacturer.class));
    }

    @Test
    public void addColor() throws Exception {
        productService.addColor("name");
        verify(productDAO).addColor(any(Color.class));
    }

    @Test
    public void deleteColor() throws Exception {
        productService.deleteColor(1);
        verify(productDAO).deleteColor(any(Color.class));
    }

}