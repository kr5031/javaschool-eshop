package com.tsystems.javaschool.eshop.service.implementation;

import com.tsystems.javaschool.eshop.dao.StatsDAO;
import com.tsystems.javaschool.eshop.model.Product;
import com.tsystems.javaschool.eshop.model.User;
import com.tsystems.javaschool.eshop.model.dto.ProductDTO;
import com.tsystems.javaschool.eshop.model.dto.UserDTO;
import com.tsystems.javaschool.eshop.service.PhotoService;
import com.tsystems.javaschool.eshop.service.ProductService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StatsServiceImplTest {
    private StatsServiceImpl statsService;
    private Product product;

    @Mock
    StatsDAO statsDAO;

    @Mock
    ProductService productService;

    @Mock
    PhotoService photoService;

    @Before
    public void setUp() throws Exception {
        statsService = new StatsServiceImpl();
        statsService.setPhotoService(photoService);
        statsService.setProductService(productService);
        statsService.setStatsDAO(statsDAO);

        product = new Product();
        product.setProductId(1);
        when(productService.getProductById(1)).thenReturn(product);
    }

    @Test
    public void getTopClients() throws Exception {
        statsService.getTopClients(10);
        verify(statsDAO).getTopClients(10);
    }

    @Test
    public void getNumberOfOrdersForClients() throws Exception {
        UserDTO u = new UserDTO(new User());
        statsService.getNumberOfOrdersForClients(Arrays.asList(u));
        verify(statsDAO).getNumberOfOrdersForClient(u);
    }

    @Test
    public void getTopProducts() throws Exception {
        statsService.getTopProducts(10);
        verify(statsDAO).getTopProducts(10);
    }

    @Test
    public void getNumberOfSalesForProducts() throws Exception {
        Product p = new Product();
        p.setProductId(1);
        statsService.getNumberOfSalesForProducts(Arrays.asList(p));
        verify(statsDAO).getNumberOfSalesForProduct(p);
    }

    @Test
    public void getNumberOfSalesForProductsDTO() throws Exception {
        ProductDTO p = new ProductDTO();
        p.setProductId(1);

        statsService.getNumberOfSalesForProductsDTO(Arrays.asList(p));
        verify(statsDAO).getNumberOfSalesForProduct(product);
    }

    @Test
    public void getOrdersForLastWeek() throws Exception {
        statsService.getOrdersForLastWeek();
        verify(statsDAO).getOrdersForLastWeek();
    }

    @Test
    public void getOrdersForLastMonth() throws Exception {
        statsService.getOrdersForLastMonth();
        verify(statsDAO).getOrdersForLastMonth();
    }

}