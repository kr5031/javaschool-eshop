package com.tsystems.javaschool.eshop.service.implementation;

import com.tsystems.javaschool.eshop.exceptions.MissingRequiredFieldsException;
import com.tsystems.javaschool.eshop.exceptions.PasswordsDontMatchException;
import com.tsystems.javaschool.eshop.dao.UserDAO;
import com.tsystems.javaschool.eshop.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import javax.servlet.http.HttpSession;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {
    private UserServiceImpl userService;

    @Mock
    private UserDAO userDAO;

    @Mock
    private HttpSession httpSession;

    @Mock
    private User user;

    @Before
    public void setUp() throws Exception {
        userService = new UserServiceImpl();
        userService.setHttpSession(httpSession);
        userService.setUserDao(userDAO);
        when(httpSession.getAttribute("user")).thenReturn(user);
    }

    @Test
    public void isAdmin() throws Exception {
        userService.isAdmin();
        verify(httpSession).getAttribute("isAdmin");
    }

    @Test
    public void getUserByLogin() throws Exception {
        userService.getUserByLogin("login");
        verify(userDAO).getUserByLogin("login");
    }

    @Test
    public void getUserByLoginEmpty() throws Exception {
        User u = userService.getUserByLogin("");
        assertEquals(null, u);
    }

    @Test
    public void saveOrUpdateUser() throws Exception {
        MultiValueMap<String, String> mvm = new LinkedMultiValueMap<>();
        mvm.add("birthdate", "01-01-2001");
        mvm.add("email", "a@b.c");
        mvm.add("name", "Vasya");
        mvm.add("surname", "Vasin");
        mvm.add("password", "123");
        mvm.add("confirmPassword", "123");
        User u = new User();
        userService.saveOrUpdateUser(mvm, u);
        verify(userDAO).saveOrUpdateUser(u);
    }

    @Test(expected = MissingRequiredFieldsException.class)
    public void saveOrUpdateUserMissingFieldsException() throws Exception {
        userService.saveOrUpdateUser(new LinkedMultiValueMap<>(), new User());
    }

    @Test(expected = PasswordsDontMatchException.class)
    public void saveOrUpdateUserPasswordsDontMatchException() throws Exception {
        MultiValueMap<String, String> mvm = new LinkedMultiValueMap<>();
        mvm.add("birthdate", "01-01-2001");
        mvm.add("email", "a@b.c");
        mvm.add("name", "Vasya");
        mvm.add("surname", "Vasin");
        User u = new User();
        userService.saveOrUpdateUser(mvm, u);
    }

    @Test
    public void createUser() throws Exception {
        MultiValueMap<String, String> mvm = new LinkedMultiValueMap<>();
        mvm.add("birthdate", "01-01-2001");
        mvm.add("email", "a@b.c");
        mvm.add("name", "Vasya");
        mvm.add("surname", "Vasin");
        mvm.add("password", "123");
        mvm.add("confirmPassword", "123");
        mvm.add("login", "vasya");
        userService.createUser(mvm);
        verify(userDAO).saveOrUpdateUser(any(User.class));
    }

    @Test(expected = MissingRequiredFieldsException.class)
    public void createUserMissingFieldsException() throws Exception {
        MultiValueMap<String, String> mvm = new LinkedMultiValueMap<>();
        userService.createUser(mvm);
    }

    @Test
    public void getRoleById() throws Exception {
        userService.getRoleById(1);
        verify(userDAO).getRoleById(1);
    }

    @Test
    public void getUserById() throws Exception {
        userService.getUserById(1);
        verify(userDAO).getUserById(1);
    }

    @Test
    public void getCurrentUser() throws Exception {
        userService.getCurrentUser();
        verify(httpSession).getAttribute("user");
    }

    @Test
    public void getCurrentUserId() throws Exception {
        userService.getCurrentUserId();
        verify(httpSession).getAttribute("user");
        verify(user).getUserId();
    }

}